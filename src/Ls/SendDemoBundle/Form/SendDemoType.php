<?php

namespace Ls\SendDemoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class SendDemoType extends AbstractType {
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('name', null, array(
            'label' => 'Imię/Nazwa zespołu:',
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Wypełnij pole'
                ))
            )
        ));
        
        $builder->add('email', null, array(
            'label' => 'Twój adres e-mail:',
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Wypełnij pole'
                ))
            )
        ));
        
        $builder->add('message', TextareaType::class, array(
            'label' => 'Treść wiadomości:',
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Wypełnij pole'
                ))
            )
        ));
        
        $builder->add('uploadFiles', FileType::class, array(
            'label' => 'Dodaj załącznik',
            'multiple' => true, 
        ));
        
        $builder->add('submit', SubmitType::class, array(
                'label' => 'Wyślij'
            )
        );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Ls\SendDemoBundle\Entity\SendDemo',
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix() {
        return 'form_send_demo';
    }
}
