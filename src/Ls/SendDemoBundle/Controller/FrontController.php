<?php

namespace Ls\SendDemoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Ls\SendDemoBundle\Entity\SendDemo;
use Ls\SendDemoBundle\Entity\SendDemoFile;
use Ls\SendDemoBundle\Form\SendDemoType;

class FrontController extends Controller {

    public function formAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $entity = new SendDemo();
        
        $form = $this->createForm(SendDemoType::class, $entity, array(
            'method' => 'POST',
            'action' => $this->generateUrl('ls_send_demo'),
        ));

        $form->handleRequest($request);

        if ($form->isValid()) {
            $files = $entity->getUploadFiles();
            
            if ($files != null) {
                foreach ($files as $file) {
                    if ($file != null) {
                        $demoFile = new SendDemoFile();
                        $demoFile->upload($file);
                        $demoFile->setSendDemo($entity);

                        $em->persist($demoFile);
                        $em->flush($demoFile);
                    }

                }
            }
            
            $em->persist($entity);
            $em->flush();

            return new Response('OK');
        }

        if ($form->isSubmitted()) {
            return new Response('Sprawdź pola formularza.');
        }

        return $this->render('LsSendDemoBundle:Front:form.html.twig', array(
            'form' => $form->createView(),
        ));
    }

}
