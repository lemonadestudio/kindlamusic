<?php

namespace Ls\SendDemoBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Ls\SendDemoBundle\Entity\SendDemoFile;

/**
 * SendDemo
 *
 * @ORM\Entity
 * @ORM\Table(name="send_demo")
 */
class SendDemo {

    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    private $name;
    
    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    private $email;
    
    /**
     * @var string
     * @ORM\Column(type="text")
     */
    private $message;
    
    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $created_at;

    /**
     * @ORM\OneToMany(
     *   targetEntity="Ls\SendDemoBundle\Entity\SendDemoFile",
     *   mappedBy="sendDemo",
     *   cascade={"persist", "remove"}
     * )
     * @var \Doctrine\Common\Collections\Collection
     */
    private $files;
    
    private $uploadFiles;

    /**
     * Constructor
     */
    public function __construct() {
        $this->files = new ArrayCollection();
        $this->created_at = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return SendDemo
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }
    
    /**
     * Set email
     *
     * @param string $email
     * @return SendDemo
     */
    public function setEmail($email) {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail() {
        return $this->email;
    }
    
    /**
     * Set message
     *
     * @param string $message
     * @return SendDemo
     */
    public function setMessage($message) {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage() {
        return $this->message;
    }
    
    /**
     * Set created_at
     *
     * @param \DateTime $created_at
     * @return SendDemo
     */
    public function setCreatedAt($created_at) {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime
     */
    public function getCreatedAt() {
        return $this->created_at;
    }
    
    /**
     * Set uploadFiles
     *
     * @param string $uploadFiles
     * @return SendDemo
     */
    public function setUploadFiles($uploadFiles) {
        $this->uploadFiles = $uploadFiles;

        return $this;
    }

    /**
     * Get uploadFiles
     *
     * @return string
     */
    public function getUploadFiles() {
        return $this->uploadFiles;
    }

    /**
     * Add file
     *
     * @param \Ls\SendDemoBundle\Entity\SendDemoFile $file
     * @return SendDemo
     */
    public function addFile(SendDemoFile $file) {
        $this->files[] = $file;

        return $this;
    }

    /**
     * Remove file
     *
     * @param \Ls\SendDemoBundle\Entity\SendDemoFile $file
     */
    public function removeFiles(SendDemoFile $file) {
        $this->files->removeElement($file);
    }

    /**
     * Get files
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFiles() {
        return $this->files;
    }

    public function __toString() {
        if (is_null($this->getName())) {
            return 'NULL';
        }
        return $this->getName();
    }
}