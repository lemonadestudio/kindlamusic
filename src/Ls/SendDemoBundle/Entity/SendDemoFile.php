<?php

namespace Ls\SendDemoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * SendDemoFile
 *
 * @ORM\Entity
 * @ORM\Table(name="send_demo_file")
 */
class SendDemoFile {

    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $filename;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Ls\SendDemoBundle\Entity\SendDemo",
     *     inversedBy="files",
     *     cascade={"persist", "remove"}
     * )
     * @ORM\JoinColumn(
     *     name="send_demo_id",
     *     referencedColumnName="id"
     * )
     * @var \Ls\SendDemoBundle\Entity\SendDemo
     *
     */
    private $sendDemo;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set filename
     *
     * @param string $filename
     * @return SendDemoFile
     */
    public function setFilename($filename) {
        $this->filename = $filename;

        return $this;
    }

    /**
     * Get filename
     *
     * @return string
     */
    public function getFilename() {
        return $this->filename;
    }
    
    /**
     * Set sendDemo
     *
     * @param \Ls\SendDemoBundle\Entity\SendDemo $sendDemo
     * @return SendDemoFile
     */
    public function setSendDemo(SendDemo $sendDemo = null) {
        $this->sendDemo = $sendDemo;

        return $this;
    }

    /**
     * Get sendDemo
     *
     * @return \Ls\SendDemoBundle\Entity\SendDemo
     */
    public function getSendDemo() {
        return $this->sendDemo;
    }

    public function __toString() {
        if (is_null($this->getFilename())) {
            return 'NULL';
        }
        return $this->getFilename();
    }
    
    
    public function deleteFile() {
        if (!empty($this->filename)) {
            $filename = $this->getFileAbsolutePath();
            if (file_exists($filename)) {
                @unlink($filename);
            }
        }
    }

    public function getFileAbsolutePath() {
        return empty($this->filename) ? null : $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->filename;
    }

    public function getFileWebPath() {
        return empty($this->filename) ? null : '/' . $this->getUploadDir() . '/' . $this->filename;
    }

    public static function getUploadRootDir() {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . self::getUploadDir();
    }

    public static function getUploadDir() {
        // get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
        return 'upload/demo';
    }
    
    public function upload(UploadedFile $file)
    {
        if (null === $file) {
            return;
        }
        
        $fileName = uniqid('demo-') . '.' . $file->guessExtension();
        $this->setFilename($fileName);
        
        $file->move($this->getUploadRootDir(), $fileName);
    }
}