<?php

namespace Ls\SectionBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Ls\AudioPlayerBundle\Form\AudioFileDownloadType;
use Ls\UserBundle\Entity\User;

/**
 * Section controller.
 *
 */
class FrontController extends Controller {

    /**
     * Lists Section entity on index page.
     *
     */
    public function kindlaMusicAction($id) {
        $em = $this->getDoctrine()->getManager();

        $qb = $em->createQueryBuilder();
        $entity = $qb->select('a')
            ->from('LsSectionBundle:Section', 'a')
            ->where('a.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();

        return $this->render('LsSectionBundle:Front:kindla_music.html.twig', array(
            'entity' => $entity,
        ));
    }
    
    public function kmPublishingAction($id) {
        $em = $this->getDoctrine()->getManager();

        $qb = $em->createQueryBuilder();
        $entity = $qb->select('a')
            ->from('LsSectionBundle:Section', 'a')
            ->where('a.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();

        return $this->render('LsSectionBundle:Front:km_publishing.html.twig', array(
            'entity' => $entity,
        ));
    }
    
    public function mamamusicAction($id) {
        $em = $this->getDoctrine()->getManager();

        $qb = $em->createQueryBuilder();
        $entity = $qb->select('a')
            ->from('LsSectionBundle:Section', 'a')
            ->where('a.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();

        return $this->render('LsSectionBundle:Front:mamamusic.html.twig', array(
            'entity' => $entity,
        ));
    }

    public function kmPublishingPlayerAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        
        $curentlyUser = $this->get('security.context')->getToken()->getUser();
        if ($curentlyUser instanceof User) {
            $userData = $curentlyUser;
        } else {
            return $this->redirect($this->generateUrl('ls_user_login'));
        }
        
        $user = $em->getRepository('LsUserBundle:User')->find($userData->getId());

        $categories = $em->createQueryBuilder()
            ->select('e', 'f')
            ->from('LsAudioPlayerBundle:AudioFileCategory', 'e')
            ->leftJoin('e.audioFiles', 'f')
            ->leftJoin('e.users', 'u')
            ->where('u.id = :user_id')
            ->setParameter('user_id', $user->getId())
            ->orderBy('e.arrangement', 'ASC')
            ->addOrderBy('f.arrangement', 'ASC')
            ->getQuery()
            ->getResult();
        
        $form = $this->createForm(AudioFileDownloadType::class, null, array(
            'action' => $this->generateUrl('ls_core_song_send'),
            'method' => 'POST'
        ));
        $form->add('submit', SubmitType::class, array('label' => $this->get('translator')->trans('form.label.send')));

        return $this->render('LsSectionBundle:Front:player.html.twig', array(
            'categories' => $categories,
            'form' => $form->createView(),
            'userData' => $userData
        ));
    }
    
    public function kmeventsAction($id) {
        $em = $this->getDoctrine()->getManager();

        $qb = $em->createQueryBuilder();
        $entity = $qb->select('a')
            ->from('LsSectionBundle:Section', 'a')
            ->where('a.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();
        
        $videoGallery = $em->createQueryBuilder()
            ->select('g', 'm')
            ->from('LsGalleryBundle:Gallery', 'g')
            ->leftJoin('g.movies', 'm')
            ->where('g.name = :galleryName')
            ->setParameter('galleryName', "Filmy na stronie głównej kmevents")
            ->getQuery()
            ->getOneOrNullResult();
        
        return $this->render('LsSectionBundle:Front:kmevents.html.twig', array(
            'entity' => $entity,
            'videoGallery' => $videoGallery
        ));
    }
    
    public function contactAction($id) {
        $em = $this->getDoctrine()->getManager();

        $qb = $em->createQueryBuilder();
        $entity = $qb->select('a')
            ->from('LsSectionBundle:Section', 'a')
            ->where('a.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();

        return $this->render('LsSectionBundle:Front:contact.html.twig', array(
            'entity' => $entity,
        ));
    }

    public function kmPublishingPlayerByUrlAction(Request $request, $publicId) {
        $em = $this->getDoctrine()->getManager();

        $curentlyUser = $this->get('security.context')->getToken()->getUser();
        if ($curentlyUser instanceof User) {
            $userData = $curentlyUser;
        } else {
            $userData = null;
        }

        $categories = $em->createQueryBuilder()
            ->select('e', 'f')
            ->from('LsAudioPlayerBundle:AudioFileCategory', 'e')
            ->leftJoin('e.audioFiles', 'f')
            ->where('e.publicId = :publicId')
            ->setParameter('publicId', $publicId)
            ->orderBy('e.arrangement', 'ASC')
            ->addOrderBy('f.arrangement', 'ASC')
            ->getQuery()
            ->getResult();

        if (count($categories) == 0) {
            throw $this->createNotFoundException('Unable to find Category entity.');
        }
        
        $form = $this->createForm(AudioFileDownloadType::class, null, array(
            'action' => $this->generateUrl('ls_core_song_send'),
            'method' => 'POST'
        ));
        $form->add('submit', SubmitType::class, array('label' => $this->get('translator')->trans('form.label.send')));

        return $this->render('LsSectionBundle:Front:player.html.twig', array(
            'categories' => $categories,
            'form' => $form->createView(),
            'userData' => $userData
        ));
    }
}
