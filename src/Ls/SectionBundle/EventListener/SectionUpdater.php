<?php

namespace Ls\SectionBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Ls\CoreBundle\Utils\Tools;
use Ls\SectionBundle\Entity\Section;

class SectionUpdater implements EventSubscriber {

    public function getSubscribedEvents() {
        return array(
            'prePersist',
            'preUpdate',
            'postRemove',
        );
    }

    public function prePersist(LifecycleEventArgs $args) {
        $entity = $args->getEntity();

        if ($entity instanceof Section) {
            if (!$entity->getCreatedAt()) {
                $entity->setCreatedAt(new \DateTime());
            }
            if ($entity->getSeoGenerate()) {
                // description
                // usunięcie ewentualnego tagu style wraz z jego zawartością
                $description = preg_replace('/<style.+\/style>/su', '', $entity->getContent());
                $description = strip_tags($description);
                $description = html_entity_decode($description, ENT_COMPAT | ENT_HTML401, 'UTF-8');
                // usunięcie nowych linii i podwójnych białych znaków
                $description = preg_replace('/\s\s+/u', ' ', $description);
                $description = trim($description);
                // skrócenie do około 160 znaków rekomendowanych przez Google
                $description = Tools::truncateWord($description, 155, '...');

                // keywords
                $keywords_arr = explode(' ', $entity->getTitle() . ' ' . $description);

                $keywords = array();
                if (is_array($keywords_arr)) {
                    foreach ($keywords_arr as $kw) {
                        $kw = trim($kw);
                        $kw = preg_replace('/[\.,;\'\"]/', '', $kw);
                        if (strlen($kw) >= 4 && !in_array($kw, $keywords)) {
                            $keywords[] = $kw;
                        }
                        if (count($keywords) >= 10) {
                            break;
                        }
                    }
                }

                $entity->setSeoTitle(strip_tags($entity->getTitle()));
                $entity->setSeoDescription($description);
                $entity->setSeoKeywords(implode(', ', $keywords));
                $entity->setSeoGenerate(false);
            }
        }
    }

    public function preUpdate(LifecycleEventArgs $args) {
        $entity = $args->getEntity();

        if ($entity instanceof Section) {
            $entity->setUpdatedAt(new \DateTime());
        
            if ($entity->getSeoGenerate()) {
                // description
                // usunięcie ewentualnego tagu style wraz z jego zawartością
                $description = preg_replace('/<style.+\/style>/su', '', $entity->getContent());
                $description = strip_tags($description);
                $description = html_entity_decode($description, ENT_COMPAT | ENT_HTML401, 'UTF-8');
                // usunięcie nowych linii i podwójnych białych znaków
                $description = preg_replace('/\s\s+/u', ' ', $description);
                $description = trim($description);
                // skrócenie do około 160 znaków rekomendowanych przez Google
                $description = Tools::truncateWord($description, 155, '...');

                // keywords
                $keywords_arr = explode(' ', $entity->getTitle() . ' ' . $description);

                $keywords = array();
                if (is_array($keywords_arr)) {
                    foreach ($keywords_arr as $kw) {
                        $kw = trim($kw);
                        $kw = preg_replace('/[\.,;\'\"]/', '', $kw);
                        if (strlen($kw) >= 4 && !in_array($kw, $keywords)) {
                            $keywords[] = $kw;
                        }
                        if (count($keywords) >= 10) {
                            break;
                        }
                    }
                }

                $entity->setSeoTitle(strip_tags($entity->getTitle()));
                $entity->setSeoDescription($description);
                $entity->setSeoKeywords(implode(', ', $keywords));
                $entity->setSeoGenerate(false);
            }
        }
    }

    public function postRemove(LifecycleEventArgs $args) {
        $entity = $args->getEntity();

        if ($entity instanceof Section) {
        }
    }

}