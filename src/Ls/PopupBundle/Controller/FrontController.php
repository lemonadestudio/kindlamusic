<?php

namespace Ls\PopupBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Cookie;

/**
 * Popup controller.
 *
 */
class FrontController extends Controller {

    public function indexAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $qb = $em->createQueryBuilder();
        $entity = $qb->select('a')
            ->from('LsPopupBundle:Popup', 'a')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();

        $sessionValue = null;
        
        if ($entity) {
            $sessionValue = md5($entity->getBtnTitle2() .$entity->getBtnUrl2() .$entity->getBtnTitle() .$entity->getBtnUrl() . $entity->getPhoto());
        }
        
        return $this->render('LsPopupBundle:Front:index.html.twig', array(
            'entity' => $entity,
            'session_value' => $sessionValue
        ));
    }

    public function closeAction(Request $request, $value) {

    	if($request->isXmlHttpRequest()) {
    		$session = new Session();
           	$session->set('popup',  $value);
            
            $response = new Response();
            $response->headers->setCookie(new Cookie('popup', $value));
            $response->sendHeaders();
        }
        else {
        	throw $this->createNotFoundException('Ajax request only');
        }

        return new Response('OK');
    }
}
