<?php

namespace Ls\PopupBundle\Controller;

use Ls\PopupBundle\Entity\Popup;
use Ls\PopupBundle\Form\PopupType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AdminController extends Controller { 

    public function indexAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->createQueryBuilder()
            ->select('e')
            ->from('LsPopupBundle:Popup', 'e')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();

        if (!$entity) {
            $entity = new Popup();
        }
        
        $form = $this->createForm(PopupType::class, $entity, array(
            'action' => $this->generateUrl('ls_admin_popup'),
            'method' => 'POST'
        ));
        $form->add('submit', SubmitType::class, array('label' => 'Zapisz zmiany'));
        $form->add('remove_image', SubmitType::class, array('label' => 'Usuń zdjęcie'));

        $form->handleRequest($request);
        if ($form->isValid()) {
			if (null !== $entity->getFile()) {
				$entity->deletePhoto();

				$sFileName = uniqid('popup-image-') . '.' . $entity->getFile()->guessExtension();
				$entity->setPhoto($sFileName);
				$entity->upload();
			}

            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Aktualizacja zakończona sukcesem.');

            if ($form->get('submit')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_popup'));
            }

            if ($form->get('remove_image')->isClicked()) {
				$entity->setPhoto(null);
				$entity->deletePhoto();
                
                $em->persist($entity);
                $em->flush();
                
                return $this->redirect($this->generateUrl('ls_admin_popup'));
            }
        }
        if ($form->isSubmitted()) {
            $this->get('session')->getFlashBag()->add('error', 'Sprawdź pola formularza.');
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
        $breadcrumbs->addItem('Popup', $this->get('router')->generate('ls_admin_popup'));

        return $this->render('LsPopupBundle:Admin:index.html.twig', array(
            'form' => $form->createView(),
            'entity' => $entity,
        ));
    }
}
