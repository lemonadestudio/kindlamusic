<?php

namespace Ls\PopupBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Ls\CoreBundle\Utils\Tools;
use Ls\PopupBundle\Entity\Popup;

class PopupUpdater implements EventSubscriber {

    public function getSubscribedEvents() {
        return array(
            'postRemove',
        );
    }


    public function postRemove(LifecycleEventArgs $args) {
        $entity = $args->getEntity();

        if ($entity instanceof Popup) {
            $entity->deletePhoto();
        }
    }

}