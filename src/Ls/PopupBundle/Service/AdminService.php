<?php

namespace Ls\PopupBundle\Service;

use Knp\Menu\ItemInterface;
use Ls\CoreBundle\Helper\AdminBlock;
use Ls\CoreBundle\Helper\AdminLink;
use Ls\CoreBundle\Helper\AdminRow;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Ls\UserBundle\Helper\AdminRightsBlock;
use Ls\UserBundle\Helper\AdminRightsBundle;
use Ls\UserBundle\Helper\AdminRightsRoute;

class AdminService {
    private $container;

    public function __construct(ContainerInterface $container) {
        $this->container = $container;
    }

    public function addToMenu(ItemInterface $parent, $route, $set) {
        $item = $parent->addChild('Moduł reklamowy Popup', array(
            'route' => 'ls_admin_popup',
        ));

        $current_set = true;

        if (!$set) {
            switch ($route) {
                case 'ls_admin_popup':
                    $item->setCurrent(true);
                    break;
                default:
                    $current_set = false;
                    break;
            }
        }

        return $current_set;
    }

    public function addToDashboard(AdminBlock $parent) {
        $router = $this->container->get('router');

        $row = new AdminRow('Moduł reklamowy Popup');
        $parent->addRow($row);

        $row->addLink(new AdminLink('Zarządzaj', 'glyphicon-list', $router->generate('ls_admin_popup')));
    }
    
    public function addToUserGroup(AdminRightsBlock $parent) {
        $bundle = new AdminRightsBundle('Moduł reklamowy Popup');
        $parent->addBundle($bundle);

        $bundle->addRoute(new AdminRightsRoute("Edytowanie", 'ls_admin_popup'));
    }
}

