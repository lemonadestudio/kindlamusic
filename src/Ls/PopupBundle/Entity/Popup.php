<?php

namespace Ls\PopupBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Popup
 * @ORM\Table(name="popup")
 * @ORM\Entity
 */
class Popup
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $btnUrl;
	
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $btnTitle;
	
    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @var string
     */
    private $btnBlank;
	
	/**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $btnUrl2;
	
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $btnTitle2;
	
    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @var string
     */
    private $btnBlank2;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $photo;
	
	private $file;

    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set btnUrl
     *
     * @param string $btnUrl
     * @return Popup
     */
    public function setBtnUrl($btnUrl)
    {
        $this->btnUrl = $btnUrl;

        return $this;
    }

    /**
     * Get btnUrl
     *
     * @return string
     */
    public function getBtnUrl()
    {
        return $this->btnUrl;
    }
	
    /**
     * Set btnTitle
     *
     * @param string $btnTitle
     * @return Popup
     */
    public function setBtnTitle($btnTitle)
    {
        $this->btnTitle = $btnTitle;

        return $this;
    }

    /**
     * Get btnTitle
     *
     * @return string
     */
    public function getBtnTitle()
    {
        return $this->btnTitle;
    }
	
	/**
     * Set btnBlank
     *
     * @param string $btnBlank
     * @return Popup
     */
    public function setBtnBlank($btnBlank)
    {
        $this->btnBlank = $btnBlank;

        return $this;
    }

    /**
     * Get btnBlank
     *
     * @return string
     */
    public function getBtnBlank()
    {
        return $this->btnBlank;
    }

	/**
     * Set btnUrl2
     *
     * @param string $btnUrl2
     * @return Popup
     */
    public function setBtnUrl2($btnUrl2)
    {
        $this->btnUrl2 = $btnUrl2;

        return $this;
    }

    /**
     * Get btnUrl2
     *
     * @return string
     */
    public function getBtnUrl2()
    {
        return $this->btnUrl2;
    }
	
    /**
     * Set btnTitle2
     *
     * @param string $btnTitle2
     * @return Popup
     */
    public function setBtnTitle2($btnTitle2)
    {
        $this->btnTitle2 = $btnTitle2;

        return $this;
    }

    /**
     * Get btnTitle2
     *
     * @return string
     */
    public function getBtnTitle2()
    {
        return $this->btnTitle2;
    }
	
	/**
     * Set btnBlank2
     *
     * @param string $btnBlank2
     * @return Popup
     */
    public function setBtnBlank2($btnBlank2)
    {
        $this->btnBlank2 = $btnBlank2;

        return $this;
    }

    /**
     * Get btnBlank2
     *
     * @return string
     */
    public function getBtnBlank2()
    {
        return $this->btnBlank2;
    }

    /**
     * Set photo
     *
     * @param string $photo
     * @return News
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get photo
     *
     * @return string
     */
    public function getPhoto()
    {
        return $this->photo;
    }
    
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
    }

    public function getFile()
    {
        return $this->file;
    }

    public function deletePhoto()
    {
        if (!empty($this->photo)) {
            $filename = $this->getPhotoAbsolutePath();
            if (file_exists($filename)) {
                @unlink($filename);
            }
        }
    }

    public function getPhotoAbsolutePath()
    {
        return empty($this->photo) ? null : $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->photo;
    }

    public function getPhotoWebPath()
    {
        return empty($this->photo) ? null : '/' . $this->getUploadDir() . '/' . $this->photo;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . $this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
        return 'upload/popup';
    }

    public function upload()
    {
        if (null === $this->file) {
            return;
        }

        $sFileName = $this->getPhoto();

        $this->file->move($this->getUploadRootDir(), $sFileName);

        unset($this->file);
    }
}