<?php

namespace Ls\PopupBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class PopupType extends AbstractType {
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        parent::buildForm($builder, $options);
        
        $builder->add('btnUrl', null, array(
            'label' => 'Adres Url (zielony przycisk)',
        ));
		
        $builder->add('btnTitle', null, array(
            'label' => 'Nazwa (zielony przycisk)',
        ));
			
        $builder->add('btnBlank', null, array(
            'label' => 'Otwierać w nowym oknie (zielony przycisk)',
        ));
		
		$builder->add('btnUrl2', null, array(
            'label' => 'Adres Url (biały przycisk)',
        ));
		
        $builder->add('btnTitle2', null, array(
            'label' => 'Nazwa (biały przycisk)',
        ));
			
        $builder->add('btnBlank2', null, array(
            'label' => 'Otwierać w nowym oknie (biały przycisk)',
        ));

        $builder->add('file', FileType::class, array(
            'label' => 'Nowe zdjęcie',
        ));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Ls\PopupBundle\Entity\Popup',
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix() {
        return 'form_admin_popup';
    }
}
