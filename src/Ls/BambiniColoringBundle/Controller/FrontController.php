<?php

namespace Ls\BambiniColoringBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * BambiniColoringPage controller.
 *
 */
class FrontController extends Controller
{
    public function indexAction($categorySlug)
    {
        $em = $this->getDoctrine()->getManager();

        $category = $em->createQueryBuilder()
            ->select('p')
            ->from('LsBambiniColoringBundle:BambiniColoring', 'p')
            ->where('p.slug = :slug')
            ->setParameter('slug', $categorySlug)
            ->getQuery()
            ->getOneOrNullResult();

        if (null === $category) {
            throw $this->createNotFoundException('Unable to find BambiniColoring entity.');
        }

        $categories = $em->createQueryBuilder()
            ->select('e')
            ->from('LsBambiniColoringBundle:BambiniColoring', 'e')
            ->orderBy('e.title', 'asc')
            ->getQuery()
            ->getResult();
        
        return $this->render('LsBambiniColoringBundle:Front:index.html.twig', array(
            'category' => $category,
            'categories' => $categories
        ));
    }
    
    public function showAction($categorySlug, $slug)
    {
        $em = $this->getDoctrine()->getManager();

        $category = $em->createQueryBuilder()
            ->select('p')
            ->from('LsBambiniColoringBundle:BambiniColoring', 'p')
            ->where('p.slug = :slug')
            ->setParameter('slug', $categorySlug)
            ->getQuery()
            ->getOneOrNullResult();

        if (null === $category) {
            throw $this->createNotFoundException('Unable to find BambiniColoring entity.');
        }

        $categories = $em->createQueryBuilder()
            ->select('e')
            ->from('LsBambiniColoringBundle:BambiniColoring', 'e')
            ->orderBy('e.title', 'asc')
            ->getQuery()
            ->getResult();
        
        $entity = $em->createQueryBuilder()
            ->select('pi')
            ->from('LsBambiniColoringBundle:BambiniColoringPhoto', 'pi')
            ->where('pi.id = :id')
            ->setParameter('id', $slug)
            ->getQuery()
            ->getOneOrNullResult();
        
        if ($entity) {
            if ($entity->getSlug()) {
                return $this->redirectToRoute('ls_bambini_coloring_show', ['categorySlug' => $categorySlug, 'slug' => $entity->getSlug()]);
            }
        } else {
            $entity = $em->createQueryBuilder()
                ->select('ps')
                ->from('LsBambiniColoringBundle:BambiniColoringPhoto', 'ps')
                ->where('ps.slug = :slug')
                ->setParameter('slug', $slug)
                ->getQuery()
                ->getOneOrNullResult();
            
            if (null === $entity) {
                throw $this->createNotFoundException('Unable to find BambiniColoringPhoto entity.');
            }
        }
        
        $other = $em->createQueryBuilder()
            ->select('o')
            ->addSelect('RAND() as HIDDEN rand')
            ->from('LsBambiniColoringBundle:BambiniColoringPhoto', 'o')
            ->where('o.id != :id')
            ->andWhere('o.category = :category')
            ->setParameter('id', $entity->getId())
            ->setParameter('category', $category)
            ->setMaxResults(3)
            ->orderBy('rand()')
            ->getQuery()
            ->getResult();
        
        return $this->render('LsBambiniColoringBundle:Front:show.html.twig', array(
            'category' => $category,
            'categories' => $categories,
            'entity' => $entity,
            'other' => $other
        ));
    }
}
