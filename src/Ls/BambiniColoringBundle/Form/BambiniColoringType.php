<?php

namespace Ls\BambiniColoringBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class BambiniColoringType extends AbstractType {
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        parent::buildForm($builder, $options);
        $builder->add('title', null, array(
            'label' => 'Tytuł',
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Wypełnij pole'
                ))
            )
        ));
        
        $builder->add('slug', null, array(
            'label' => 'Końcówka adresu URL'
        ));
        
        $builder->add('description', null, array(
            'label' => 'Krótki opis (opcjonalnie)'
        ));
        
        $builder->add('seo_generate', null, array(
            'label' => 'Generuj automatycznie'
        ));
        
        $builder->add('seo_title', null, array(
            'label' => 'Tag "title"'
        ));
        
        $builder->add('seo_keywords', TextareaType::class, array(
            'label' => 'Meta "keywords"'
        ));
        
        $builder->add('seo_description', TextareaType::class, array(
            'label' => 'Meta "description"',
            'attr' => array(
                'rows' => 3
            )
        ));
        
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $object = $event->getData();
            $form = $event->getForm();

            if (!$object || null === $object->getId()) {
                $form->add('file', FileType::class, array(
                    'label' => 'Nowe zdjęcie',
                    'constraints' => array(
                        new NotBlank(array(
                            'message' => 'Wybierz zdjęcie'
                        ))
                    )
                ));
            } else {
                $form->add('file', FileType::class, array(
                    'label' => 'Nowe zdjęcie',
                    'constraints' => array(
                    )
                ));
            }
        });
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Ls\BambiniColoringBundle\Entity\BambiniColoring',
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix() {
        return 'form_admin_bambini_coloring';
    }
}
