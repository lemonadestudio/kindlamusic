<?php

namespace Ls\BambiniColoringBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Ls\CoreBundle\Utils\Tools;
use Ls\BambiniColoringBundle\Entity\BambiniColoring;
use Ls\BambiniColoringBundle\Entity\BambiniColoringPhoto;

class BambiniColoringUpdater implements EventSubscriber {

    public function getSubscribedEvents() {
        return array(
            'prePersist',
            'preUpdate',
            'postRemove',
        );
    }

    public function prePersist(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof BambiniColoring) {
            if (!$entity->getCreatedAt()) {
                $entity->setCreatedAt(new \DateTime());
            }
            
            if (!$entity->getSlug()) {
                $slug = Tools::slugify(Tools::strip_pl($entity->getTitle()));
                $entity->setSlug($slug);
            }
            
            if ($entity->getSeoGenerate()) {
                // description
                // usunięcie ewentualnego tagu style wraz z jego zawartością
                $description = preg_replace('/<style.+\/style>/su', '', $entity->getDescription());
                $description = strip_tags($description);
                $description = html_entity_decode($description, ENT_COMPAT | ENT_HTML401, 'UTF-8');
                // usunięcie nowych linii i podwójnych białych znaków
                $description = preg_replace('/\s\s+/u', ' ', $description);
                $description = trim($description);
                // skrócenie do około 160 znaków rekomendowanych przez Google
                $description = Tools::truncateWord($description, 155, '...');

                // keywords
                $keywords_arr = explode(' ', $entity->getTitle() . ' ' . $description);

                $keywords = array();
                if (is_array($keywords_arr)) {
                    foreach ($keywords_arr as $kw) {
                        $kw = trim($kw);
                        $kw = preg_replace('/[\.,;\'\"]/', '', $kw);
                        if (strlen($kw) >= 4 && !in_array($kw, $keywords)) {
                            $keywords[] = $kw;
                        }
                        if (count($keywords) >= 10) {
                            break;
                        }
                    }
                }

                $entity->setSeoTitle(strip_tags($entity->getTitle()));
                $entity->setSeoDescription($description);
                $entity->setSeoKeywords(implode(', ', $keywords));
            }
        }
        
        if ($entity instanceof BambiniColoringPhoto) {
            if (!$entity->getCreatedAt()) {
                $entity->setCreatedAt(new \DateTime());
            }
            
            if (!$entity->getSlug()) {
                $slug = Tools::slugify(Tools::strip_pl($entity->getTitle()));
                $entity->setSlug($slug);
            }
            
            if ($entity->getSeoGenerate()) {
                // description
                // usunięcie ewentualnego tagu style wraz z jego zawartością
                $description = preg_replace('/<style.+\/style>/su', '', $entity->getDescription());
                $description = strip_tags($description);
                $description = html_entity_decode($description, ENT_COMPAT | ENT_HTML401, 'UTF-8');
                // usunięcie nowych linii i podwójnych białych znaków
                $description = preg_replace('/\s\s+/u', ' ', $description);
                $description = trim($description);
                // skrócenie do około 160 znaków rekomendowanych przez Google
                $description = Tools::truncateWord($description, 155, '...');

                // keywords
                $keywords_arr = explode(' ', $entity->getTitle() . ' ' . $description);

                $keywords = array();
                if (is_array($keywords_arr)) {
                    foreach ($keywords_arr as $kw) {
                        $kw = trim($kw);
                        $kw = preg_replace('/[\.,;\'\"]/', '', $kw);
                        if (strlen($kw) >= 4 && !in_array($kw, $keywords)) {
                            $keywords[] = $kw;
                        }
                        if (count($keywords) >= 10) {
                            break;
                        }
                    }
                }

                $entity->setSeoTitle(strip_tags($entity->getTitle()));
                $entity->setSeoDescription($description);
                $entity->setSeoKeywords(implode(', ', $keywords));
            }
            
            if (null === $entity->getArrangement()) {
                $category = $entity->getCategory();
                $query = $em->createQueryBuilder()
                    ->select('COUNT(c.id)')
                    ->from('LsBambiniColoringBundle:BambiniColoringPhoto', 'c')
                    ->where('c.category = :category')
                    ->setParameter('category', $category)
                    ->getQuery();

                $total = $query->getSingleScalarResult();
                $arrangement = $total + 1;
                $entity->setArrangement($arrangement);
            }
        }
    }

    public function preUpdate(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof BambiniColoring) {
            $entity->setUpdatedAt(new \DateTime());
            
            $slug = Tools::slugify(Tools::strip_pl($entity->getTitle()));
            $entity->setSlug($slug);
            
            if ($entity->getSeoGenerate()) {
                // description
                // usunięcie ewentualnego tagu style wraz z jego zawartością
                $description = preg_replace('/<style.+\/style>/su', '', $entity->getDescription());
                $description = strip_tags($description);
                $description = html_entity_decode($description, ENT_COMPAT | ENT_HTML401, 'UTF-8');
                // usunięcie nowych linii i podwójnych białych znaków
                $description = preg_replace('/\s\s+/u', ' ', $description);
                $description = trim($description);
                // skrócenie do około 160 znaków rekomendowanych przez Google
                $description = Tools::truncateWord($description, 155, '...');

                // keywords
                $keywords_arr = explode(' ', $entity->getTitle() . ' ' . $description);

                $keywords = array();
                if (is_array($keywords_arr)) {
                    foreach ($keywords_arr as $kw) {
                        $kw = trim($kw);
                        $kw = preg_replace('/[\.,;\'\"]/', '', $kw);
                        if (strlen($kw) >= 4 && !in_array($kw, $keywords)) {
                            $keywords[] = $kw;
                        }
                        if (count($keywords) >= 10) {
                            break;
                        }
                    }
                }

                $entity->setSeoTitle(strip_tags($entity->getTitle()));
                $entity->setSeoDescription($description);
                $entity->setSeoKeywords(implode(', ', $keywords));
            }
        }
        
        if ($entity instanceof BambiniColoringPhoto) {
            $entity->setUpdatedAt(new \DateTime());
            
            $slug = Tools::slugify(Tools::strip_pl($entity->getTitle()));
            $entity->setSlug($slug);
            
            if ($entity->getSeoGenerate()) {
                // description
                // usunięcie ewentualnego tagu style wraz z jego zawartością
                $description = preg_replace('/<style.+\/style>/su', '', $entity->getDescription());
                $description = strip_tags($description);
                $description = html_entity_decode($description, ENT_COMPAT | ENT_HTML401, 'UTF-8');
                // usunięcie nowych linii i podwójnych białych znaków
                $description = preg_replace('/\s\s+/u', ' ', $description);
                $description = trim($description);
                // skrócenie do około 160 znaków rekomendowanych przez Google
                $description = Tools::truncateWord($description, 155, '...');

                // keywords
                $keywords_arr = explode(' ', $entity->getTitle() . ' ' . $description);

                $keywords = array();
                if (is_array($keywords_arr)) {
                    foreach ($keywords_arr as $kw) {
                        $kw = trim($kw);
                        $kw = preg_replace('/[\.,;\'\"]/', '', $kw);
                        if (strlen($kw) >= 4 && !in_array($kw, $keywords)) {
                            $keywords[] = $kw;
                        }
                        if (count($keywords) >= 10) {
                            break;
                        }
                    }
                }

                $entity->setSeoTitle(strip_tags($entity->getTitle()));
                $entity->setSeoDescription($description);
                $entity->setSeoKeywords(implode(', ', $keywords));
            }
            
            if (null === $entity->getArrangement()) {
                $category = $entity->getCategory();
                $query = $em->createQueryBuilder()
                    ->select('COUNT(c.id)')
                    ->from('LsBambiniColoringBundle:BambiniColoringPhoto', 'c')
                    ->where('c.category = :category')
                    ->setParameter('category', $category)
                    ->getQuery();

                $total = $query->getSingleScalarResult();
                $arrangement = $total + 1;
                $entity->setArrangement($arrangement);
            }
        }
    }

    public function postRemove(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof BambiniColoring) {
            $entity->deletePhoto();
        }
        
        if ($entity instanceof BambiniColoringPhoto) {
            if (!isset($_SESSION['stopupdate'])) {
                $arrangement = $entity->getArrangement();
                $category = $entity->getCategory();

                $query = $em->createQueryBuilder()
                    ->select('c')
                    ->from('LsBambiniColoringBundle:BambiniColoringPhoto', 'c')
                    ->where('c.arrangement > :arrangement')
                    ->andWhere('c.category = :category')
                    ->setParameter('arrangement', $arrangement)
                    ->setParameter('category', $category)
                    ->getQuery();

                $items = $query->getArrayResult();
                $ids = array();
                foreach ($items as $item) {
                    $ids[] = $item['id'];
                }
                $c = 0;
                if (isset($_SESSION['updateKolejnosc'])) {
                    $c = count($_SESSION['updateKolejnosc']);
                }
                $_SESSION['updateKolejnosc'][$c]['class'] = 'LsBambiniColoringBundle:BambiniColoringPhoto';
                $_SESSION['updateKolejnosc'][$c]['ids'] = $ids;
            }
            $entity->deletePhoto();
        }
    }
}