<?php

namespace Ls\AudioPlayerBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Validator\Constraints\NotBlank;

class AudioFileType extends AbstractType {
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('title', null, array(
            'label' => 'Tytuł',
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Wypełnij pole'
                ))
            )
        ));
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $entity = $event->getData();
            $form = $event->getForm();

            if (!$entity || null === $entity->getId()) {
                $form->add('file', HiddenType::class, array(
                    'label' => 'Plik',
                    'mapped' => false,
                    'data' => 0,
                    'required' => false,
                    'error_bubbling' => false,
                    'constraints' => array(
                        new GreaterThan(array(
                            'value' => 0,
                            'message' => 'Dodaj plik'
                        ))
                    )
                ));
            } else {
                $form->add('file', HiddenType::class, array(
                    'label' => 'Plik',
                    'mapped' => false,
                    'data' => 0,
                    'required' => false,
                    'error_bubbling' => false,
                ));
            }
        });
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Ls\AudioPlayerBundle\Entity\AudioFile',
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix() {
        return 'form_admin_audio_file';
    }
}
