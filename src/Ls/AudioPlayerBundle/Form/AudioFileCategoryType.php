<?php

namespace Ls\AudioPlayerBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;

class AudioFileCategoryType extends AbstractType {
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('title', null, array(
            'label' => 'Nazwa kategorii',
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Wypełnij pole'
                ))
            )
        ));
        
        $builder->add('users', EntityType::class, array(
            'label' => 'Lista użytkowników',
            'class' => 'LsUserBundle:User',
            'multiple' => true,
            'required' => true,
            'choice_label' => 'username',
            'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('u')
                          ->orderBy('u.username', 'ASC');
            }
        ));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Ls\AudioPlayerBundle\Entity\AudioFileCategory',
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix() {
        return 'form_admin_audio_file_category';
    }
}
