<?php

namespace Ls\AudioPlayerBundle\Controller;

use Ls\AudioPlayerBundle\Entity\AudioFile;
use Ls\AudioPlayerBundle\Form\AudioFileType;
use Ls\TempFileBundle\Entity\TempFile;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AdminController extends Controller {
    private $pager_limit_name = 'admin_audio_player_pager_limit';

    public function indexAction(Request $request, $categoryId) {
        $em = $this->getDoctrine()->getManager();
        $session = $this->container->get('session');

        $category = $em->getRepository('LsAudioPlayerBundle:AudioFileCategory')->find($categoryId);
        if (!$category) {
            throw $this->createNotFoundException('Unable to find AudioFileCategory entity.');
        }
        
        $page = $request->query->get('page', 1);
        if ($session->has($this->pager_limit_name)) {
            $limit = $session->get($this->pager_limit_name);
        } else {
            $limit = 15;
            $session->set($this->pager_limit_name, $limit);
        }

        $query = $em->createQueryBuilder()
            ->select('e')
            ->from('LsAudioPlayerBundle:AudioFile', 'e')
            ->where('e.category = :category')
            ->setParameter(':category', $category)
            ->orderBy('e.arrangement', 'ASC')
            ->getQuery();

        $paginator = $this->get('knp_paginator');
        $entities = $paginator->paginate(
            $query,
            $page,
            $limit
        );
        $entities->setTemplate('LsCoreBundle:Backend:paginator.html.twig');

        if ($page > $entities->getPageCount() && $entities->getPageCount() > 0) {
            return $this->redirect($this->generateUrl('ls_admin_audio_player', ['categoryId' => $categoryId]));
        }

        $total = $em->createQueryBuilder()
            ->select('COUNT(c.id)')
            ->from('LsAudioPlayerBundle:AudioFile', 'c')
            ->where('c.category = :category')
            ->setParameter(':category', $category)
            ->getQuery()
            ->getSingleScalarResult();

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
        $breadcrumbs->addItem($category->getTitle(), $this->get('router')->generate('ls_admin_audio_player_category'));
        $breadcrumbs->addItem('Pliki audio', $this->get('router')->generate('ls_admin_audio_player', ['categoryId' => $categoryId]));

        return $this->render('LsAudioPlayerBundle:Admin:index.html.twig', array(
            'page' => $page,
            'limit' => $limit,
            'total' => $total,
            'entities' => $entities,
            'categoryId' => $categoryId,
        ));
    }

    public function newAction(Request $request, $categoryId) {
        $em = $this->getDoctrine()->getManager();

        $category = $em->getRepository('LsAudioPlayerBundle:AudioFileCategory')->find($categoryId);
        if (!$category) {
            throw $this->createNotFoundException('Unable to find AudioFileCategory entity.');
        }
        
        $entity = new AudioFile();
        $entity->setCategory($category);

        $form = $this->createForm(AudioFileType::class, $entity, array(
            'action' => $this->generateUrl('ls_admin_audio_player_new', ['categoryId' => $categoryId]),
            'method' => 'POST'
        ));
        $form->add('submit', SubmitType::class, array('label' => 'Zapisz'));
        $form->add('submit_and_list', SubmitType::class, array('label' => 'Zapisz i wróć na listę'));
        $form->add('submit_and_new', SubmitType::class, array('label' => 'Zapisz i dodaj następny'));

        $form->handleRequest($request);
        if ($form->isValid()) {
            $file_id = $form->get('file')->getData();
            if (null !== $file_id && $file_id > 0) {
                $oTempFile = $em->getRepository('LsTempFileBundle:TempFile')->findOneById($file_id);

                $entity->setFilename(mb_convert_case($oTempFile->getFilename(), MB_CASE_LOWER, 'UTF-8'));

                if (!is_dir($entity->getUploadRootDir())) {
                    $old_umask = umask(0);
                    mkdir($entity->getUploadRootDir(), 0777);
                    umask($old_umask);
                }

                @rename($oTempFile->getFileAbsolutePath(), $entity->getFileAbsolutePath());

                $oTempFile->deleteFile();
                $em->remove($oTempFile);
                $em->flush();
            }

            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Dodanie pliku zakończone sukcesem.');

            if ($form->get('submit')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_audio_player_edit', array('categoryId' => $categoryId, 'id' => $entity->getId())));
            }
            if ($form->get('submit_and_list')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_audio_player', ['categoryId' => $categoryId]));
            }
            if ($form->get('submit_and_new')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_audio_player_new', ['categoryId' => $categoryId]));
            }
        }
        if ($form->isSubmitted()) {
            $this->get('session')->getFlashBag()->add('error', 'Sprawdź pola formularza.');
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
        $breadcrumbs->addItem($category->getTitle(), $this->get('router')->generate('ls_admin_audio_player_category'));
        $breadcrumbs->addItem('Pliki audio', $this->get('router')->generate('ls_admin_audio_player', ['categoryId' => $categoryId]));
        $breadcrumbs->addItem($entity->__toString(), $this->get('router')->generate('ls_admin_audio_player_new', ['categoryId' => $categoryId]));

        return $this->render('LsAudioPlayerBundle:Admin:new.html.twig', array(
            'form' => $form->createView(),
            'temp_file_folder' => TempFile::getUploadRootDir(),
            'entity' => $entity,
            'categoryId' => $categoryId,
        ));
    }

    public function editAction(Request $request, $categoryId, $id) {
        $em = $this->getDoctrine()->getManager();

        $category = $em->getRepository('LsAudioPlayerBundle:AudioFileCategory')->find($categoryId);
        if (!$category) {
            throw $this->createNotFoundException('Unable to find AudioFileCategory entity.');
        }
        
        $entity = $em->getRepository('LsAudioPlayerBundle:AudioFile')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find AudioFile entity.');
        }

        $form = $this->createForm(AudioFileType::class, $entity, array(
            'action' => $this->generateUrl('ls_admin_audio_player_edit', array('categoryId' => $categoryId, 'id' => $entity->getId())),
            'method' => 'POST'
        ));
        $form->add('submit', SubmitType::class, array('label' => 'Zapisz zmiany'));
        $form->add('submit_and_list', SubmitType::class, array('label' => 'Zapisz zmiany i zamknij'));

        $form->handleRequest($request);
        if ($form->isValid()) {
            $file_id = $form->get('file')->getData();
            if (null !== $file_id && $file_id > 0) {
                $oTempFile = $em->getRepository('LsTempFileBundle:TempFile')->findOneById($file_id);

                $entity->deleteFile();
                $entity->setFilename(mb_convert_case($oTempFile->getFilename(), MB_CASE_LOWER, 'UTF-8'));

                if (!is_dir($entity->getUploadRootDir())) {
                    $old_umask = umask(0);
                    mkdir($entity->getUploadRootDir(), 0777);
                    umask($old_umask);
                }

                @rename($oTempFile->getFileAbsolutePath(), $entity->getFileAbsolutePath());

                $oTempFile->deleteFile();
                $em->remove($oTempFile);
                $em->flush();
            }

            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Aktualizacja aktualności zakończona sukcesem.');

            if ($form->get('submit')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_audio_player_edit', array('categoryId' => $categoryId, 'id' => $entity->getId())));
            }
            if ($form->get('submit_and_list')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_audio_player', ['categoryId' => $categoryId]));
            }
        }
        if ($form->isSubmitted()) {
            $this->get('session')->getFlashBag()->add('error', 'Sprawdź pola formularza.');
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
        $breadcrumbs->addItem($category->getTitle(), $this->get('router')->generate('ls_admin_audio_player_category'));
        $breadcrumbs->addItem('Pliki audio', $this->get('router')->generate('ls_admin_audio_player', ['categoryId' => $categoryId]));
        $breadcrumbs->addItem($entity->__toString(), $this->get('router')->generate('ls_admin_audio_player_edit', array('categoryId' => $categoryId, 'id' => $entity->getId())));

        return $this->render('LsAudioPlayerBundle:Admin:edit.html.twig', array(
            'form' => $form->createView(),
            'entity' => $entity,
            'categoryId' => $categoryId,
        ));
    }

    public function deleteAction($categoryId, $id) {
        $em = $this->getDoctrine()->getManager();

        $category = $em->getRepository('LsAudioPlayerBundle:AudioFileCategory')->find($categoryId);
        if (!$category) {
            throw $this->createNotFoundException('Unable to find AudioFileCategory entity.');
        }
        
        $entity = $em->getRepository('LsAudioPlayerBundle:AudioFile')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find AudioFile entity.');
        }

        $em->remove($entity);
        $em->flush();

        $this->get('session')->getFlashBag()->add('success', 'Usunięcie aktualności zakończone sukcesem.');

        return new Response('OK');
    }

    private function getMaxKolejnosc($categoryId) {
        $em = $this->getDoctrine()->getManager();
        
        $category = $em->getRepository('LsAudioPlayerBundle:AudioFileCategory')->find($categoryId);
        if (!$category) {
            throw $this->createNotFoundException('Unable to find AudioFileCategory entity.');
        }
        
        $query = $em->createQueryBuilder()
            ->select('COUNT(c.id)')
            ->from('LsAudioPlayerBundle:AudioFile', 'c')
            ->where('c.category = :category')
            ->setParameter(':category', $category)
            ->getQuery();

        $total = $query->getSingleScalarResult();
        return $total + 1;
    }

    public function upAction($categoryId, $id) {
        $em = $this->getDoctrine()->getManager();

        $category = $em->getRepository('LsAudioPlayerBundle:AudioFileCategory')->find($categoryId);
        if (!$category) {
            throw $this->createNotFoundException('Unable to find AudioFileCategory entity.');
        }
        
        $target = $em->getRepository('LsAudioPlayerBundle:AudioFile')->find($id);

        if (!$target) {
            throw $this->createNotFoundException('Unable to find AudioFile entity.');
        }

        $old_kolejnosc = $target->getArrangement();
        $new_kolejnosc = $old_kolejnosc - 1;
        if ($new_kolejnosc > 0) {
            $em = $this->getDoctrine()->getManager();
            $query = $em->createQueryBuilder()
                ->select('c.id')
                ->from('LsAudioPlayerBundle:AudioFile', 'c')
                ->where('c.arrangement = :arrangement')
                ->andWhere('c.category = :category')
                ->setParameter(':category', $category)
                ->setParameter('arrangement', $new_kolejnosc)
                ->getQuery();

            $entity_id = $query->getSingleScalarResult();
            $entity = $em->getRepository('LsAudioPlayerBundle:AudioFile')->findOneById($entity_id);
            $entity->setArrangement(0);
            $em->persist($entity);
            $em->flush();
            $target->setArrangement($new_kolejnosc);
            $em->persist($target);
            $em->flush();
            $entity->setArrangement($old_kolejnosc);
            $em->persist($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('ls_admin_audio_player', ['categoryId' => $categoryId]));
    }

    public function downAction($categoryId, $id) {
        $em = $this->getDoctrine()->getManager();

        $category = $em->getRepository('LsAudioPlayerBundle:AudioFileCategory')->find($categoryId);
        if (!$category) {
            throw $this->createNotFoundException('Unable to find AudioFileCategory entity.');
        }
        
        $target = $em->getRepository('LsAudioPlayerBundle:AudioFile')->find($id);

        if (!$target) {
            throw $this->createNotFoundException('Unable to find AudioFile entity.');
        }

        $max = $this->getMaxKolejnosc($categoryId);
        $old_kolejnosc = $target->getArrangement();
        $new_kolejnosc = $old_kolejnosc + 1;
        if ($new_kolejnosc < $max) {
            $em = $this->getDoctrine()->getManager();
            $query = $em->createQueryBuilder()
                ->select('c.id')
                ->from('LsAudioPlayerBundle:AudioFile', 'c')
                ->where('c.arrangement = :arrangement')
                ->andWhere('c.category = :category')
                ->setParameter(':category', $category)
                ->setParameter('arrangement', $new_kolejnosc)
                ->getQuery();

            $entity_id = $query->getSingleScalarResult();
            $entity = $em->getRepository('LsAudioPlayerBundle:AudioFile')->findOneById($entity_id);
            $entity->setArrangement(0);
            $em->persist($entity);
            $em->flush();
            $target->setArrangement($new_kolejnosc);
            $em->persist($target);
            $em->flush();
            $entity->setArrangement($old_kolejnosc);
            $em->persist($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('ls_admin_audio_player', ['categoryId' => $categoryId]));
    }

    public function batchAction(Request $request, $categoryId) {
        $ids = $request->request->get('ids');
        $action = $request->request->get('action');

        if (is_array($ids) && count($ids) > 0) {
            $elements = count($ids);
            $message = 'Czy na pewno chcesz ';
            switch ($action) {
                case 'delete':
                    $message .= 'usunąć ';
                    break;
            }
            $message .= $elements . ' ';
            switch ($elements) {
                case 1:
                    $message .= 'element?';
                    break;
                case 2:
                case 3:
                case 4:
                    $message .= 'elementy?';
                    break;
                default:
                    $message .= 'elementów?';
                    break;
            }

            $breadcrumbs = $this->get("white_october_breadcrumbs");
            $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
            $breadcrumbs->addItem($category->getTitle(), $this->get('router')->generate('ls_admin_audio_player_category'));
            $breadcrumbs->addItem('Pliki audio', $this->get('router')->generate('ls_admin_audio_player', ['categoryId' => $categoryId]));
            $breadcrumbs->addItem('Potwierdzenie', $this->get('router')->generate('ls_admin_audio_player_batch', ['categoryId' => $categoryId]));

            return $this->render('LsAudioPlayerBundle:Admin:batch.html.twig', array(
                'message' => $message,
                'action' => $action,
                'ids' => implode(',', $ids),
                'categoryId' => $categoryId,
            ));
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Nie wybrałeś żadnych elementów.');
            return $this->redirect($this->generateUrl('ls_admin_audio_player', ['categoryId' => $categoryId]));
        }
    }

    public function batchExecuteAction(Request $request, $categoryId) {
        $em = $this->getDoctrine()->getManager();

        $category = $em->getRepository('LsAudioPlayerBundle:AudioFileCategory')->find($categoryId);
        if (!$category) {
            throw $this->createNotFoundException('Unable to find AudioFileCategory entity.');
        }
        
        $ids = explode(',', $request->request->get('ids'));
        $action = $request->request->get('action');

        if (is_array($ids) && count($ids) > 0) {
            $elements = count($ids);
            $message = '';
            switch ($action) {
                case 'delete':
                    $message .= 'Usunięcie ';
                    $qb = $em->createQueryBuilder();
                    $query = $qb->select('e')
                        ->from('LsAudioPlayerBundle:AudioFile', 'e')
                        ->add('where', $qb->expr()->in('e.id', $ids))
                        ->getQuery();

                    $iterableResult = $query->iterate();
                    while (($row = $iterableResult->next()) !== false) {
                        $em->remove($row[0]);
                        $em->flush();
                    }
                    break;
            }
            $message .= $elements . ' ';
            switch ($elements) {
                case 1:
                    $message .= 'elementu ';
                    break;
                default:
                    $message .= 'elementów ';
                    break;
            }
            $message .= 'zakończone sukcesem ';

            $this->get('session')->getFlashBag()->add('success', $message);
            return $this->redirect($this->generateUrl('ls_admin_audio_player', ['categoryId' => $categoryId]));
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Nie wybrałeś żadnych elementów.');
            return $this->redirect($this->generateUrl('ls_admin_audio_player', ['categoryId' => $categoryId]));
        }
    }

    public function downloadsAction(Request $request, $categoryId, $id) {
        $em = $this->getDoctrine()->getManager();

        $category = $em->getRepository('LsAudioPlayerBundle:AudioFileCategory')->find($categoryId);
        if (!$category) {
            throw $this->createNotFoundException('Unable to find AudioFileCategory entity.');
        }
        
        $entity = $em->getRepository('LsAudioPlayerBundle:AudioFile')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find AudioFile entity.');
        }

        $page = $request->query->get('page', 1);
        $limit = 15;

        $query = $em->createQueryBuilder()
            ->select('e')
            ->from('LsAudioPlayerBundle:AudioFileDownload', 'e')
            ->where('e.audio_file = :audio_file')
            ->setParameter('audio_file', $entity->getId())
            ->getQuery();

        $paginator = $this->get('knp_paginator');
        $entities = $paginator->paginate(
            $query,
            $page,
            $limit
        );
        $entities->setTemplate('LsCoreBundle:Backend:paginator.html.twig');

        if ($page > $entities->getPageCount() && $entities->getPageCount() > 0) {
            return $this->redirect($this->get('router')->generate('ls_admin_audio_player_downloads', array('categoryId' => $categoryId, 'id' => $entity->getId())));
        }

        $total = $em->createQueryBuilder()
            ->select('COUNT(c.id)')
            ->from('LsAudioPlayerBundle:AudioFileDownload', 'c')
            ->getQuery()
            ->getSingleScalarResult();

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
        $breadcrumbs->addItem($category->getTitle(), $this->get('router')->generate('ls_admin_audio_player_category'));
        $breadcrumbs->addItem('Pliki audio', $this->get('router')->generate('ls_admin_audio_player', ['categoryId' => $categoryId]));
        $breadcrumbs->addItem($entity->__toString(), $this->get('router')->generate('ls_admin_audio_player_edit', array('categoryId' => $categoryId, 'id' => $entity->getId())));
        $breadcrumbs->addItem('Statystyki pobrań', $this->get('router')->generate('ls_admin_audio_player_downloads', array('categoryId' => $categoryId, 'id' => $entity->getId())));

        return $this->render('LsAudioPlayerBundle:Admin:downloads.html.twig', array(
            'page' => $page,
            'limit' => $limit,
            'total' => $total,
            'entities' => $entities,
            'categoryId' => $categoryId,
        ));
    }

    public function setLimitAction(Request $request, $categoryId) {
        $session = $this->container->get('session');

        $limit = $request->request->get('limit');
        $session->set($this->pager_limit_name, $limit);

        return new Response('OK');
    }
}
