<?php

namespace Ls\AudioPlayerBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Ls\AudioPlayerBundle\Entity\AudioFile;
use Ls\AudioPlayerBundle\Entity\AudioFileCategory;

class AudioFileUpdater implements EventSubscriber {

    public function getSubscribedEvents() {
        return array(
            'prePersist',
            'preUpdate',
            'postRemove',
        );
    }

    public function prePersist(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof AudioFile) {
            if (null === $entity->getArrangement()) {
                $category = $entity->getCategory();
                $query = $em->createQueryBuilder()
                    ->select('COUNT(c.id)')
                    ->from('LsAudioPlayerBundle:AudioFile', 'c')
                    ->where('c.category = :category')
                    ->setParameter(':category', $category)
                    ->getQuery();

                $total = $query->getSingleScalarResult();
                $arrangement = $total + 1;
                $entity->setArrangement($arrangement);
            }
        }
        
        if ($entity instanceof AudioFileCategory) {
            if (null === $entity->getArrangement()) {
                $query = $em->createQueryBuilder()
                    ->select('COUNT(c.id)')
                    ->from('LsAudioPlayerBundle:AudioFileCategory', 'c')
                    ->getQuery();

                $total = $query->getSingleScalarResult();
                $arrangement = $total + 1;
                $entity->setArrangement($arrangement);
            }
        }
    }

    public function preUpdate(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof AudioFile) {
            if (null === $entity->getArrangement()) {
                $category = $entity->getCategory();
                $query = $em->createQueryBuilder()
                    ->select('COUNT(c.id)')
                    ->from('LsAudioPlayerBundle:AudioFile', 'c')
                    ->where('c.category = :category')
                    ->setParameter(':category', $category)
                    ->getQuery();

                $total = $query->getSingleScalarResult();
                $arrangement = $total + 1;
                $entity->setArrangement($arrangement);
            }
        }
        
        if ($entity instanceof AudioFileCategory) {
            if (null === $entity->getArrangement()) {
                $query = $em->createQueryBuilder()
                    ->select('COUNT(c.id)')
                    ->from('LsAudioPlayerBundle:AudioFileCategory', 'c')
                    ->getQuery();

                $total = $query->getSingleScalarResult();
                $arrangement = $total + 1;
                $entity->setArrangement($arrangement);
            }
        }
    }

    public function postRemove(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof AudioFile) {
            if (!isset($_SESSION['stopupdate'])) {
                $arrangement = $entity->getArrangement();
                $category = $entity->getCategory();
                $query = $em->createQueryBuilder()
                    ->select('c')
                    ->from('LsAudioPlayerBundle:AudioFile', 'c')
                    ->where('c.arrangement > :arrangement')
                    ->andWhere('c.category = :category')
                    ->setParameter(':category', $category)
                    ->setParameter('arrangement', $arrangement)
                    ->getQuery();

                $items = $query->getArrayResult();
                $ids = array();
                foreach ($items as $item) {
                    $ids[] = $item['id'];
                }
                $c = 0;
                if (isset($_SESSION['updateKolejnosc'])) {
                    $c = count($_SESSION['updateKolejnosc']);
                }
                $_SESSION['updateKolejnosc'][$c]['class'] = 'LsAudioPlayerBundle:AudioFile';
                $_SESSION['updateKolejnosc'][$c]['ids'] = $ids;
            }
            $entity->deleteFile();
        }
        
        if ($entity instanceof AudioFileCategory) {
            if (!isset($_SESSION['stopupdate'])) {
                $arrangement = $entity->getArrangement();
                $query = $em->createQueryBuilder()
                    ->select('c')
                    ->from('LsAudioPlayerBundle:AudioFileCategory', 'c')
                    ->where('c.arrangement > :arrangement')
                    ->setParameter('arrangement', $arrangement)
                    ->getQuery();

                $items = $query->getArrayResult();
                $ids = array();
                foreach ($items as $item) {
                    $ids[] = $item['id'];
                }
                $c = 0;
                if (isset($_SESSION['updateKolejnosc'])) {
                    $c = count($_SESSION['updateKolejnosc']);
                }
                $_SESSION['updateKolejnosc'][$c]['class'] = 'LsAudioPlayerBundle:AudioFileCategory';
                $_SESSION['updateKolejnosc'][$c]['ids'] = $ids;
            }
        }
    }
}