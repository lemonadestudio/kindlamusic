<?php

namespace Ls\AudioPlayerBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Ls\AudioPlayerBundle\Entity\AudioFile;
use Ls\UserBundle\Entity\User;

/**
 * AudioFileCategory
 *
 * @ORM\Entity
 * @ORM\Table(name="audio_file_category")
 */
class AudioFileCategory {

    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $publicId;

    /**
     * @ORM\Column(type="integer")
     * @var integer
     */
    private $arrangement;

    /**
     * @ORM\OneToMany(targetEntity="Ls\AudioPlayerBundle\Entity\AudioFile", mappedBy="category")
     * @var \Doctrine\Common\Collections\Collection
     */
    private $audioFiles;
    
    /**
     * @ORM\ManyToMany(targetEntity="\Ls\UserBundle\Entity\User", inversedBy="categories")
     * @ORM\JoinTable(name="categories_users",
     *      joinColumns={@ORM\JoinColumn(name="category_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")}
     *      )
     */
    private $users;

    /**
     * Constructor
     */
    public function __construct() {
        $this->audioFiles = new ArrayCollection();
        $this->users = new ArrayCollection();
        $this->publicId = $this->generateRandomString(10);
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return AudioFileCategory
     */
    public function setTitle($title) {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle() {
        return $this->title;
    }

    public function setPublicId($publicId) {
        $this->publicId = $publicId;

        return $this;
    }

    public function getPublicId() {
        return $this->publicId;
    }

    /**
     * Set arrangement
     *
     * @param string $arrangement
     * @return AudioFileCategory
     */
    public function setArrangement($arrangement) {
        $this->arrangement = $arrangement;

        return $this;
    }

    /**
     * Get arrangement
     *
     * @return string
     */
    public function getArrangement() {
        return $this->arrangement;
    }

    /**
     * Add audioFile
     *
     * @param \Ls\AudioPlayerBundle\Entity\AudioFile $audioFile
     * @return AudioFileCategory
     */
    public function addAudioFile(AudioFile $audioFile) {
        $this->audioFiles[] = $audioFile;

        return $this;
    }

    /**
     * Remove audioFile
     *
     * @param \Ls\AudioPlayerBundle\Entity\AudioFile $audioFiles
     */
    public function removeAudioFile(AudioFile $audioFile) {
        $this->audioFiles->removeElement($audioFile);
    }

    /**
     * Get audioFiles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAudioFiles() {
        return $this->audioFiles;
    }
    
    /**
     * Add user
     *
     * @param \Ls\UserBundle\Entity\User $user
     */
    public function addUser(User $user) {
        if (!$this->users->contains($user)) {
            $this->users->add($user);
        }
            
        return $this;
    }
    
    /**
     * Remove user
     *
     * @param \Ls\UserBundle\Entity\User $user
     */
    public function removeUser(User $user) {
        $this->users->removeElement($user);
    }

    /**
     * Get users
     *
     * @return \Ls\UserBundle\Entity\User
     */
    public function getUsers() {
        return $this->users;
    }

    public function __toString() {
        if (is_null($this->getTitle())) {
            return 'NULL';
        }
        return $this->getTitle();
    }

    public function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}