<?php

namespace Ls\AudioPlayerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AudioFileDownload
 *
 * @ORM\Entity
 * @ORM\Table(name="audio_file_download")
 */
class AudioFileDownload {

    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $created_at;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Ls\AudioPlayerBundle\Entity\AudioFile",
     *     inversedBy="downloads"
     * )
     * @ORM\JoinColumn(
     *     name="audio_file_id",
     *     referencedColumnName="id",
     *     onDelete="SET NULL"
     * )
     * @var \Ls\AudioPlayerBundle\Entity\AudioFile
     *
     */
    private $audio_file;

    /**
     * Constructor
     */
    public function __construct() {
        $this->created_at = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return AudioFileDownload
     */
    public function setEmail($email) {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $created_at
     * @return AudioFileDownload
     */
    public function setCreatedAt($created_at) {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime
     */
    public function getCreatedAt() {
        return $this->created_at;
    }

    /**
     * Set audio_file
     *
     * @param \Ls\AudioPlayerBundle\Entity\AudioFile $audio_file
     * @return AudioFileDownload
     */
    public function setAudioFile(AudioFile $audio_file = null) {
        $this->audio_file = $audio_file;

        return $this;
    }

    /**
     * Get audio_file
     *
     * @return \Ls\AudioPlayerBundle\Entity\AudioFile
     */
    public function getAudioFile() {
        return $this->audio_file;
    }

    public function __toString() {
        if (is_null($this->getEmail())) {
            return 'NULL';
        }
        return $this->getEmail();
    }
}