<?php

namespace Ls\NewsletterBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class SubscribeFormType extends AbstractType {
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder->add('email', null, array(
            'label' => 'Adres e-mail:',
            'required' => true,
            'attr' => array(
                'placeholder' => 'Podaj adres e-mail',
            ),
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Wypełnij pole'
                )),
                new Email(array(
                    'message' => 'Podaj poprawny adres e-mail'
                ))
            )
        ));

        $builder->add('accept', CheckboxType::class, array(
            'required' => true,
            'mapped' => false,
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Wypełnij pole'
                )),
            )
        ));
        $builder->add('submit', SubmitType::class, array(
            )
        );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array());
    }

    /**
     * @return string
     */
    public function getBlockPrefix() {
        return 'form_subscribe';
    }
}
