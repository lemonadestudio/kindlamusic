<?php

namespace Ls\NewsletterBundle\Form;

use Symfony\Component\Form\AbstractType;
use Ls\CoreBundle\Form\DataTransformer\DateTimeTransformer;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;

class NewsletterMessageType extends AbstractType {
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        parent::buildForm($builder, $options);
        $builder->add('title', null, array(
            'label' => 'Tytuł',
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Wypełnij pole'
                ))
            )
        ));
        
        $builder->add('recipient', EntityType::class, array(
            'label' => 'Subskrybenci',
            'class' => 'LsNewsletterBundle:NewsletterSubscribes',
            'multiple' => true,
            'required' => true,
            'choice_label' => 'email',
            'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('m')
                          ->orderBy('m.id', 'ASC');
            },
            'choice_attr' => function($choice, $key, $value) {
                if ($choice->getCategory() != null) {
                    return ['data-category' => $choice->getCategory()->getId()];
                } else {
                    return ['data-category' => 0];
                }
            },
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Wypełnij pole'
                ))
            )
        ));
       
        $builder->add('startSendDate', TextType::class, array(
            'label' => 'Data rozpoczęcia wysyłania',
            'attr' => array(
                'data-date-format' => 'DD.MM.YYYY HH:mm'
            )
        ));
        $builder->get('startSendDate')->addModelTransformer(new DateTimeTransformer());

        $builder->add('message', TextareaType::class, array(
            'label' => 'Treść',
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Wypełnij pole'
                ))
            )
        ));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Ls\NewsletterBundle\Entity\NewsletterMessage',
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix() {
        return 'form_admin_newsletter_message';
    }
}