<?php

namespace Ls\NewsletterBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;

class NewsletterSubscribesType extends AbstractType {
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder->add('email', null, array(
            'label' => 'Adres e-mail',
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Wypełnij pole'
                )),
                new Email(array(
                    'message' => 'Podaj poprawny adres e-mail'
                ))
            ),
            
        ));

        $builder->add('category', EntityType::class, array(
            'label' => 'Kategoria',
            'class' => 'LsNewsletterBundle:NewsletterSubscribesCategory',
            'multiple' => false,
            'required' => false,
            'choice_label' => 'title',
            'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('m')
                          ->orderBy('m.title', 'asc');
            }
        ));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Ls\NewsletterBundle\Entity\NewsletterSubscribes',
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix() {
        return 'form_admin_newsletter_subscribes';
    }
}
