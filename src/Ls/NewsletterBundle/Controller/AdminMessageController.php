<?php

namespace Ls\NewsletterBundle\Controller;

use Ls\NewsletterBundle\Entity\NewsletterMessage;
use Ls\NewsletterBundle\Form\NewsletterMessageType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AdminMessageController extends Controller {
    private $pager_limit_name = 'admin_newsletter_message_pager_limit';

    public function indexAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $session = $this->container->get('session');

        $page = $request->query->get('page', 1);
        if ($session->has($this->pager_limit_name)) {
            $limit = $session->get($this->pager_limit_name);
        } else {
            $limit = 15;
            $session->set($this->pager_limit_name, $limit);
        }

        $query = $em->createQueryBuilder()
            ->select('e')
            ->from('LsNewsletterBundle:NewsletterMessage', 'e')
            ->getQuery();

        $paginator = $this->get('knp_paginator');
        $entities = $paginator->paginate(
            $query,
            $page,
            $limit,
            array(
                'defaultSortFieldName' => 'e.created_at',
                'defaultSortDirection' => 'desc',
            )
        );
        $entities->setTemplate('LsCoreBundle:Backend:paginator.html.twig');

        if ($page > $entities->getPageCount() && $entities->getPageCount() > 0) {
            return $this->redirect($this->generateUrl('ls_admin_newsletter_message'));
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
        $breadcrumbs->addItem('Wiadomości', $this->get('router')->generate('ls_admin_newsletter_message'));

        return $this->render('LsNewsletterBundle:AdminMessage:index.html.twig', array(
            'page' => $page,
            'limit' => $limit,
            'entities' => $entities,
        ));
    }

    public function newAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $entity = new NewsletterMessage();
        
        $templates = $em->createQueryBuilder()
            ->select('t')
            ->from('LsNewsletterBundle:NewsletterTemplate', 't')
            ->orderBy('t.id', 'desc')
            ->getQuery()
            ->getResult();
        
        if (!$templates) {
            $templates = null;
        }

        $form = $this->createForm(NewsletterMessageType::class, $entity, array(
            'action' => $this->generateUrl('ls_admin_newsletter_message_new'),
            'method' => 'POST'
        ));
        $form->add('submit', SubmitType::class, array('label' => 'Zapisz'));
        $form->add('submit_and_list', SubmitType::class, array('label' => 'Zapisz i wróć na listę'));
        $form->add('submit_and_new', SubmitType::class, array('label' => 'Zapisz i dodaj następny'));

        $form->handleRequest($request);
        if ($form->isValid()) {
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Dodanie zakończone sukcesem.');

            if ($form->get('submit')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_newsletter_message_edit', array('id' => $entity->getId())));
            }
            if ($form->get('submit_and_list')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_newsletter_message'));
            }
            if ($form->get('submit_and_new')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_newsletter_message_new'));
            }
        }
        if ($form->isSubmitted()) {
            $this->get('session')->getFlashBag()->add('error', 'Sprawdź pola formularza.');
        }

        $categories = $em->getRepository('LsNewsletterBundle:NewsletterSubscribesCategory')->findAll();
        
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
        $breadcrumbs->addItem('Wiadomości', $this->get('router')->generate('ls_admin_newsletter_message'));
        $breadcrumbs->addItem($entity->__toString(), $this->get('router')->generate('ls_admin_newsletter_message_new'));

        return $this->render('LsNewsletterBundle:AdminMessage:new.html.twig', array(
            'form' => $form->createView(),
            'templates' => $templates,
            'categories' => $categories
        ));
    }

    public function editAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LsNewsletterBundle:NewsletterMessage')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find NewsletterSubscribes entity.');
        }
        
        $templates = $em->createQueryBuilder()
            ->select('t')
            ->from('LsNewsletterBundle:NewsletterTemplate', 't')
            ->orderBy('t.id', 'desc')
            ->getQuery()
            ->getResult();
        
        if (!$templates) {
            $templates = null;
        }
        
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
        $breadcrumbs->addItem('Wiadomości', $this->get('router')->generate('ls_admin_newsletter_message'));
        $breadcrumbs->addItem($entity->__toString(), $this->get('router')->generate('ls_admin_newsletter_message_edit', array('id' => $id)));

        return $this->render('LsNewsletterBundle:AdminMessage:show.html.twig', array(
            'entity' => $entity,
            'templates' => $templates,
            'categories' => $categories
        ));
    }

    public function deleteAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LsNewsletterBundle:NewsletterMessage')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find NewsletterMessage entity.');
        }

        $em->remove($entity);
        $em->flush();

        $this->get('session')->getFlashBag()->add('success', 'Usunięcie zakończone sukcesem.');

        return new Response('OK');
    }

    public function batchAction(Request $request) {
        $ids = $request->request->get('ids');
        $action = $request->request->get('action');

        if (is_array($ids) && count($ids) > 0) {
            $elements = count($ids);
            $message = 'Czy na pewno chcesz ';
            switch ($action) {
                case 'delete':
                    $message .= 'usunąć ';
                    break;
            }
            $message .= $elements . ' ';
            switch ($elements) {
                case 1:
                    $message .= 'element?';
                    break;
                case 2:
                case 3:
                case 4:
                    $message .= 'elementy?';
                    break;
                default:
                    $message .= 'elementów?';
                    break;
            }

            $breadcrumbs = $this->get("white_october_breadcrumbs");
            $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
            $breadcrumbs->addItem('Wiadomości', $this->get('router')->generate('ls_admin_newsletter_message'));
            $breadcrumbs->addItem('Potwierdzenie', $this->get('router')->generate('ls_admin_newsletter_message_batch'));

            return $this->render('LsNewsletterBundle:AdminMessage:batch.html.twig', array(
                'message' => $message,
                'action' => $action,
                'ids' => implode(',', $ids),
            ));
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Nie wybrałeś żadnych elementów.');
            return $this->redirect($this->generateUrl('ls_admin_newsletter_message'));
        }
    }

    public function batchExecuteAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $ids = explode(',', $request->request->get('ids'));
        $action = $request->request->get('action');

        if (is_array($ids) && count($ids) > 0) {
            $elements = count($ids);
            $message = '';
            switch ($action) {
                case 'delete':
                    $message .= 'Usunięcie ';
                    $qb = $em->createQueryBuilder();
                    $query = $qb->select('e')
                        ->from('LsNewsletterBundle:NewsletterMessage', 'e')
                        ->add('where', $qb->expr()->in('e.id', $ids))
                        ->getQuery();

                    $iterableResult = $query->iterate();
                    while (($row = $iterableResult->next()) !== false) {
                        $em->remove($row[0]);
                        $em->flush();
                    }
                    break;
            }
            $message .= $elements . ' ';
            switch ($elements) {
                case 1:
                    $message .= 'elementu ';
                    break;
                default:
                    $message .= 'elementów ';
                    break;
            }
            $message .= 'zakończone sukcesem ';

            $this->get('session')->getFlashBag()->add('success', $message);
            return $this->redirect($this->generateUrl('ls_admin_newsletter_message'));
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Nie wybrałeś żadnych elementów.');
            return $this->redirect($this->generateUrl('ls_admin_newsletter_message'));
        }
    }
    
    public function setLimitAction(Request $request) {
        $session = $this->container->get('session');

        $limit = $request->request->get('limit');
        $session->set($this->pager_limit_name, $limit);

        return new Response('OK');
    }
    
    public function getStatusAction(NewsletterMessage $newsletter) {
        $em = $this->getDoctrine()->getManager();
        
        $all = $em->createQueryBuilder()
            ->select('e')
            ->from('LsNewsletterBundle:NewsletterMessageRecipients', 'e')
            ->leftJoin('e.message', 'ms')
            ->where('ms.id = :id')
            ->setParameter(':id', $newsletter->getId())
            ->getQuery()
            ->getResult();
        
        $sent = $em->createQueryBuilder()
            ->select('nmr')
            ->from('LsNewsletterBundle:NewsletterMessageRecipients', 'nmr')
            ->leftJoin('nmr.message', 'm')
            ->where('m.id = :id')
            ->andWhere('nmr.sentDate is NOT NULL')
            ->setParameter(':id', $newsletter->getId())
            ->orderBy('nmr.sentDate', 'desc')
            ->getQuery()
            ->getResult();
        
        if (count($sent) <= 0) {
            $message = 'Jeszcze nie rozpoczęto wysyłania.';
        } elseif (count($all) > count($sent)) {
            $message = 'Wiadomość została wysłana do ' . count($sent) . ' z ' . count($all) . ' odbiorców.';
        } else {            
            $message = 'Wiadomość została wysłana do wszystkich odbiorców.';
        }
        
        return $this->render('LsNewsletterBundle:AdminMessage:status.html.twig', array(
            'message' => $message
        ));
    }
}
