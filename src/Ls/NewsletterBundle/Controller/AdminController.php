<?php

namespace Ls\NewsletterBundle\Controller;

use Ls\NewsletterBundle\Entity\NewsletterSubscribes;
use Ls\NewsletterBundle\Entity\NewsletterSubscribesCategory;
use Ls\NewsletterBundle\Form\NewsletterSubscribesType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\File;

class AdminController extends Controller {
    private $pager_limit_name = 'admin_newsletter_subscribes_pager_limit';
    private $filter_name      = 'admin_newsletter_subscribes_filters';

    public function indexAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $session = $this->container->get('session');

        $page = $request->query->get('page', 1);
        if ($session->has($this->pager_limit_name)) {
            $limit = $session->get($this->pager_limit_name);
        } else {
            $limit = 15;
            $session->set($this->pager_limit_name, $limit);
        }
        
        $filters = array(
            'email' => null,
            'category' => null
        );

        if ($request->isMethod('POST')) {
            $filters['email'] = $request->request->get('filter_email');
            $filters['category'] = $request->request->get('filter_category');                    
            $session->set($this->filter_name, $filters);
        } else {
            if ($session->has($this->filter_name)) {
                $filters = $session->get($this->filter_name);
            }
        }

        $qb = $em->createQueryBuilder();
        $qb->select('e');
        $qb->from('LsNewsletterBundle:NewsletterSubscribes', 'e');
        
        if (strlen($filters['email']) > 0) {
            $words = explode(' ', $filters['email']);
            foreach ($words as $key => $word) {
                $qb->andWhere($qb->expr()->like('LOWER(e.email)', ':email' . $key));
                $qb->setParameter('email' . $key, '%' . mb_convert_case($word, MB_CASE_LOWER, 'utf-8') . '%');
            }
        }
        
        if (strlen($filters['category']) > 0) {
            $qb->leftJoin('e.category', 'c');
            $qb->andWhere('c.id = :categoryId');
            $qb->setParameter('categoryId', $filters['category']);
        }
        
        $query = $qb->getQuery();

        $paginator = $this->get('knp_paginator');
        $entities = $paginator->paginate(
            $query,
            $page,
            $limit,
            array(
                'defaultSortFieldName' => 'e.created_at',
                'defaultSortDirection' => 'desc',
            )
        );
        $entities->setTemplate('LsCoreBundle:Backend:paginator.html.twig');

        if ($page > $entities->getPageCount() && $entities->getPageCount() > 0) {
            return $this->redirect($this->generateUrl('ls_admin_newsletter_subscribes'));
        }
        
        $categories = $em->getRepository('LsNewsletterBundle:NewsletterSubscribesCategory')->findAll();

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
        $breadcrumbs->addItem('Subskrybenci', $this->get('router')->generate('ls_admin_newsletter_subscribes'));

        return $this->render('LsNewsletterBundle:Admin:index.html.twig', array(
            'page' => $page,
            'limit' => $limit,
            'entities' => $entities,
            'filters' => $filters,
            'categories' => $categories,
        ));
    }

    public function newAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $entity = new NewsletterSubscribes();

        $form = $this->createForm(NewsletterSubscribesType::class, $entity, array(
            'action' => $this->generateUrl('ls_admin_newsletter_subscribes_new'),
            'method' => 'POST'
        ));
        $form->add('submit', SubmitType::class, array('label' => 'Zapisz'));
        $form->add('submit_and_list', SubmitType::class, array('label' => 'Zapisz i wróć na listę'));
        $form->add('submit_and_new', SubmitType::class, array('label' => 'Zapisz i dodaj następny'));

        $form->handleRequest($request);
        if ($form->isValid()) {
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Dodanie zakończone sukcesem.');

            if ($form->get('submit')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_newsletter_subscribes_edit', array('id' => $entity->getId())));
            }
            if ($form->get('submit_and_list')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_newsletter_subscribes'));
            }
            if ($form->get('submit_and_new')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_newsletter_subscribes_new'));
            }
        }
        if ($form->isSubmitted()) {
            $this->get('session')->getFlashBag()->add('error', 'Sprawdź pola formularza.');
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
        $breadcrumbs->addItem('Subskrybenci', $this->get('router')->generate('ls_admin_newsletter_subscribes'));
        $breadcrumbs->addItem($entity->__toString(), $this->get('router')->generate('ls_admin_newsletter_subscribes_new'));

        return $this->render('LsNewsletterBundle:Admin:new.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    public function editAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LsNewsletterBundle:NewsletterSubscribes')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find NewsletterSubscribes entity.');
        }

        $form = $this->createForm(NewsletterSubscribesType::class, $entity, array(
            'action' => $this->generateUrl('ls_admin_newsletter_subscribes_edit', array('id' => $entity->getId())),
            'method' => 'POST'
        ));
        $form->add('submit', SubmitType::class, array('label' => 'Zapisz zmiany'));
        $form->add('submit_and_list', SubmitType::class, array('label' => 'Zapisz zmiany i zamknij'));

        $form->handleRequest($request);
        if ($form->isValid()) {
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Aktualizacja użytkownika zakończona sukcesem.');

            if ($form->get('submit')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_newsletter_subscribes_edit', array('id' => $entity->getId())));
            }
            if ($form->get('submit_and_list')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_newsletter_subscribes'));
            }
        }
        if ($form->isSubmitted()) {
            $this->get('session')->getFlashBag()->add('error', 'Sprawdź pola formularza.');
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
        $breadcrumbs->addItem('Subskrybenci', $this->get('router')->generate('ls_admin_newsletter_subscribes'));
        $breadcrumbs->addItem($entity->__toString(), $this->get('router')->generate('ls_admin_newsletter_subscribes_new', array('id' => $id)));

        return $this->render('LsNewsletterBundle:Admin:edit.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));
    }

    public function deleteAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LsNewsletterBundle:NewsletterSubscribes')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find NewsletterSubscribes entity.');
        }

        $em->remove($entity);
        $em->flush();

        $this->get('session')->getFlashBag()->add('success', 'Usunięcie zakończone sukcesem.');

        return new Response('OK');
    }

    public function batchAction(Request $request) {
        $ids = $request->request->get('ids');
        $action = $request->request->get('action');

        if (is_array($ids) && count($ids) > 0) {
            $elements = count($ids);
            $message = 'Czy na pewno chcesz ';
            switch ($action) {
                case 'delete':
                    $message .= 'usunąć ';
                    break;
            }
            $message .= $elements . ' ';
            switch ($elements) {
                case 1:
                    $message .= 'element?';
                    break;
                case 2:
                case 3:
                case 4:
                    $message .= 'elementy?';
                    break;
                default:
                    $message .= 'elementów?';
                    break;
            }

            $breadcrumbs = $this->get("white_october_breadcrumbs");
            $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
            $breadcrumbs->addItem('Subskrybenci', $this->get('router')->generate('ls_admin_newsletter_subscribes'));
            $breadcrumbs->addItem('Potwierdzenie', $this->get('router')->generate('ls_admin_newsletter_subscribes_batch'));

            return $this->render('LsNewsletterBundle:Admin:batch.html.twig', array(
                'message' => $message,
                'action' => $action,
                'ids' => implode(',', $ids),
            ));
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Nie wybrałeś żadnych elementów.');
            return $this->redirect($this->generateUrl('ls_admin_newsletter_subscribes'));
        }
    }

    public function batchExecuteAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $ids = explode(',', $request->request->get('ids'));
        $action = $request->request->get('action');

        if (is_array($ids) && count($ids) > 0) {
            $elements = count($ids);
            $message = '';
            switch ($action) {
                case 'delete':
                    $message .= 'Usunięcie ';
                    $qb = $em->createQueryBuilder();
                    $query = $qb->select('e')
                        ->from('LsNewsletterBundle:NewsletterSubscribes', 'e')
                        ->add('where', $qb->expr()->in('e.id', $ids))
                        ->getQuery();

                    $iterableResult = $query->iterate();
                    while (($row = $iterableResult->next()) !== false) {
                        $em->remove($row[0]);
                        $em->flush();
                    }
                    break;
            }
            $message .= $elements . ' ';
            switch ($elements) {
                case 1:
                    $message .= 'elementu ';
                    break;
                default:
                    $message .= 'elementów ';
                    break;
            }
            $message .= 'zakończone sukcesem ';

            $this->get('session')->getFlashBag()->add('success', $message);
            return $this->redirect($this->generateUrl('ls_admin_newsletter_subscribes'));
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Nie wybrałeś żadnych elementów.');
            return $this->redirect($this->generateUrl('ls_admin_newsletter_subscribes'));
        }
    }
    
    public function setLimitAction(Request $request) {
        $session = $this->container->get('session');

        $limit = $request->request->get('limit');
        $session->set($this->pager_limit_name, $limit);

        return new Response('OK');
    }
    
    public function filterClearAction() {
        $session = $this->container->get('session');

        $filters = array(
            'email' => null,
            'category' => null
        );

        $session->set($this->filter_name, $filters);

        return $this->redirect($this->generateUrl('ls_admin_newsletter_subscribes'));
    }
    
    public function importAction(Request $request) {
        $form = $this->createFormBuilder([]);
        
        $form->add('column', TextType::class, array(
            'label' => 'Litera kolumny, która zawiera adresy email',
            'attr' => [
                'placeholder' => 'Np. F'
            ] 
        ));
        
        $form->add('category', EntityType::class, array(
            'label' => 'Kategoria',
            'class' => 'LsNewsletterBundle:NewsletterSubscribesCategory',
            'multiple' => false,
            'required' => false,
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Wybierz kategorię'
                )),
            ),
            'choice_label' => 'title',
            'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('m')
                          ->orderBy('m.title', 'asc');
            }
        ));
        
        $form->add('file', FileType::class, [
            'label' => 'Plik Excel (.xls, .xlsx)',
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Wypełnij pole'
                )),
                new File()
            ),
            'attr' => [
                'accept' => 'application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
            ]
        ]);
        
        $form->add('submit', SubmitType::class, array('label' => 'Zapisz i dodaj następny'));
        $form->add('submit_and_list', SubmitType::class, array('label' => 'Zapisz i wróć na listę'));
        $form = $form->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $file = $form->get('file')->getData();
            $category = $form->get('category')->getData();
            $columnLetter = $form->get('column')->getData();
            $extension = $file->getClientOriginalExtension();
            $totalCount = 0;
            $newCount = 0;
            $duplicatesCount = 0;
            $invalidCount = 0;
            $invalidCoords = [];
            
            if ($extension != 'xls' && $extension != 'xlsx') {
                $this->get('session')->getFlashBag()->add('error', 'Błędny format pliku');
                return $this->redirect($this->generateUrl('ls_admin_newsletter_subscribes_import'));
            } else {
                $filename = uniqid('file-'). '.' . $extension;
                $filePath = $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $filename;
                $file->move($this->getUploadRootDir(), $filename);
                
                if ($extension == 'xlsx') {
                    require_once __DIR__ . DIRECTORY_SEPARATOR . 'SimpleXLSX.php';
                    $excel = \SimpleXLSX::parse($filePath);
                } else {
                    require_once __DIR__ . DIRECTORY_SEPARATOR . 'SimpleXLS.php';
                    $excel = \SimpleXLS::parse($filePath);
                }

                @unlink($filePath);

                $alphas = range('a', 'z');
                $columnNumber = null;
                if ($columnLetter !=  null) {
                    $columnNumber = array_search(strtolower($columnLetter), $alphas);
                }

                foreach ($excel->rows() as $r => $row) {
                    foreach ($row as $c => $cell) {
                        if ($columnNumber != null && $columnNumber >= 0) {
                            if ($c == $columnNumber) {
                                $result = $this->addEmailAddress($cell, $category);
                                if ($result == 'VALID') {
                                    $newCount++;
                                } else if ($result == 'DUPLICATE') {
                                    $duplicatesCount ++;
                                } else if ($result == 'INVALID') {
                                    $invalidCount ++;
                                }
                                $totalCount++;
                            }
                        } else {
                            $result = $this->addEmailAddress($cell, $category);
                            if ($result == 'VALID') {
                                $newCount++;
                            } else if ($result == 'DUPLICATE') {
                                $duplicatesCount ++;
                            } else if ($result == 'INVALID') {
                                $invalidCount ++;
                            }
                            $totalCount++;
                        }
                    }
                }
            }
            
            $message = "Przeskanowano komórek: " . $totalCount . "<br>";
            $message .= "Dodano nowych adresów e-mail: " . $newCount . "<br>";
            $message .= "Już istnieją w bazie danych: " . $duplicatesCount . "<br>";
            $message .= "Ilośc błędnych wartości: " . $invalidCount . "<br>";
            if (count($invalidCoords) > 0) {
                $message .= "Komórki z błednymi wartościami: " . implode(', ', $invalidCoords);;
            }
            
            $this->get('session')->getFlashBag()->add('success', $message);
            
            if ($form->get('submit')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_newsletter_subscribes_import'));
            }
            if ($form->get('submit_and_list')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_newsletter_subscribes'));
            }
        }

        if ($form->isSubmitted()) {
            $this->get('session')->getFlashBag()->add('error', 'Sprawdź pola formularza.');
        }
        
        return $this->render('LsNewsletterBundle:Admin:import.html.twig', array(
            'form' => $form->createView(),
        ));
    }
    
    private function getUploadRootDir()
    {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . $this->getUploadDir();
    }

    private function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
        return 'upload/newsletter';
    }
    
    private function addEmailAddress($value, NewsletterSubscribesCategory $category = null) {
        $em = $this->getDoctrine()->getManager();
        $value = trim($value, " \t\n\r\0\x0B,.");
        
        if (filter_var($value, FILTER_VALIDATE_EMAIL)) {
            $exists = $em->getRepository(NewsletterSubscribes::class)->findOneBy(['email' => $value]);
            if (!$exists) {
                $entity = new NewsletterSubscribes();
                $entity->setCategory($category);
                $entity->setEmail($value);
                $em->persist($entity);
                $em->flush();
                
                return "VALID";
            } else {
                return "DUPLICATE";
            }
        } else {
            return "INVALID";
        }
    }
}
