<?php

namespace Ls\NewsletterBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Ls\NewsletterBundle\Entity\NewsletterSubscribes;
use Ls\NewsletterBundle\Form\SubscribeFormType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Ls\DictionaryBundle\Entity\Dictionary;
use Ls\NewsletterBundle\Form\UnsubscribeFormType;

/**
 * News controller.
 *
 */
class FrontController extends Controller {
    /**
     * subscribe form
     */
    public function showSubscribeFormAction() {        
        $entity = new NewsletterSubscribes();

        $form = $this->createSubscribeForm($entity);

        return $this->render('LsNewsletterBundle:Front:subscribe_form.html.twig', array(
            'form' => $form->createView(),
        ));
    }
    
    public function handleSubscribeFormAction(Request $request) {
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(array('message' => 'You can access this only using Ajax!'), 400);
        }
        $em = $this->getDoctrine()->getManager();
        
        $emailExistMessage = "Ten adres jest już w naszej bazie";
        $emailExistEntity = $em->getRepository(Dictionary::class)->findOneBy(['label' => 'newsletter_email_exist']);
        if ($emailExistEntity) {
            $emailExistMessage = $emailExistEntity->getTranslation($request->getLocale())->getValue();
        }
        
        $subscriptionAddedMessage = "Subskrypcja została dodana. Dziękujemy!";
        $subscriptionAddedEntity = $em->getRepository(Dictionary::class)->findOneBy(['label' => 'newsletter_email_added']);
        if ($emailExistEntity) {
            $subscriptionAddedMessage = $subscriptionAddedEntity->getTranslation($request->getLocale())->getValue();
        }
        
        
        $entity = new NewsletterSubscribes();

        $form = $this->createSubscribeForm($entity);

        $form->handleRequest($request);
        
        if ($form->isValid()) {
            
            $check_entity = $em->getRepository('LsNewsletterBundle:NewsletterSubscribes')->findOneBy(array('email' => $entity->getEmail()));
            if (!empty($check_entity)) {
                return new JsonResponse(array('message' => $emailExistMessage));
            }
            $entity->setActive(true);
            $em->persist($entity);
            $em->flush();
            
            return new JsonResponse(array('message' => $subscriptionAddedMessage));
        }
        
        $error = $form['email']->getErrors();
        foreach ($error as $value) {
            $error_message = $value->getMessage();
        }

        return new JsonResponse(array('message' => $error_message));
    }
    
    private function createSubscribeForm(NewsletterSubscribes $entity)
    {
        $form = $this->createForm(SubscribeFormType::class, $entity, array(
            'action' => $this->container->get('router')->generate('ls_newsletter_subscribe'),
            'method' => 'PUT',
        ));

        return $form;
    }
    
    public function unsubscribeAction(Request $request, $token) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LsNewsletterBundle:NewsletterSubscribes')->findOneBy(array('token' => $token));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find NewsletterSubscribes entity.');
        }

        $form = $this->createForm(UnsubscribeFormType::class, $entity, array(
            'action' => $this->container->get('router')->generate('ls_newsletter_unsubscribe', ['token' => $token]),
            'method' => 'PUT',
        ));

        $form->handleRequest($request);
        
        if ($form->isValid()) {
        	$em->remove($entity);
        	$em->flush();

        	return $this->redirect($this->get('router')->generate('ls_core_homepage'));
        }

        return $this->render('LsNewsletterBundle:Front:unsubscribe_form.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}
