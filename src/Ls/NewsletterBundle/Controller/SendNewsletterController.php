<?php



namespace Ls\NewsletterBundle\Controller;



use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\DomCrawler\Crawler;



class SendNewsletterController extends Controller {

    public function sendMessagesAction($key, $count = 30, $command = false) {        

        if ($key != "66HATe4nA6BstgGC") {

            throw $this->createNotFoundException('Wrong key.');

        }

        

        $sentMailsCount = 0;

        $recipients = $this->getRecipientsToSendMessage($count);

        foreach ($recipients as $recipient) {

            

            if ($this->sendMessage($recipient)) {

                $sentMailsCount++;

            }

        }

        

        if ($command) {

            return "Ilość wysłanych maili: ".$sentMailsCount;

        }

        

        return new Response("Ilość wysłanych maili: ".$sentMailsCount);

    }

    

    private function sendMessage($recipient) {

        $em = $this->getDoctrine()->getManager();

        $router = $this->container->get('router');

        $baseUrl = $this->container->getParameter('base_url');

        

        $subscriptionDeactivationUrl = $router->generate('ls_newsletter_unsubscribe', array(

            'token' => $recipient->getSubscriber()->getToken()));

        $recipientMail = $recipient->getSubscriber()->getEmail();

        

        if (filter_var($recipientMail, FILTER_VALIDATE_EMAIL)) {

	        $htmlMessage = $this->renderView('LsNewsletterBundle:Front:newsletter_template.html.twig', array(

	            'content' => $recipient->getMessage()->getMessage(),

	            'subscriptionDeactivationUrl' => $baseUrl . $subscriptionDeactivationUrl

	        ));

	        $htmlMessage = $this->modifyImageElements($htmlMessage, $baseUrl);



	        $messageEmail = \Swift_Message::newInstance();

	        $messageEmail->setSubject($recipient->getMessage()->getTitle());

	        $messageEmail->setFrom(array($this->container->getParameter('mailer_user') => $this->container->getParameter('mailer_name')));

	        $messageEmail->setTo($recipientMail);

	        $messageEmail->setReplyTo(['rollsfilm@gmail.com']);

	        $messageEmail->setBody($htmlMessage, 'text/html');

	        $messageEmail->addPart(strip_tags($htmlMessage), 'text/plain');

	        

	        try {

	            $mailer = $this->get('mailer');

	            if ($mailer->send($messageEmail)) {

	                $spool = $mailer->getTransport()->getSpool();

	                $transport = $this->container->get('swiftmailer.transport.real');

	                $spool->flushQueue($transport);



	                $recipient->setSentDate(new \DateTime());

	                $em->persist($recipient);

	                $em->flush();

	                

	                return true;

	            }

	        } catch (Exception $e) {

	           echo ("Błąd wysyłania maila do {$recipientMail}. Treść błędu: {$e->toString()} \n");

	            

	            return false;

	        }

	    } else {

	    	echo ("Błąd wysyłania maila do {$recipientMail}.\n");

	            

	        return false;

	    }

    }



    private function modifyImageElements($htmlMessage, $baseUrl) {

        $document = new \DOMDocument();

        libxml_use_internal_errors(true);

        $document->loadHtml($htmlMessage);

        libxml_use_internal_errors(false);

        $images = $document->getElementsByTagName('img');



        foreach ($images as $domElement) {

            $this->changeImagesUrlToAbsolute($domElement, $baseUrl);

        }



        return $this->getDomDocumentHtml($document);

    }

    

    private function changeImagesUrlToAbsolute($domElement, $baseUrl) {

        if (substr($domElement->getAttribute('src'), 0, 4) != "http") {

            $domElement->setAttribute('src', $baseUrl . $domElement->getAttribute('src'));

        }

    }

    

    private function getDomDocumentHtml($document)

    {

        $crawler = new Crawler();

        $crawler->addDocument($document);

        

        $html = '';



        foreach ($crawler as $domElement) {

            $html .= $domElement->ownerDocument->saveHTML($domElement);

        }



        return $html;

    }

    

    private function getRecipientsToSendMessage($limit = 30) {

        $em = $this->getDoctrine()->getManager();

        

        return $em->createQueryBuilder('r')

                ->from('LsNewsletterBundle:NewsletterMessageRecipients', 'r')

                ->leftJoin('r.message', 'm')

                ->leftJoin('r.recipients', 'rr')

                ->select('r, m', 'rr')

                ->where('r.sentDate IS NULL')

                ->andWhere('m.startSendDate <= :now OR m.startSendDate IS NULL')

                ->orderBy('r.id', 'ASC')

                ->setMaxResults($limit)

                ->setParameter('now', new \DateTime('now'))

                ->getQuery()

                ->execute();

    }

}

