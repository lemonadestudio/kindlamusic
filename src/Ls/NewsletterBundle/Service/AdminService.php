<?php

namespace Ls\NewsletterBundle\Service;

use Knp\Menu\ItemInterface;
use Ls\CoreBundle\Helper\AdminBlock;
use Ls\CoreBundle\Helper\AdminLink;
use Ls\CoreBundle\Helper\AdminRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

class AdminService {
    private $container;

    public function __construct(ContainerInterface $container) {
        $this->container = $container;
    }

    public function addToMenu(ItemInterface $parent, $route, $set) {
        $item = $parent->addChild('Subskrybenci', array(
            'route' => 'ls_admin_newsletter_subscribes',
        ));

        $current_set = true;

        if (!$set) {
            switch ($route) {
                case 'ls_admin_newsletter_subscribes':
                case 'ls_admin_newsletter_subscribes_new':
                case 'ls_admin_newsletter_subscribes_edit':
                case 'ls_admin_newsletter_subscribes_batch':
                    $item->setCurrent(true);
                    break;
                default:
                    $current_set = false;
                    break;
            }
        }

        return $current_set;
    }

    public function addToDashboard(AdminBlock $parent) {
        $router = $this->container->get('router');

        $row = new AdminRow('Subskrybenci');
        $parent->addRow($row);

        $row->addLink(new AdminLink('Dodaj', 'glyphicon-plus', $router->generate('ls_admin_newsletter_subscribes_new')));
        $row->addLink(new AdminLink('Zarządzaj', 'glyphicon-list', $router->generate('ls_admin_newsletter_subscribes')));
        $row->addLink(new AdminLink('Kategorie', 'glyphicon-th', $router->generate('ls_admin_newsletter_subscribes_category')));
    }
}

