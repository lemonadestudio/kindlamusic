<?php
namespace Ls\NewsletterBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SendNewsletterCommand extends ContainerAwareCommand
{    
    protected function configure()
    {
        $this
            ->setName('newsletter:send')
            ->setDescription('Wysyłanie newslettera.')
            ->addArgument('emails-count-to-send', InputArgument::REQUIRED, 'Ilość emaili do jednorazowej wysyłki.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $mailsCountToSend = $input->getArgument('emails-count-to-send');
        $output->writeln('Maksymalna ilość maili do wysłania: ' . $mailsCountToSend);
        
        try {
            $controller = $this->getContainer()->get('ls_newsletter.send_newsletter_controller'); 
            $response = $controller->sendMessagesAction('66HATe4nA6BstgGC', $mailsCountToSend, true);   
            $output->writeln($response);
        } catch (Exception $e) {
            $output->writeln("Błąd wysyłania maila. Treść błędu: {$e->toString()}");
            
            return false;
        }
    }
}
