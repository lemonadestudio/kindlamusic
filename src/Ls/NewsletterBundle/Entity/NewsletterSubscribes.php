<?php

namespace Ls\NewsletterBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Ls\NewsletterBundle\Entity\NewsletterSubscribesCategory;

/**
 * NewsletterSubscribes
 *
 * @ORM\Table(name="newsletter_subscribes")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */

class NewsletterSubscribes
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     */
    private $email;
    
    /**
     * @ORM\Column(name="token", type="string", length=40, nullable=true)
     */
    private $token;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    private $created_at;
    
    /**
     * @ORM\OneToMany(targetEntity="NewsletterMessageRecipients" , mappedBy="recipients" , cascade={"all"})
     * */
    protected $messageRecipients;
    
    /**
     * @ORM\ManyToOne(targetEntity="NewsletterSubscribesCategory", inversedBy="subscribes")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $category;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->created_at = new \DateTime();
    }
    
    public function generateToken($prefix)
    {
        return sha1(uniqid($prefix, true));
    }
    
    /**
     * @ORM\PrePersist()
     */
    public function onPrePersist()
    {
        $this->token = $this->generateToken($this->email);
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set category
     *
     * @param string $category
     *
     * @return NewsletterSubscribes
     */
    public function setCategory(NewsletterSubscribesCategory $category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return NewsletterSubscribesCategory
     */
    public function getCategory()
    {
        return $this->category;
    }
    
    /**
     * Set email
     *
     * @param string $email
     *
     * @return NewsletterSubscribes
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }
    
    /**
     * Set token
     *
     * @param string $token
     * @return NewsletterSubscribes
     */
    public function setToken($token)
    {
        $this->token = $token;
    
        return $this;
    }

    /**
     * Get token
     *
     * @return string 
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return NewsletterSubscribes
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }
    
    public function __toString()
    {
        if (is_null($this->getEmail())) {
            return 'NULL';
        }

        return $this->getEmail();
    }
}
