<?php

namespace Ls\NewsletterBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Ls\NewsletterBundle\Entity\NewsletterSubscribes;

/**
 * NewsletterSubscribes
 *
 * @ORM\Table(name="newsletter_subscribes_category")
 * @ORM\Entity
 */

class NewsletterSubscribesCategory
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    private $created_at;
    
    /**
     * @ORM\OneToMany(targetEntity="NewsletterSubscribes", mappedBy="category")
     */
    private $subscribes;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->created_at = new \DateTime();
        $this->subscribes = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set title
     *
     * @param string $title
     *
     * @return NewsletterSubscribes
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return NewsletterSubscribes
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }
    
    public function __toString()
    {
        if (is_null($this->getTitle())) {
            return 'NULL';
        }

        return $this->getTitle();
    }
    
    /**
     * Add subscribe
     *
     * @param NewsletterSubscribes $subscribe
     */
    public function addSubscribe(NewsletterSubscribes $subscribe) {
        if (!$this->subscribe->contains($subscribe)) {
            $this->subscribe->add($subscribe);
        }
        return $this;
    }
    
    /**
     * Remove subscribe
     *
     * @param NewsletterSubscribes $subscribe
     */
    public function removeSubscribe(NewsletterSubscribes $subscribe) {
        $this->subscribe->removeElement($subscribe);
    }

    public function getSubscribes() {
        return $this->subscribes;
    }
}
