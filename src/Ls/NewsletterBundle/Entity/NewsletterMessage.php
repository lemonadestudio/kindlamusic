<?php

namespace Ls\NewsletterBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Ls\NewsletterBundle\Entity\NewsletterMessageRecipients;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * NewsletterMessage
 *
 * @ORM\Table(name="newsletter_message")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class NewsletterMessage
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     * @Assert\NotNull();
     */
    private $title;
    
    /**
     * @ORM\Column(type="text", nullable=false)
     * @Assert\NotBlank()
     */
    private $message;
    
    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $startSendDate;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;
    
    /**
     * @ORM\OneToMany(targetEntity="NewsletterMessageRecipients", mappedBy="message", cascade={"all"})
     * */
    protected $messageRecipients;
    
    protected $recipients;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->created_at = new \DateTime();
        $this->messageRecipients = new ArrayCollection();
        $this->recipients = new ArrayCollection();
    }
    
    
    public function getRecipientsCount()
    {
        return count($this->getRecipient());
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    
    
    /**
     * Set title
     *
     * @param string $title
     * @return NewsletterMessage
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set message
     *
     * @param string $message
     * @return NewsletterMessage
     */
    public function setMessage($message)
    {
        $this->message = $message;
    
        return $this;
    }

    /**
     * Get message
     *
     * @return string 
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set startSendDate
     *
     * @param \DateTime $startSendDate
     * @return NewsletterMessage
     */
    public function setStartSendDate($startSendDate)
    {
        $this->startSendDate = $startSendDate;
    
        return $this;
    }

    /**
     * Get startSendDate
     *
     * @return \DateTime 
     */
    public function getStartSendDate()
    {
        return $this->startSendDate;
    }

    
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }
    
    public function __toString()
    {
        if (is_null($this->getTitle())) {
            return 'NULL';
        }

        return $this->getTitle();
    }

    // Important 
    public function getRecipient()
    {
        $recipients = new ArrayCollection();
        
        foreach($this->messageRecipients as $mr)
        {
            $recipients[] = $mr->getSubscriber();
        }

        return $recipients;
    }
    // Important
    public function setRecipient($recipients)
    {
        foreach($recipients as $r)
        {
            $nmr = new NewsletterMessageRecipients();

            $nmr->setMessage($this);
            $nmr->setSubscriber($r);

            $this->addMessageRecipients($nmr);
        }

    }
    
    public function getNewsletterMessage()
    {
        return $this;
    }

    public function addMessageRecipients($messageRecipient)
    {
        $this->messageRecipients[] = $messageRecipient;
    }
    
    public function removeMessageRecipients($messageRecipient)
    {
        return $this->messageRecipients->removeElement($messageRecipient);
    }
    
    public function getMessageRecipients()
    {
        return $this->messageRecipients;
    }
}
