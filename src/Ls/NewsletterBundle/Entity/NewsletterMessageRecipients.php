<?php

namespace Ls\NewsletterBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ls\NewsletterBundle\Entity\NewsletterMessage;
use Ls\NewsletterBundle\Entity\NewsletterSubscribes;
use Doctrine\Common\Collections\ArrayCollection;

/**
 *
 * NewsletterMessageRecipients
 *
 * @ORM\Entity
 * @ORM\Table(name="newsletter_message_recipients")
 * @ORM\HasLifecycleCallbacks();
 */
class NewsletterMessageRecipients
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $sentDate;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;
    
    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;
    
    /**
     * @ORM\ManyToOne(targetEntity="NewsletterSubscribes", inversedBy="messageRecipients")
     * @ORM\JoinColumn(name="recipient_id", referencedColumnName="id")
     * */
    protected $recipients;

    /**
     * @ORM\ManyToOne(targetEntity="NewsletterMessage", inversedBy="messageRecipients")
     * @ORM\JoinColumn(name="message_id", referencedColumnName="id")
     * */
    protected $message;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sentDate
     *
     * @param \DateTime $sentDate
     * @return NewsletterMessageRecipients
     */
    public function setSentDate($sentDate)
    {
        $this->sentDate = $sentDate;
    
        return $this;
    }

    /**
     * Get sentDate
     *
     * @return \DateTime 
     */
    public function getSentDate()
    {
        return $this->sentDate;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return NewsletterMessageRecipients
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return NewsletterMessageRecipients
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
    
    /**
     * Set Message
     *
     * @param NewsletterMessage $newsletterMessage
     * @return NewsletterMessageRecipients
     */
    public function setMessage($newsletterMessage)
    {
        $this->message = $newsletterMessage;
    
        return $this;
    }

    /**
     * Get Message
     *
     * @return NewsletterMessage 
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set Subscriber
     *
     * @param NewsletterSubscribes $newsletterSubscriber
     * @return NewsletterMessageRecipients
     */
    public function setSubscriber($newsletterSubscriber)
    {
        $this->recipients = $newsletterSubscriber;
    
        return $this;
    }

    /**
     * Get Subscriber
     *
     * @return NewsletterSubscribes
     */
    public function getSubscriber()
    {
        return $this->recipients;
    }
}