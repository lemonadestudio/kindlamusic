<?php

namespace Ls\GalleryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * GalleryMovie
 * @ORM\Table(name="gallery_movie")
 * @ORM\Entity
 */
class GalleryMovie {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $movie_id;

    /**
     * @ORM\Column(type="integer")
     * @var integer
     */
    private $arrangement;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Gallery",
     *     inversedBy="movies"
     * )
     * @ORM\JoinColumn(
     *     name="gallery_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     * @var \Ls\GalleryBundle\Entity\Gallery
     */
    private $gallery;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set movie_id
     *
     * @param string $movie_id
     * @return GalleryMovie
     */
    public function setMovieId($movie_id) {
        $this->movie_id = $movie_id;

        return $this;
    }

    /**
     * Get movie_id
     *
     * @return string
     */
    public function getMovieId() {
        return $this->movie_id;
    }

    /**
     * Set arrangement
     *
     * @param integer $arrangement
     * @return GalleryMovie
     */
    public function setArrangement($arrangement) {
        $this->arrangement = $arrangement;

        return $this;
    }

    /**
     * Get arrangement
     *
     * @return integer
     */
    public function getArrangement() {
        return $this->arrangement;
    }

    /**
     * Set gallery
     *
     * @param \Ls\GalleryBundle\Entity\Gallery $gallery
     * @return GalleryMovie
     */
    public function setGallery(Gallery $gallery = null) {
        $this->gallery = $gallery;

        return $this;
    }

    /**
     * Get gallery
     *
     * @return \Ls\GalleryBundle\Entity\Gallery
     */
    public function getGallery() {
        return $this->gallery;
    }

    public function __toString() {
        if (is_null($this->getMovieId())) {
            return 'NULL';
        }
        return $this->getMovieId();
    }
}