<?php

namespace Ls\GalleryBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Ls\PageBundle\Entity\Page;
use Ls\ComposerBundle\Entity\Composer;

/**
 * Gallery
 * @ORM\Table(name="gallery")
 * @ORM\Entity
 */
class Gallery {
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $name;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    private $updated_at;

    /**
     * @ORM\OneToMany(
     *   targetEntity="GalleryMovie",
     *   mappedBy="gallery",
     *   cascade={"all"}
     * )
     * @ORM\OrderBy({"arrangement" = "ASC"})
     * @var \Doctrine\Common\Collections\Collection
     */
    private $movies;

    /**
     * @ORM\OneToMany(
     *   targetEntity="Ls\PageBundle\Entity\Page",
     *   mappedBy="gallery"
     * )
     * @var \Doctrine\Common\Collections\Collection
     */
    private $pages;
    
    /**
     * @ORM\OneToMany(
     *   targetEntity="Ls\ComposerBundle\Entity\Composer",
     *   mappedBy="gallery"
     * )
     * @var \Doctrine\Common\Collections\Collection
     */
    private $composers;

    /**
     * @ORM\OneToMany(
     *   targetEntity="Ls\ActorsBundle\Entity\Actors",
     *   mappedBy="gallery"
     * )
     * @var \Doctrine\Common\Collections\Collection
     */
    private $actors;
	
    /**
     * @ORM\OneToMany(
     *   targetEntity="Ls\EventsPageBundle\Entity\EventsPage",
     *   mappedBy="gallery"
     * )
     * @var \Doctrine\Common\Collections\Collection
     */
    private $eventsPages;

    /**
     * Constructor
     */
    public function __construct() {
        $this->created_at = new \DateTime();
        $this->movies = new ArrayCollection();
        $this->pages = new ArrayCollection();
        $this->composers = new ArrayCollection();        
        $this->actors = new ArrayCollection();
		$this->eventsPages = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Gallery
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $created_at
     * @return Gallery
     */
    public function setCreatedAt($created_at) {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime
     */
    public function getCreatedAt() {
        return $this->created_at;
    }

    /**
     * Set updated_at
     *
     * @param \DateTime $updated_at
     * @return Gallery
     */
    public function setUpdatedAt($updated_at) {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime
     */
    public function getUpdatedAt() {
        return $this->updated_at;
    }

    /**
     * Add movie
     *
     * @param \Ls\GalleryBundle\Entity\GalleryMovie $movie
     * @return Gallery
     */
    public function addMovie(GalleryMovie $movie) {
        $movie->setGallery($this);
        $this->movies[] = $movie;

        return $this;
    }

    /**
     * Remove movie
     *
     * @param \Ls\GalleryBundle\Entity\GalleryMovie $movie
     */
    public function removeMovie(GalleryMovie $movie) {
        $this->movies->removeElement($movie);
    }

    /**
     * Get movies
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMovies() {
        return $this->movies;
    }

    /**
     * Add page
     *
     * @param \Ls\PageBundle\Entity\Page $pages
     * @return Gallery
     */
    public function addPage(Page $pages)
    {
        $this->pages[] = $pages;

        return $this;
    }

    /**
     * Remove page
     *
     * @param \Ls\PageBundle\Entity\Page $pages
     */
    public function removePage(Page $pages)
    {
        $this->pages->removeElement($pages);
    }

    /**
     * Get pages
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPages()
    {
        return $this->pages;
    }

    /**
     * Add composer
     *
     * @param \Ls\ComposerBundle\Entity\Composer $composer
     * @return Gallery
     */
    public function addComposer(Composer $composer)
    {
        $this->composers[] = $composer;

        return $this;
    }

    /**
     * Remove composer
     *
     * @param \Ls\ComposerBundle\Entity\Composer $composer
     */
    public function removeComposer(Composer $composer)
    {
        $this->composers->removeElement($composer);
    }

    /**
     * Get composers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getComposers()
    {
        return $this->composers;
    }

    public function __toString() {
        if (is_null($this->getName())) {
            return 'NULL';
        }
        return $this->getName();
    }
}