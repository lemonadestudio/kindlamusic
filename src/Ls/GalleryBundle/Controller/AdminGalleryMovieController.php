<?php

namespace Ls\GalleryBundle\Controller;

use Ls\GalleryBundle\Entity\GalleryMovie;
use Ls\GalleryBundle\Form\GalleryMovieType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AdminGalleryMovieController extends Controller {
    public function indexAction($galleryId) {
        $em = $this->getDoctrine()->getManager();

        $gallery = $em->createQueryBuilder()
            ->select('g', 'p')
            ->from('LsGalleryBundle:Gallery', 'g')
            ->leftJoin('g.movies', 'p')
            ->where('g.id = :id')
            ->setParameter('id', $galleryId)
            ->getQuery()
            ->getOneOrNullResult();

        if (null === $gallery) {
            throw $this->createNotFoundException('Unable to find Gallery entity.');
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
        $breadcrumbs->addItem('Galerie', $this->get('router')->generate('ls_admin_gallery'));
        $breadcrumbs->addItem($gallery->__toString(), $this->get('router')->generate('ls_admin_gallery_edit', array('id' => $galleryId)));
        $breadcrumbs->addItem('Filmy', $this->get('router')->generate('ls_admin_gallery_movie', array('galleryId' => $galleryId)));

        return $this->render('LsGalleryBundle:AdminMovie:index.html.twig', array(
            'gallery' => $gallery,
        ));
    }

    public function newAction(Request $request, $galleryId) {
        $em = $this->getDoctrine()->getManager();

        $gallery = $em->createQueryBuilder()
            ->select('g', 'p')
            ->from('LsGalleryBundle:Gallery', 'g')
            ->leftJoin('g.movies', 'p')
            ->where('g.id = :id')
            ->setParameter('id', $galleryId)
            ->getQuery()
            ->getOneOrNullResult();

        if (null === $gallery) {
            throw $this->createNotFoundException('Unable to find Gallery entity.');
        }

        $entity = new GalleryMovie();

        $form = $this->createForm(GalleryMovieType::class, $entity, array(
            'action' => $this->generateUrl('ls_admin_gallery_movie_new', array('galleryId' => $galleryId)),
            'method' => 'POST'
        ));
        $form->add('submit', SubmitType::class, array('label' => 'Zapisz'));
        $form->add('submit_and_list', SubmitType::class, array('label' => 'Zapisz i wróć na listę'));
        $form->add('submit_and_new', SubmitType::class, array('label' => 'Zapisz i dodaj następny'));

        $form->handleRequest($request);
        if ($form->isValid()) {
            $entity->setGallery($gallery);
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Dodanie filmu zakończone sukcesem.');

            if ($form->get('submit')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_gallery_movie_edit', array('galleryId' => $galleryId, 'id' => $entity->getId())));
            }
            if ($form->get('submit_and_list')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_gallery_movie', array('galleryId' => $galleryId)));
            }
            if ($form->get('submit_and_new')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_gallery_movie_new', array('galleryId' => $galleryId)));
            }
        }
        if ($form->isSubmitted()) {
            $this->get('session')->getFlashBag()->add('error', 'Sprawdź pola formularza.');
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
        $breadcrumbs->addItem('Galerie filmów Youtube', $this->get('router')->generate('ls_admin_gallery'));
        $breadcrumbs->addItem($entity->__toString(), $this->get('router')->generate('ls_admin_gallery_new'));
        $breadcrumbs->addItem('Filmy', $this->get('router')->generate('ls_admin_gallery_movie', array('galleryId' => $galleryId)));
        $breadcrumbs->addItem('Dodaj', $this->get('router')->generate('ls_admin_gallery_movie_new', array('galleryId' => $galleryId)));

        return $this->render('LsGalleryBundle:AdminMovie:new.html.twig', array(
            'form' => $form->createView(),
            'gallery' => $gallery,
        ));
    }

    public function updateArrangementAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $repository = $em->getRepository('LsGalleryBundle:GalleryMovie');

        $items = json_decode($request->request->get('items'));
        $arrangement = 1;

        if (is_array($items)) {
            foreach ($items as $item) {
                $item_id = str_replace('item-', '', $item);
                $entity = $repository->find($item_id);
                if ($entity) {
                    $entity->setArrangement($arrangement);
                    $em->flush();

                    $arrangement++;
                }
            }
        }

        return new Response('OK');
    }

    public function editAction(Request $request, $galleryId, $id) {
        $em = $this->getDoctrine()->getManager();

        $gallery = $em->createQueryBuilder()
            ->select('g', 'p')
            ->from('LsGalleryBundle:Gallery', 'g')
            ->leftJoin('g.movies', 'p')
            ->where('g.id = :id')
            ->setParameter('id', $galleryId)
            ->getQuery()
            ->getOneOrNullResult();

        if (null === $gallery) {
            throw $this->createNotFoundException('Unable to find Gallery entity.');
        }

        $entity = $em->getRepository('LsGalleryBundle:GalleryMovie')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find GalleryMovie entity.');
        }

        $form = $this->createForm(GalleryMovieType::class, $entity, array(
            'action' => $this->generateUrl('ls_admin_gallery_movie_edit', array('galleryId' => $galleryId, 'id' => $entity->getId())),
            'method' => 'POST'
        ));
        $form->add('submit', SubmitType::class, array('label' => 'Zapisz zmiany'));
        $form->add('submit_and_list', SubmitType::class, array('label' => 'Zapisz zmiany i zamknij'));

        $form->handleRequest($request);
        if ($form->isValid()) {
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Aktualizacja filmu zakończona sukcesem.');

            if ($form->get('submit')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_gallery_movie_edit', array('galleryId' => $galleryId, 'id' => $entity->getId())));
            }
            if ($form->get('submit_and_list')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_gallery_movie', array('galleryId' => $galleryId)));
            }
        }
        if ($form->isSubmitted()) {
            $this->get('session')->getFlashBag()->add('error', 'Sprawdź pola formularza.');
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
        $breadcrumbs->addItem('Galerie', $this->get('router')->generate('ls_admin_gallery'));
        $breadcrumbs->addItem($gallery->__toString(), $this->get('router')->generate('ls_admin_gallery_edit', array('id' => $galleryId)));
        $breadcrumbs->addItem('Filmy', $this->get('router')->generate('ls_admin_gallery_movie', array('galleryId' => $galleryId)));
        $breadcrumbs->addItem($entity->__toString(), $this->get('router')->generate('ls_admin_gallery_movie_edit', array('galleryId' => $galleryId, 'id' => $entity->getId())));

        return $this->render('LsGalleryBundle:AdminMovie:edit.html.twig', array(
            'form' => $form->createView(),
            'gallery' => $gallery,
            'entity' => $entity,
        ));
    }

    public function deleteAction($galleryId, $id) {
        $em = $this->getDoctrine()->getManager();

        $gallery = $em->createQueryBuilder()
            ->select('g', 'p')
            ->from('LsGalleryBundle:Gallery', 'g')
            ->leftJoin('g.movies', 'p')
            ->where('g.id = :id')
            ->setParameter('id', $galleryId)
            ->getQuery()
            ->getOneOrNullResult();

        if (null === $gallery) {
            throw $this->createNotFoundException('Unable to find Gallery entity.');
        }

        $entity = $em->getRepository('LsGalleryBundle:GalleryMovie')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find GalleryMovie entity.');
        }

        $em->remove($entity);
        $em->flush();

        $this->get('session')->getFlashBag()->add('success', 'Usunięcie filmu zakończone sukcesem.');

        return new Response('OK');
    }

    public function batchAction(Request $request, $galleryId) {
        $em = $this->getDoctrine()->getManager();

        $gallery = $em->createQueryBuilder()
            ->select('g', 'p')
            ->from('LsGalleryBundle:Gallery', 'g')
            ->leftJoin('g.movies', 'p')
            ->where('g.id = :id')
            ->setParameter('id', $galleryId)
            ->getQuery()
            ->getOneOrNullResult();

        if (null === $gallery) {
            throw $this->createNotFoundException('Unable to find Gallery entity.');
        }

        $ids = $request->request->get('ids');
        $action = $request->request->get('action');

        if (is_array($ids) && count($ids) > 0) {
            $elements = count($ids);
            $message = 'Czy na pewno chcesz ';
            switch ($action) {
                case 'delete':
                    $message .= 'usunąć ';
                    break;
            }
            $message .= $elements . ' ';
            switch ($elements) {
                case 1:
                    $message .= 'element?';
                    break;
                case 2:
                case 3:
                case 4:
                    $message .= 'elementy?';
                    break;
                default:
                    $message .= 'elementów?';
                    break;
            }

            $breadcrumbs = $this->get("white_october_breadcrumbs");
            $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
            $breadcrumbs->addItem('Galerie', $this->get('router')->generate('ls_admin_gallery'));
            $breadcrumbs->addItem($gallery->__toString(), $this->get('router')->generate('ls_admin_gallery_edit', array('id' => $galleryId)));
            $breadcrumbs->addItem('Filmy', $this->get('router')->generate('ls_admin_gallery_movie', array('galleryId' => $galleryId)));
            $breadcrumbs->addItem('Potwierdzenie', $this->get('router')->generate('ls_admin_gallery_movie_batch', array('galleryId' => $galleryId)));

            return $this->render('LsGalleryBundle:AdminMovie:batch.html.twig', array(
                'gallery' => $gallery,
                'message' => $message,
                'action' => $action,
                'ids' => implode(',', $ids),
            ));
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Nie wybrałeś żadnych elementów.');
            return $this->redirect($this->generateUrl('ls_admin_gallery_movie', array('galleryId' => $galleryId)));
        }
    }

    public function batchExecuteAction(Request $request, $galleryId) {
        $em = $this->getDoctrine()->getManager();

        $gallery = $em->createQueryBuilder()
            ->select('g', 'p')
            ->from('LsGalleryBundle:Gallery', 'g')
            ->leftJoin('g.movies', 'p')
            ->where('g.id = :id')
            ->setParameter('id', $galleryId)
            ->getQuery()
            ->getOneOrNullResult();

        if (null === $gallery) {
            throw $this->createNotFoundException('Unable to find Gallery entity.');
        }

        $ids = explode(',', $request->request->get('ids'));
        $action = $request->request->get('action');

        if (is_array($ids) && count($ids) > 0) {
            $elements = count($ids);
            $message = '';
            switch ($action) {
                case 'delete':
                    $message .= 'Usunięcie ';
                    $qb = $em->createQueryBuilder();
                    $query = $qb->select('e')
                        ->from('LsGalleryBundle:GalleryMovie', 'e')
                        ->add('where', $qb->expr()->in('e.id', $ids))
                        ->getQuery();

                    $iterableResult = $query->iterate();
                    while (($row = $iterableResult->next()) !== false) {
                        $em->remove($row[0]);
                        $em->flush();
                    }
                    break;
            }
            $message .= $elements . ' ';
            switch ($elements) {
                case 1:
                    $message .= 'elementu ';
                    break;
                default:
                    $message .= 'elementów ';
                    break;
            }
            $message .= 'zakończone sukcesem ';

            $this->get('session')->getFlashBag()->add('success', $message);
            return $this->redirect($this->generateUrl('ls_admin_gallery_movie', array('galleryId' => $galleryId)));
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Nie wybrałeś żadnych elementów.');
            return $this->redirect($this->generateUrl('ls_admin_gallery_movie', array('galleryId' => $galleryId)));
        }
    }
}
