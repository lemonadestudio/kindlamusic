<?php

namespace Ls\GalleryBundle\Controller;

use Ls\GalleryBundle\Entity\Gallery;
use Ls\GalleryBundle\Form\GalleryType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AdminGalleryController extends Controller {
    private $pager_limit_name = 'admin_gallery_pager_limit';

    public function indexAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $session = $this->container->get('session');

        $page = $request->query->get('page', 1);
        if ($session->has($this->pager_limit_name)) {
            $limit = $session->get($this->pager_limit_name);
        } else {
            $limit = 15;
            $session->set($this->pager_limit_name, $limit);
        }

        $query = $em->createQueryBuilder()
            ->select('e')
            ->from('LsGalleryBundle:Gallery', 'e')
            ->getQuery();

        $paginator = $this->get('knp_paginator');
        $entities = $paginator->paginate(
            $query,
            $page,
            $limit,
            array(
                'defaultSortFieldName' => 'e.name',
                'defaultSortDirection' => 'asc',
            )
        );
        $entities->setTemplate('LsCoreBundle:Backend:paginator.html.twig');

        if ($page > $entities->getPageCount() && $entities->getPageCount() > 0) {
            return $this->redirect($this->generateUrl('ls_admin_gallery'));
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
        $breadcrumbs->addItem('Galerie filmów Youtube', $this->get('router')->generate('ls_admin_gallery'));

        return $this->render('LsGalleryBundle:Admin:index.html.twig', array(
            'page' => $page,
            'limit' => $limit,
            'entities' => $entities,
        ));
    }

    public function newAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $entity = new Gallery();

        $form = $this->createForm(GalleryType::class, $entity, array(
            'action' => $this->generateUrl('ls_admin_gallery_new'),
            'method' => 'POST'
        ));
        $form->add('submit', SubmitType::class, array('label' => 'Zapisz'));
        $form->add('submit_and_list', SubmitType::class, array('label' => 'Zapisz i wróć na listę'));
        $form->add('submit_and_new', SubmitType::class, array('label' => 'Zapisz i dodaj następny'));

        $form->handleRequest($request);
        if ($form->isValid()) {
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Dodanie galerii zakończone sukcesem.');

            if ($form->get('submit')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_gallery_edit', array('id' => $entity->getId())));
            }
            if ($form->get('submit_and_list')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_gallery'));
            }
            if ($form->get('submit_and_new')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_gallery_new'));
            }
        }
        if ($form->isSubmitted()) {
            $this->get('session')->getFlashBag()->add('error', 'Sprawdź pola formularza.');
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
        $breadcrumbs->addItem('Galerie filmów Youtube', $this->get('router')->generate('ls_admin_gallery'));
        $breadcrumbs->addItem($entity->__toString(), $this->get('router')->generate('ls_admin_gallery_new'));

        return $this->render('LsGalleryBundle:Admin:new.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function editAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LsGalleryBundle:Gallery')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Gallery entity.');
        }

        $form = $this->createForm(GalleryType::class, $entity, array(
            'action' => $this->generateUrl('ls_admin_gallery_edit', array('id' => $entity->getId())),
            'method' => 'POST'
        ));
        $form->add('submit', SubmitType::class, array('label' => 'Zapisz zmiany'));
        $form->add('submit_and_list', SubmitType::class, array('label' => 'Zapisz zmiany i zamknij'));

        $form->handleRequest($request);
        if ($form->isValid()) {
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Aktualizacja galerii zakończona sukcesem.');

            if ($form->get('submit')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_gallery_edit', array('id' => $entity->getId())));
            }
            if ($form->get('submit_and_list')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_gallery'));
            }
        }
        if ($form->isSubmitted()) {
            $this->get('session')->getFlashBag()->add('error', 'Sprawdź pola formularza.');
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
        $breadcrumbs->addItem('Galerie filmów Youtube', $this->get('router')->generate('ls_admin_gallery'));
        $breadcrumbs->addItem($entity->__toString(), $this->get('router')->generate('ls_admin_gallery_edit', array('id' => $entity->getId())));

        return $this->render('LsGalleryBundle:Admin:edit.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView()
        ));
    }

    public function deleteAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LsGalleryBundle:Gallery')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Gallery entity.');
        }

        $em->remove($entity);
        $em->flush();

        $this->get('session')->getFlashBag()->add('success', 'Usunięcie galerii zakończone sukcesem.');

        return new Response('OK');
    }

    public function batchAction(Request $request) {
        $ids = $request->request->get('ids');
        $action = $request->request->get('action');

        if (is_array($ids) && count($ids) > 0) {
            $elements = count($ids);
            $message = 'Czy na pewno chcesz ';
            switch ($action) {
                case 'delete':
                    $message .= 'usunąć ';
                    break;
            }
            $message .= $elements . ' ';
            switch ($elements) {
                case 1:
                    $message .= 'element?';
                    break;
                case 2:
                case 3:
                case 4:
                    $message .= 'elementy?';
                    break;
                default:
                    $message .= 'elementów?';
                    break;
            }

            $breadcrumbs = $this->get("white_october_breadcrumbs");
            $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
            $breadcrumbs->addItem('Galerie filmów Youtube', $this->get('router')->generate('ls_admin_gallery'));
            $breadcrumbs->addItem('Potwierdzenie', $this->get('router')->generate('ls_admin_gallery_batch'));

            return $this->render('LsGalleryBundle:Admin:batch.html.twig', array(
                'message' => $message,
                'action' => $action,
                'ids' => implode(',', $ids),
            ));
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Nie wybrałeś żadnych elementów.');
            return $this->redirect($this->generateUrl('ls_admin_gallery'));
        }
    }

    public function batchExecuteAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $ids = explode(',', $request->request->get('ids'));
        $action = $request->request->get('action');

        if (is_array($ids) && count($ids) > 0) {
            $elements = count($ids);
            $message = '';
            switch ($action) {
                case 'delete':
                    $message .= 'Usunięcie ';
                    $qb = $em->createQueryBuilder();
                    $query = $qb->select('e')
                        ->from('LsGalleryBundle:Gallery', 'e')
                        ->add('where', $qb->expr()->in('e.id', $ids))
                        ->getQuery();

                    $iterableResult = $query->iterate();
                    while (($row = $iterableResult->next()) !== false) {
                        $em->remove($row[0]);
                        $em->flush();
                    }
                    break;
            }
            $message .= $elements . ' ';
            switch ($elements) {
                case 1:
                    $message .= 'elementu ';
                    break;
                default:
                    $message .= 'elementów ';
                    break;
            }
            $message .= 'zakończone sukcesem ';

            $this->get('session')->getFlashBag()->add('success', $message);
            return $this->redirect($this->generateUrl('ls_admin_gallery'));
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Nie wybrałeś żadnych elementów.');
            return $this->redirect($this->generateUrl('ls_admin_gallery'));
        }
    }

    public function setLimitAction(Request $request) {
        $session = $this->container->get('session');

        $limit = $request->request->get('limit');
        $session->set($this->pager_limit_name, $limit);

        return new Response('OK');
    }
}
