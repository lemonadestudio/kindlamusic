<?php

namespace Ls\BambiniSettingBundle\Extensions;

use Ls\BambiniSettingBundle\Utils\BambiniConfig;

class BambiniSettingsExtension extends \Twig_Extension implements \Twig_Extension_GlobalsInterface {

    protected $cms_config;

    function __construct(BambiniConfig $config) {
        $this->cms_config = $config;
    }

    public function getGlobals() {
        return array(
            'bambini_cms_config' => $this->cms_config,
            'bambini_settings' => $this->cms_config->all(),
        );
    }

    public function getName() {
        return 'bambini_cms_config';
    }

}