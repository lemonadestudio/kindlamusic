<?php

namespace Ls\MediaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class MediaType extends AbstractType {
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('title', null, array(
            'label' => 'Tytuł',
            'attr' => array(
                'class' => 'form-control'
            ),
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Wypełnij pole'
                ))
            )
        ));
        $builder->add('slug', null, array(
            'label' => 'Końcówka adresu URL',
            'attr' => array(
                'class' => 'form-control'
            )
        ));
        $builder->add('content', null, array(
            'label' => 'Info prasowe - Treść',
            'attr' => array(
                'class' => 'form-control wysiwyg'
            )
        ));
        $builder->add('content2', null, array(
            'label' => 'Bio - Treść',
            'attr' => array(
                'class' => 'form-control wysiwyg'
            )
        ));
        $builder->add('seo_title', null, array(
            'label' => 'Tag "title"',
            'attr' => array(
                'class' => 'form-control'
            )
        ));
        $builder->add('seo_keywords', TextareaType::class, array(
            'label' => 'Meta "keywords"',
            'attr' => array(
                'class' => 'form-control',
                'rows' => 3
            )
        ));
        $builder->add('seo_description', TextareaType::class, array(
            'label' => 'Meta "description"',
            'attr' => array(
                'class' => 'form-control',
                'rows' => 3
            )
        ));
        $builder->add('youtube', null, array(
            'label' => 'Youtube',
            'attr' => array(
                'class' => 'form-control'
            )
        ));
        $builder->add('facebook', null, array(
            'label' => 'Facebook',
            'attr' => array(
                'class' => 'form-control'
            )
        ));
        $builder->add('instagram', null, array(
            'label' => 'Instagram',
            'attr' => array(
                'class' => 'form-control'
            )
        ));
        $builder->add('twitter', null, array(
            'label' => 'Twitter',
            'attr' => array(
                'class' => 'form-control'
            )
        ));
        $builder->add('seo_generate', null, array(
            'label' => 'Generuj automatycznie'
        ));
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $form = $event->getForm();
            $form->add('multipleFiles', FileType::class, array(
                'label' => 'Pliki',
                'multiple' => true,
                'required' => false
            ));
        });
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Ls\MediaBundle\Entity\Media',
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix() {
        return 'form_admin_media';
    }
}
