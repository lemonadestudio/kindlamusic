<?php

namespace Ls\MediaBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Ls\CoreBundle\Utils\Tools;
use Ls\MediaBundle\Entity\Media;

class MediaUpdater implements EventSubscriber {

    public function getSubscribedEvents() {
        return array(
            'prePersist',
            'postPersist',
            'preUpdate',
            'postUpdate',
        );
    }

    public function prePersist(LifecycleEventArgs $args) {
        $entity = $args->getEntity();

        if ($entity instanceof Media) {
            if (!$entity->getCreatedAt()) {
                $entity->setCreatedAt(new \DateTime());
            }
            if (!$entity->getId()) {
                $entity->setSlug(Tools::slugify($entity->getTitle()));
            }
            if ($entity->getSeoGenerate()) {
                // description
                $description = strip_tags($entity->getContent());
                $description = html_entity_decode($entity, ENT_COMPAT | ENT_HTML401, 'UTF-8');
                $description = str_replace("\xC2\xA0", ' ', $description);
                $description = Tools::truncateWord($description, 255, '');

                // usunięcie nowych linii i podwójnych białych znaków
                $description = preg_replace('/\s\s+/', ' ', $description);

                // usunięcie ostatniego, niedokończonego zdania
                $description = preg_replace('@(.*)\..*@', '\1.', $description);

                // trim
                $description = trim($description);

                // keywords
                $keywords_arr = explode(' ', $entity->getTitle() . ' ' . $description);

                $keywords = array();
                if (is_array($keywords_arr)) {
                    foreach ($keywords_arr as $kw) {
                        $kw = trim($kw);
                        $kw = preg_replace('/[\.,;\'\"]/', '', $kw);
                        if (strlen($kw) >= 4 && !in_array($kw, $keywords)) {
                            $keywords[] = $kw;
                        }
                        if (count($keywords) >= 10) {
                            break;
                        }
                    }
                }
                
                $entity->setSeoTitle(strip_tags($entity->getTitle()));
                $entity->setSeoDescription($description);
                $entity->setSeoKeywords(implode(', ', $keywords));
                $entity->setSeoGenerate(false);
            }
        }
    }

    public function postPersist(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof Media) {
//            if (strcmp($entity->getOldSlug(), $entity->getSlug()) !== 0) {
//                if (null !== $entity->getOldSlug()) {
//                    $query = $em->createQueryBuilder()
//                        ->select('c')
//                        ->from('LsMenuBundle:MenuItem', 'c')
//                        ->where('c.route LIKE :route')
//                        ->andWhere('c.route_parameters LIKE :slug')
//                        ->setParameter('route', 'ls_media_show')
//                        ->setParameter('slug', '%' . $entity->getOldSlug() . '%')
//                        ->getQuery();
//
//                    $items = $query->getResult();
//
//                    foreach ($items as $item) {
//                        $item->setRouteParameters('{"slug":"' . $entity->getSlug() . '"}');
//                        $em->persist($item);
//                    }
//                    $em->flush();
//
//                    $entity->setOldSlug($entity->getSlug());
//                    $em->persist($entity);
//                    $em->flush();
//                } else {
//                    $entity->setOldSlug($entity->getSlug());
//                    $em->persist($entity);
//                    $em->flush();
//                }
//            }
        }
    }

    public function preUpdate(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof Media) {
            $entity->setUpdatedAt(new \DateTime());
            if ($entity->getSeoGenerate()) {
                // description
                $description = strip_tags($entity->getContent());
                $description = html_entity_decode($description, ENT_COMPAT | ENT_HTML401, 'UTF-8');
                $description = str_replace("\xC2\xA0", ' ', $description);
                $description = Tools::truncateWord($description, 255, '');

                // usunięcie nowych linii i podwójnych białych znaków
                $description = preg_replace('/\s\s+/', ' ', $description);

                // usunięcie ostatniego, niedokończonego zdania
                $description = preg_replace('@(.*)\..*@', '\1.', $description);

                // trim
                $description = trim($description);

                // keywords
                $keywords_arr = explode(' ', $entity->getTitle() . ' ' . $description);

                $keywords = array();
                if (is_array($keywords_arr)) {
                    foreach ($keywords_arr as $kw) {
                        $kw = trim($kw);
                        $kw = preg_replace('/[\.,;\'\"]/', '', $kw);
                        if (strlen($kw) >= 4 && !in_array($kw, $keywords)) {
                            $keywords[] = $kw;
                        }
                        if (count($keywords) >= 10) {
                            break;
                        }
                    }
                }

                $entity->setSeoTitle(strip_tags($entity->getTitle()));
                $entity->setSeoDescription($description);
                $entity->setSeoKeywords(implode(', ', $keywords));
                $entity->setSeoGenerate(false);
            }
        }
    }

    public function postUpdate(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof Media) {
//            if (strcmp($entity->getOldSlug(), $entity->getSlug()) !== 0) {
//                $query = $em->createQueryBuilder()
//                    ->select('c')
//                    ->from('LsMenuBundle:MenuItem', 'c')
//                    ->where('c.route LIKE :route')
//                    ->andWhere('c.route_parameters LIKE :slug')
//                    ->setParameter('route', 'ls_media_show')
//                    ->setParameter('slug', '%' . $entity->getOldSlug() . '%')
//                    ->getQuery();
//
//                $items = $query->getResult();
//
//                foreach ($items as $item) {
//                    $item->setRouteParameters('{"slug":"' . $entity->getSlug() . '"}');
//                    $em->persist($item);
//                }
//                $em->flush();
//
//                $entity->setOldSlug($entity->getSlug());
//                $em->persist($entity);
//                $em->flush();
//            }
        }
    }
}