<?php

namespace Ls\MediaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * MediaFile
 * @ORM\Table(name="media_file")
 * @ORM\Entity
 */
class MediaFile {
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer
     */
    private $id;
    
    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $file_name;
    
    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $created_at;
    
    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    private $updated_at;
    
    protected $file;
    
    /**
     * @ORM\ManyToOne(targetEntity="Media", inversedBy="files")
     * @ORM\JoinColumn(name="media_id", referencedColumnName="id")
     */
    private $media;
    
    /**
     * Constructor
     */
    public function __construct() {
        $this->created_at = new \DateTime();
    }
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }
    
    /**
     * Set file_name
     *
     * @param string $file_name
     * @return MediaFile
     */
    public function setFileName($file_name = null)
    {
        $this->file_name = $file_name;
    }
    
    /**
     * Get file_name
     *
     * @return string
     */
    public function getFileName()
    {
        return $this->file_name;
    }
    
    /**
     * Set created_at
     *
     * @param \DateTime $created_at
     * @return MediaFile
     */
    public function setCreatedAt($created_at) {
        $this->created_at = $created_at;
        
        return $this;
    }
    
    /**
     * Get created_at
     *
     * @return \DateTime
     */
    public function getCreatedAt() {
        return $this->created_at;
    }
    
    public function getMedia() {
        return $this->media;
    }
    
    public function setMedia(Media $media) {
        $this->media = $media;
        
        return $this;
    }
    
    /**
     * Set updated_at
     *
     * @param \DateTime $updated_at
     * @return MediaFile
     */
    public function setUpdatedAt($updated_at) {
        $this->updated_at = $updated_at;
        
        return $this;
    }
    
    /**
     * Get updated_at
     *
     * @return \DateTime
     */
    public function getUpdatedAt() {
        return $this->updated_at;
    }
    
    public function setFile(UploadedFile $file = null) {
        $this->file = $file;
    }
    
    public function getFile() {
        return $this->file;
    }
    
    public function deleteFile() {
        if (!empty($this->file_name)) {
            $filename = $this->getFileAbsolutePath();
            
            if (file_exists($filename)) {
                @unlink($filename);
            }
        }
    }
    
    public function getFileAbsolutePath() {
        return empty($this->file_name) ? null : $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->file_name;
    }
    
    public function getFileWebPath() {
        return empty($this->file_name) ? null : '/' . $this->getUploadDir(true) . '/' . rawurlencode($this->file_name);
    }
    
    public function getUploadRootDir() {
        return __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . $this->getUploadDir();
    }
    
    protected function getUploadDir() {
        return 'upload/media-files';
    }
    
    public function upload() {
        if (null === $this->file) {
            return;
        }
        
        $sFileName = $this->getFileName();
        
        $this->file->move($this->getUploadRootDir(), $sFileName);
        
        unset($this->file);
    }
    
    public function __toString() {
        return $this->getId();
    }
}