<?php

namespace Ls\MediaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MediaLink
 * @ORM\Table(name="media_link")
 * @ORM\Entity
 */
class MediaLink {
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer
     */
    private $id;
    
    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $title;
    
    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $url;
    
    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $created_at;
    
    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    private $updated_at;
    
    /**
     * @ORM\ManyToOne(targetEntity="Media", inversedBy="links")
     * @ORM\JoinColumn(name="media_id", referencedColumnName="id")
     */
    private $media;
    
    /**
     * Constructor
     */
    public function __construct() {
        $this->created_at = new \DateTime();
    }
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }
    
    /**
     * Set title
     *
     * @param string $title
     * @return MediaLink
     */
    public function setTitle($title) {
        $this->title = $title;
        
        return $this;
    }
    
    /**
     * Get title
     *
     * @return string
     */
    public function getTitle() {
        return $this->title;
    }
    
    /**
     * Set url
     *
     * @param string $url
     * @return MediaLink
     */
    public function setUrl($url) {
        $this->url = $url;
        
        return $this;
    }
    
    /**
     * Get url
     *
     * @return string
     */
    public function getUrl() {
        return $this->url;
    }
    
    /**
     * Set created_at
     *
     * @param \DateTime $created_at
     * @return MediaLink
     */
    public function setCreatedAt($created_at) {
        $this->created_at = $created_at;
        
        return $this;
    }
    
    /**
     * Get created_at
     *
     * @return \DateTime
     */
    public function getCreatedAt() {
        return $this->created_at;
    }
    
    public function getMedia() {
        return $this->media;
    }
    
    public function setMedia(Media $media) {
        $this->media = $media;
        
        return $this;
    }
    
    /**
     * Set updated_at
     *
     * @param \DateTime $updated_at
     * @return MediaLink
     */
    public function setUpdatedAt($updated_at) {
        $this->updated_at = $updated_at;
        
        return $this;
    }
    
    /**
     * Get updated_at
     *
     * @return \DateTime
     */
    public function getUpdatedAt() {
        return $this->updated_at;
    }
    
    public function __toString() {
        return $this->getTitle();
    }
}