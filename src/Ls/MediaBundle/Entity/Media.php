<?php

namespace Ls\MediaBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Media
 * @ORM\Table(name="media")
 * @ORM\Entity
 */
class Media {
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer
     */
    private $id;
    
    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $title;
    
    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $slug;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $old_slug;
    
    /**
     * @ORM\Column(type="text", nullable=true)
     * @var string
     */
    private $content;
    
    /**
     * @ORM\Column(type="text", nullable=true)
     * @var string
     */
    private $content2;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $seo_title;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $seo_keywords;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $seo_description;
    
    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @var boolean
     */
    private $seo_generate;
    
    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $created_at;
    
    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    private $updated_at;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $youtube;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $facebook;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $instagram;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $twitter;
    
    /**
     * One item has Many Files.
     * @ORM\OneToMany(targetEntity="MediaFile", mappedBy="media", cascade={"persist"}, orphanRemoval=true)
     * @ORM\OrderBy({"id" = "ASC"})
     */
    private $files;
    
    /**
     * One item has Many Links.
     * @ORM\OneToMany(targetEntity="MediaLink", mappedBy="media", cascade={"persist"}, orphanRemoval=true)
     * @ORM\OrderBy({"id" = "ASC"})
     */
    private $links;
    
    protected $multipleFiles;
    
    /**
     * Constructor
     */
    public function __construct() {
        $this->seo_generate = true;
        $this->created_at = new \DateTime();
        $this->files = new ArrayCollection();
        $this->links = new ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }
    
    /**
     * Set title
     *
     * @param string $title
     * @return Media
     */
    public function setTitle($title) {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Media
     */
    public function setSlug($slug) {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug() {
        return $this->slug;
    }

    /**
     * Set old_slug
     *
     * @param string $old_slug
     * @return Media
     */
    public function setOldSlug($old_slug) {
        $this->old_slug = $old_slug;

        return $this;
    }

    /**
     * Get old_slug
     *
     * @return string
     */
    public function getOldSlug() {
        return $this->old_slug;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Media
     */
    public function setContent($content) {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent() {
        return $this->content;
    }
    
    /**
     * Set content2
     *
     * @param string $content2
     * @return Media
     */
    public function setContent2($content2) {
        $this->content2 = $content2;

        return $this;
    }

    /**
     * Get content2
     *
     * @return string
     */
    public function getContent2() {
        return $this->content2;
    }

    /**
     * Set seo_title
     *
     * @param string $seo_title
     * @return Media
     */
    public function setSeoTitle($seo_title) {
        $this->seo_title = $seo_title;

        return $this;
    }

    /**
     * Get seo_title
     *
     * @return string
     */
    public function getSeoTitle() {
        return $this->seo_title;
    }

    /**
     * Set seo_keywords
     *
     * @param string $seo_keywords
     * @return Media
     */
    public function setSeoKeywords($seo_keywords) {
        $this->seo_keywords = $seo_keywords;

        return $this;
    }

    /**
     * Get seo_keywords
     *
     * @return string
     */
    public function getSeoKeywords() {
        return $this->seo_keywords;
    }

    /**
     * Set seo_description
     *
     * @param string $seo_description
     * @return Media
     */
    public function setSeoDescription($seo_description) {
        $this->seo_description = $seo_description;

        return $this;
    }

    /**
     * Get seo_description
     *
     * @return string
     */
    public function getSeoDescription() {
        return $this->seo_description;
    }
    
    /**
     * Set seo_generate
     *
     * @param boolean $seo_generate
     * @return Media
     */
    public function setSeoGenerate($seo_generate) {
        $this->seo_generate = $seo_generate;

        return $this;
    }
    
    /**
     * Get seo_generate
     *
     * @return boolean
     */
    public function getSeoGenerate() {
        return $this->seo_generate;
    }
    
    /**
     * Set created_at
     *
     * @param \DateTime $created_at
     * @return Media
     */
    public function setCreatedAt($created_at) {
        $this->created_at = $created_at;

        return $this;
    }
    
    /**
     * Get created_at
     *
     * @return \DateTime
     */
    public function getCreatedAt() {
        return $this->created_at;
    }
    
    /**
     * Add file
     *
     * @param MediaFile $file
     * @return Media
     */
    public function addFile(MediaFile $file) {
        $this->files->add($file);

        return $this;
    }
    
    /**
     * Remove file
     *
     * @param MediaFile $file
     */
    public function removeFile(MediaFile $file) {
        $this->files->removeElement($file);
    }
    
    /**
     * Get files
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFiles() {
        return $this->files;
    }
    
    /**
     * Add link
     *
     * @param MediaLink $link
     * @return Media
     */
    public function addLink(MediaLink $link) {
        $this->links->add($link);

        return $this;
    }
    
    /**
     * Remove link
     *
     * @param MediaLink $link
     */
    public function removeLink(MediaLink $link) {
        $this->links->removeElement($link);
    }
    
    /**
     * Get links
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLinks() {
        return $this->links;
    }
    
    /**
     * Get multipleFiles
     *
     * @return string
     */
    public function getMultipleFiles()
    {
        return $this->multipleFiles;
    }
    
    /**
     * Set multipleFiles
     *
     * @param string $multipleFiles
     * @return Media
     */
    public function setMultipleFiles($multipleFiles)
    {
        $this->multipleFiles = $multipleFiles;
    }
    
    /**
     * Set updated_at
     *
     * @param \DateTime $updated_at
     * @return Media
     */
    public function setUpdatedAt($updated_at) {
        $this->updated_at = $updated_at;

        return $this;
    }
    
    /**
     * Get updated_at
     *
     * @return \DateTime
     */
    public function getUpdatedAt() {
        return $this->updated_at;
    }
    
    /**
     * Set youtube
     *
     * @param string $youtube
     * @return Media
     */
    public function setYoutube($youtube) {
        $this->youtube = $youtube;

        return $this;
    }

    /**
     * Get youtube
     *
     * @return string
     */
    public function getYoutube() {
        return $this->youtube;
    }
    
    /**
     * Set facebook
     *
     * @param string $facebook
     * @return Media
     */
    public function setFacebook($facebook) {
        $this->facebook = $facebook;

        return $this;
    }

    /**
     * Get facebook
     *
     * @return string
     */
    public function getFacebook() {
        return $this->facebook;
    }
    
    /**
     * Set instagram
     *
     * @param string $instagram
     * @return Media
     */
    public function setInstagram($instagram) {
        $this->instagram = $instagram;

        return $this;
    }
    
    /**
     * Get instagram
     *
     * @return string
     */
    public function getInstagram() {
        return $this->instagram;
    }
    
    /**
     * Set twitter
     *
     * @param string $twitter
     * @return Media
     */
    public function setTwitter($twitter) {
        $this->twitter = $twitter;

        return $this;
    }
    
    /**
     * Get twitter
     *
     * @return string
     */
    public function getTwitter() {
        return $this->twitter;
    }
    
    public function __toString() {
        return $this->getTitle();
    }
}