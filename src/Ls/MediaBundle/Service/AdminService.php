<?php

namespace Ls\MediaBundle\Service;

use Knp\Menu\ItemInterface;
use Ls\CoreBundle\Helper\AdminBlock;
use Ls\CoreBundle\Helper\AdminLink;
use Ls\CoreBundle\Helper\AdminRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

class AdminService {
    private $container;

    public function __construct(ContainerInterface $container) {
        $this->container = $container;
    }

    public function addToMenu(ItemInterface $parent, $route, $set) {
        $item = $parent->addChild('Media', array(
            'route' => 'ls_admin_media',
        ));

        $current_set = true;

        if (!$set) {
            switch ($route) {
                case 'ls_admin_media':
                case 'ls_admin_media_new':
                case 'ls_admin_media_edit':
                case 'ls_admin_media_batch':
                    $item->setCurrent(true);
                    break;
                default:
                    $current_set = false;
                    break;
            }
        }

        return $current_set;
    }

    public function addToDashboard(AdminBlock $parent) {
        $router = $this->container->get('router');

        $row = new AdminRow('Media');
        $parent->addRow($row);

        $row->addLink(new AdminLink('Dodaj', 'glyphicon-plus', $router->generate('ls_admin_media_new')));
        $row->addLink(new AdminLink('Zarządzaj', 'glyphicon-list', $router->generate('ls_admin_media')));
    }
}

