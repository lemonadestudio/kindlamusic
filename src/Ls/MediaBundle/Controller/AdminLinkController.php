<?php

namespace Ls\MediaBundle\Controller;

use Ls\MediaBundle\Entity\MediaLink;
use Ls\MediaBundle\Form\MediaLinkType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AdminLinkController extends Controller {
    private $pager_limit_name = 'admin_media_link_pager_limit';

    public function indexAction(Request $request, $mediaId) {
        $em = $this->getDoctrine()->getManager();
        $session = $this->container->get('session');
        
        $page = $request->query->get('page', 1);
        if ($session->has($this->pager_limit_name)) {
            $limit = $session->get($this->pager_limit_name);
        } else {
            $limit = 15;
            $session->set($this->pager_limit_name, $limit);
        }
        
        $query = $em->createQueryBuilder()
            ->select('e', 'm')
            ->from('LsMediaBundle:MediaLink', 'e')
            ->join('e.media', 'm')
            ->where('m.id = :mediaId')
            ->setParameter('mediaId', $mediaId)
            ->getQuery();
        
        $paginator = $this->get('knp_paginator');
        $entities = $paginator->paginate(
            $query,
            $page,
            $limit,
            array(
                'defaultSortFieldName' => 'e.title',
                'defaultSortDirection' => 'asc',
                'wrap-queries' => true,
            )
        );
        $entities->setTemplate('LsCoreBundle:Backend:paginator.html.twig');

        if ($page > $entities->getPageCount() && $entities->getPageCount() > 0) {
            return $this->redirect($this->generateUrl('ls_admin_media_link', array('mediaId' => $mediaId)));
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
        $breadcrumbs->addItem('Media', $this->get('router')->generate('ls_admin_media'));
        $breadcrumbs->addItem('Linki do teledysków', $this->get('router')->generate('ls_admin_media_link', array('mediaId' => $mediaId)));
        
        return $this->render('LsMediaBundle:AdminLink:index.html.twig', array(
            'page' => $page,
            'limit' => $limit,
            'entities' => $entities,
            'mediaId' => $mediaId
        ));
    }
    
    public function newAction(Request $request, $mediaId) {
        $em = $this->getDoctrine()->getManager();
        
        $entity = new MediaLink();
        
        $form = $this->createForm(MediaLinkType::class, $entity, array(
            'action' => $this->generateUrl('ls_admin_media_link_new', array('mediaId' => $mediaId)),
            'method' => 'POST'
        ));
        $form->add('submit', SubmitType::class, array('label' => 'Zapisz'));
        $form->add('submit_and_list', SubmitType::class, array('label' => 'Zapisz i wróć na listę'));
        $form->add('submit_and_new', SubmitType::class, array('label' => 'Zapisz i dodaj następny'));
        
        $media = $em->getRepository('LsMediaBundle:Media')->find($mediaId);
        
        $form->handleRequest($request);
        if ($form->isValid()) {
            $entity->setMedia($media);
            $em->persist($entity);
            $em->flush();
            
            $this->get('session')->getFlashBag()->add('success', 'Dodanie media zakończone sukcesem.');

            if ($form->get('submit')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_media_link_edit', array('mediaId' => $mediaId, 'id' => $entity->getId())));
            }
            if ($form->get('submit_and_list')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_media_link', array('mediaId' => $mediaId)));
            }
            if ($form->get('submit_and_new')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_media_link_new', array('mediaId' => $mediaId)));
            }
        }
        if ($form->isSubmitted()) {
            $this->get('session')->getFlashBag()->add('error', 'Sprawdź pola formularza.');
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
        $breadcrumbs->addItem('Media', $this->get('router')->generate('ls_admin_media'));
        $breadcrumbs->addItem('Linki do teledysków', $this->get('router')->generate('ls_admin_media_link', array('mediaId' => $mediaId)));
        $breadcrumbs->addItem($entity->__toString(), $this->get('router')->generate('ls_admin_media_link_new', array('mediaId' => $mediaId)));

        return $this->render('LsMediaBundle:AdminLink:new.html.twig', array(
            'form' => $form->createView(),
            'mediaId' => $mediaId
        ));
    }

    public function editAction(Request $request, $id, $mediaId) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LsMediaBundle:MediaLink')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find MediaLink entity.');
        }

        $form = $this->createForm(MediaLinkType::class, $entity, array(
            'action' => $this->generateUrl('ls_admin_media_link_edit', array('mediaId' => $mediaId, 'id' => $entity->getId())),
            'method' => 'POST'
        ));
        $form->add('submit', SubmitType::class, array('label' => 'Zapisz zmiany'));
        $form->add('submit_and_list', SubmitType::class, array('label' => 'Zapisz zmiany i zamknij'));
        
        $form->handleRequest($request);
        if ($form->isValid()) {
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Aktualizacja media zakończona sukcesem.');
            
            if ($form->get('submit')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_media_link_edit', array('mediaId' => $mediaId, 'id' => $entity->getId())));
            }
            if ($form->get('submit_and_list')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_media_link', array('mediaId' => $mediaId)));
            }
        }
        if ($form->isSubmitted()) {
            $this->get('session')->getFlashBag()->add('error', 'Sprawdź pola formularza.');
        }
        
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
        $breadcrumbs->addItem('Media', $this->get('router')->generate('ls_admin_media'));
        $breadcrumbs->addItem('Linki do teledysków', $this->get('router')->generate('ls_admin_media_link', array('mediaId' => $mediaId)));
        $breadcrumbs->addItem($entity->__toString(), $this->get('router')->generate('ls_admin_media_link_edit', array('mediaId' => $mediaId, 'id' => $entity->getId())));

        return $this->render('LsMediaBundle:AdminLink:edit.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
            'mediaId' => $mediaId
        ));
    }

    public function deleteAction($mediaId, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LsMediaBundle:MediaLink')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find MediaLink entity.');
        }

        $em->remove($entity);
        $em->flush();

        $this->get('session')->getFlashBag()->add('success', 'Usunięcie MediaLink zakończone sukcesem.');

        return new Response('OK');
    }

    public function batchAction(Request $request, $mediaId) {
        $ids = $request->request->get('ids');
        $action = $request->request->get('action');

        if (is_array($ids) && count($ids) > 0) {
            $elements = count($ids);
            $message = 'Czy na pewno chcesz ';
            switch ($action) {
                case 'delete':
                    $message .= 'usunąć ';
                    break;
            }
            $message .= $elements . ' ';
            switch ($elements) {
                case 1:
                    $message .= 'element?';
                    break;
                case 2:
                case 3:
                case 4:
                    $message .= 'elementy?';
                    break;
                default:
                    $message .= 'elementów?';
                    break;
            }

            $breadcrumbs = $this->get("white_october_breadcrumbs");
            $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
            $breadcrumbs->addItem('Media', $this->get('router')->generate('ls_admin_media'));
            $breadcrumbs->addItem('Linki do teledysków', $this->get('router')->generate('ls_admin_media_link', array('mediaId' => $mediaId)));
            $breadcrumbs->addItem('Potwierdzenie', $this->get('router')->generate('ls_admin_media_link_batch', array('mediaId' => $mediaId)));

            return $this->render('LsMediaBundle:AdminLink:batch.html.twig', array(
                'message' => $message,
                'action' => $action,
                'ids' => implode(',', $ids),
                'mediaId' => $mediaId
            ));
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Nie wybrałeś żadnych elementów.');
            return $this->redirect($this->generateUrl('ls_admin_media_link', array('mediaId' => $mediaId)));
        }
    }

    public function batchExecuteAction(Request $request, $mediaId) {
        $em = $this->getDoctrine()->getManager();

        $ids = explode(',', $request->request->get('ids'));
        $action = $request->request->get('action');

        if (is_array($ids) && count($ids) > 0) {
            $elements = count($ids);
            $message = '';
            switch ($action) {
                case 'delete':
                    $message .= 'Usunięcie ';
                    $qb = $em->createQueryBuilder();
                    $query = $qb->select('e')
                        ->from('LsMediaBundle:MediaLink', 'e')
                        ->add('where', $qb->expr()->in('e.id', $ids))
                        ->getQuery();

                    $iterableResult = $query->iterate();
                    while (($row = $iterableResult->next()) !== false) {
                        $em->remove($row[0]);
                        $em->flush();
                    }
                    break;
            }
            $message .= $elements . ' ';
            switch ($elements) {
                case 1:
                    $message .= 'elementu ';
                    break;
                default:
                    $message .= 'elementów ';
                    break;
            }
            $message .= 'zakończone sukcesem ';

            $this->get('session')->getFlashBag()->add('success', $message);
            return $this->redirect($this->generateUrl('ls_admin_media_link', array('mediaId' => $mediaId)));
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Nie wybrałeś żadnych elementów.');
            return $this->redirect($this->generateUrl('ls_admin_media_link', array('mediaId' => $mediaId)));
        }
    }

    public function setLimitAction(Request $request) {
        $session = $this->container->get('session');

        $limit = $request->request->get('limit');
        $session->set($this->pager_limit_name, $limit);
        
        return new Response('OK');
    }
}
