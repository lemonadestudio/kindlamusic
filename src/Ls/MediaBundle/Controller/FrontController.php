<?php

namespace Ls\MediaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

/**
 * Media controller.
 *
 */
class FrontController extends Controller {

    public function loginAction(Request $request) {
        if ($request->query->has('_locale')) {
            return $this->redirect($this->generateUrl('ls_events_media_login'));
        }
        
        $em = $this->getDoctrine()->getManager();

        $form = $this->createFormBuilder([])
            ->add('password', PasswordType::class, ['label' => 'Podaj hasło'])
            ->add('submit', SubmitType::class, ['label' => 'Przejdź dalej'])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $password = $em->getRepository('LsSettingBundle:Setting')->findOneBy(['label' => 'kmevents_media_password'])->getValue();
            
            if ($data['password'] != $password) {
                $this->get('session')->getFlashBag()->add('error', 'Password error');
                return $this->redirect($this->generateUrl('ls_events_media_login'));
            } else {
                $this->get('session')->set('loginMedia', md5(date("d.m.Y")));

                return $this->redirect($this->generateUrl('ls_events_media'));
            }
        }

        return $this->render('LsMediaBundle:Front:login.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Media entities list.
     *
     */
    public function indexAction(Request $request) {
        if ($request->query->has('_locale')) {
            return $this->redirect($this->generateUrl('ls_events_media'));
        }

        if ($this->get('session')->get('loginMedia') != md5(date("d.m.Y"))) {
            return $this->redirect($this->generateUrl('ls_events_media_login'));
        }
        
        $em = $this->getDoctrine()->getManager();
        
        $qb = $em->createQueryBuilder();
        $query = $qb->select('m')
            ->from('LsMediaBundle:Media', 'm')
            ->getQuery();
        
        $entities = $query->getResult();
        
        return $this->render('LsMediaBundle:Front:index.html.twig', array(
            'entities' => $entities
        ));
    }
    
    /**
     * Finds and displays a Media entity.
     *
     */
    public function showAction(Request $request, $slug) {
        if ($request->query->has('_locale')) {
            return $this->redirect($this->generateUrl('ls_events_media'));
        }

        if ($this->get('session')->get('loginMedia') != md5(date("d.m.Y"))) {
            return $this->redirect($this->generateUrl('ls_events_media_login'));
        }
        
        $em = $this->getDoctrine()->getManager();

        $entity = $em->createQueryBuilder()
            ->select('m')
            ->from('LsMediaBundle:Media', 'm')
            ->andWhere('m.slug = :slug')
            ->setParameter('slug', $slug)
            ->getQuery()
            ->getOneOrNullResult();
        
        return $this->render('LsMediaBundle:Front:show.html.twig', array(
            'entity' => $entity
        ));
    }
}
