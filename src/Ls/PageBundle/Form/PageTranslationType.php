<?php

namespace Ls\PageBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class PageTranslationType extends AbstractType {
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('title', null, array(
            'label' => 'Tytuł',
            'attr' => array(
                'class' => 'form-control'
            ),
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Wypełnij pole'
                ))
            )
        ));
        $builder->add('slug', null, array(
            'label' => 'Końcówka adresu URL',
            'attr' => array(
                'class' => 'form-control'
            )
        ));
        $builder->add('content_short', TextareaType::class, array(
            'label' => 'Krótka treść',
            'attr' => array(
                'class' => 'form-control',
                'rows' => 5
            )
        ));
        $builder->add('content', null, array(
            'label' => 'Treść',
            'attr' => array(
                'class' => 'form-control wysiwyg'
            )
        ));
        $builder->add('seo_title', null, array(
            'label' => 'Tag "title"',
            'attr' => array(
                'class' => 'form-control'
            )
        ));
        $builder->add('seo_keywords', TextareaType::class, array(
            'label' => 'Meta "keywords"',
            'attr' => array(
                'class' => 'form-control',
                'rows' => 3
            )
        ));
        $builder->add('seo_description', TextareaType::class, array(
            'label' => 'Meta "description"',
            'attr' => array(
                'class' => 'form-control',
                'rows' => 3
            )
        ));
        $builder->add('locale', HiddenType::class, array(
            'label' => 'Locale',
        ));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Ls\PageBundle\Entity\PageTranslation',
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix() {
        return 'form_admin_page_translation';
    }
}
