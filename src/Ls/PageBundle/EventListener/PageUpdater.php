<?php

namespace Ls\PageBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Ls\CoreBundle\Utils\Tools;
use Ls\PageBundle\Entity\Page;

class PageUpdater implements EventSubscriber {

    public function getSubscribedEvents() {
        return array(
            'prePersist',
            'postPersist',
            'preUpdate',
            'postUpdate',
        );
    }

    public function prePersist(LifecycleEventArgs $args) {
        $entity = $args->getEntity();

        if ($entity instanceof Page) {
            if (!$entity->getCreatedAt()) {
                $entity->setCreatedAt(new \DateTime());
            }
            if (!$entity->getId()) {
                foreach ($entity->getTranslations() as $translation) {
                    $translation->setSlug(Tools::slugify($translation->getTitle()));
                }
            }
            if ($entity->getSeoGenerate()) {
                foreach ($entity->getTranslations() as $translation) {
                    // description
                    $description = strip_tags($translation->getContent());
                    $description = html_entity_decode($description, ENT_COMPAT | ENT_HTML401, 'UTF-8');
                    $description = str_replace("\xC2\xA0", ' ', $description);
                    $description = Tools::truncateWord($description, 255, '');

                    // usunięcie nowych linii i podwójnych białych znaków
                    $description = preg_replace('/\s\s+/', ' ', $description);

                    // usunięcie ostatniego, niedokończonego zdania
                    $description = preg_replace('@(.*)\..*@', '\1.', $description);

                    // trim
                    $description = trim($description);

                    // keywords
                    $keywords_arr = explode(' ', $translation->getTitle() . ' ' . $description);

                    $keywords = array();
                    if (is_array($keywords_arr)) {
                        foreach ($keywords_arr as $kw) {
                            $kw = trim($kw);
                            $kw = preg_replace('/[\.,;\'\"]/', '', $kw);
                            if (strlen($kw) >= 4 && !in_array($kw, $keywords)) {
                                $keywords[] = $kw;
                            }
                            if (count($keywords) >= 10) {
                                break;
                            }
                        }
                    }

                    $translation->setSeoTitle(strip_tags($translation->getTitle()));
                    $translation->setSeoDescription($description);
                    $translation->setSeoKeywords(implode(', ', $keywords));
                }
                $entity->setSeoGenerate(false);
            }
            if ($entity->getContentShortGenerate()) {
                foreach ($entity->getTranslations() as $translation) {
                    $content_short = strip_tags($translation->getContent());
                    $content_short = html_entity_decode($content_short, ENT_COMPAT | ENT_HTML401, 'UTF-8');
                    $content_short = str_replace("\xC2\xA0", ' ', $content_short);
                    $content_short = Tools::truncateWord($content_short, 255, '');
                    $content_short = preg_replace('/\s\s+/', ' ', $content_short);
                    $content_short = preg_replace('@(.*)\..*@', '\1.', $content_short);
                    $content_short = trim($content_short);
                    $translation->setContentShort($content_short);
                }
                $entity->setContentShortGenerate(false);
            }
        }
    }

    public function postPersist(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof Page) {
//            if (strcmp($entity->getOldSlug(), $entity->getSlug()) !== 0) {
//                if (null !== $entity->getOldSlug()) {
//                    $query = $em->createQueryBuilder()
//                        ->select('c')
//                        ->from('LsMenuBundle:MenuItem', 'c')
//                        ->where('c.route LIKE :route')
//                        ->andWhere('c.route_parameters LIKE :slug')
//                        ->setParameter('route', 'ls_page_show')
//                        ->setParameter('slug', '%' . $entity->getOldSlug() . '%')
//                        ->getQuery();
//
//                    $items = $query->getResult();
//
//                    foreach ($items as $item) {
//                        $item->setRouteParameters('{"slug":"' . $entity->getSlug() . '"}');
//                        $em->persist($item);
//                    }
//                    $em->flush();
//
//                    $entity->setOldSlug($entity->getSlug());
//                    $em->persist($entity);
//                    $em->flush();
//                } else {
//                    $entity->setOldSlug($entity->getSlug());
//                    $em->persist($entity);
//                    $em->flush();
//                }
//            }
        }
    }

    public function preUpdate(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof Page) {
            $entity->setUpdatedAt(new \DateTime());
            if ($entity->getSeoGenerate()) {
                foreach ($entity->getTranslations() as $translation) {
                    // description
                    $description = strip_tags($translation->getContent());
                    $description = html_entity_decode($description, ENT_COMPAT | ENT_HTML401, 'UTF-8');
                    $description = str_replace("\xC2\xA0", ' ', $description);
                    $description = Tools::truncateWord($description, 255, '');

                    // usunięcie nowych linii i podwójnych białych znaków
                    $description = preg_replace('/\s\s+/', ' ', $description);

                    // usunięcie ostatniego, niedokończonego zdania
                    $description = preg_replace('@(.*)\..*@', '\1.', $description);

                    // trim
                    $description = trim($description);

                    // keywords
                    $keywords_arr = explode(' ', $translation->getTitle() . ' ' . $description);

                    $keywords = array();
                    if (is_array($keywords_arr)) {
                        foreach ($keywords_arr as $kw) {
                            $kw = trim($kw);
                            $kw = preg_replace('/[\.,;\'\"]/', '', $kw);
                            if (strlen($kw) >= 4 && !in_array($kw, $keywords)) {
                                $keywords[] = $kw;
                            }
                            if (count($keywords) >= 10) {
                                break;
                            }
                        }
                    }

                    $translation->setSeoTitle(strip_tags($translation->getTitle()));
                    $translation->setSeoDescription($description);
                    $translation->setSeoKeywords(implode(', ', $keywords));
                }
                $entity->setSeoGenerate(false);
            }
            if ($entity->getContentShortGenerate()) {
                foreach ($entity->getTranslations() as $translation) {
                    $content_short = strip_tags($translation->getContent());
                    $content_short = html_entity_decode($content_short, ENT_COMPAT | ENT_HTML401, 'UTF-8');
                    $content_short = str_replace("\xC2\xA0", ' ', $content_short);
                    $content_short = Tools::truncateWord($content_short, 255, '');
                    $content_short = preg_replace('/\s\s+/', ' ', $content_short);
                    $content_short = preg_replace('@(.*)\..*@', '\1.', $content_short);
                    $content_short = trim($content_short);
                    $translation->setContentShort($content_short);
                }
                $entity->setContentShortGenerate(false);
            }
        }
    }

    public function postUpdate(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof Page) {
//            if (strcmp($entity->getOldSlug(), $entity->getSlug()) !== 0) {
//                $query = $em->createQueryBuilder()
//                    ->select('c')
//                    ->from('LsMenuBundle:MenuItem', 'c')
//                    ->where('c.route LIKE :route')
//                    ->andWhere('c.route_parameters LIKE :slug')
//                    ->setParameter('route', 'ls_page_show')
//                    ->setParameter('slug', '%' . $entity->getOldSlug() . '%')
//                    ->getQuery();
//
//                $items = $query->getResult();
//
//                foreach ($items as $item) {
//                    $item->setRouteParameters('{"slug":"' . $entity->getSlug() . '"}');
//                    $em->persist($item);
//                }
//                $em->flush();
//
//                $entity->setOldSlug($entity->getSlug());
//                $em->persist($entity);
//                $em->flush();
//            }
        }
    }
}