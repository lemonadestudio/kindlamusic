<?php

namespace Ls\ContactBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Validator\Constraints\NotBlank;
use Ls\CoreBundle\Form\GoogleRecaptchaType;
use Ls\CoreBundle\Validator\Constraints\GoogleRecaptcha;

class ContactType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('name', null, array(
            'label_attr' => array('style' => 'display: none;'),
            'attr' => array(
                'placeholder' => 'Imię i nazwisko',
            ),
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Zaznacz pole'
                )),
            )
        ));
        
        $builder->add('email', null, array(
            'label_attr' => array('style' => 'display: none;'),
            'attr' => array(
                'placeholder' => 'Adres e-mail',
            ),
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Zaznacz pole'
                )),
            )
        ));
        
        $builder->add('content', TextareaType::class, array(
            'label_attr' => array('style' => 'display: none;'),
            'attr' => array(
                'placeholder' => 'Wiadomość',
            ),
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Zaznacz pole'
                )),
            )
        ));

        $builder->add('phone', null, array(
            'label_attr' => array('style' => 'display: none;'),
            'attr' => array(
                'placeholder' => 'Numer telefonu',
            ),
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Zaznacz pole'
                )),
            )
        ));
        
        $builder->add('phone', null, array(
            'label_attr' => array('style' => 'display: none;'),
            'attr' => array(
                'placeholder' => 'Numer telefonu',
            ),
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Zaznacz pole'
                )),
            )
        ));
        
        $builder->add('google_recaptcha', GoogleRecaptchaType::class, array(
            'label_attr' => array('style' => 'display: none;'),
            'error_bubbling' => false,
            'mapped' => false,
            'constraints' => array(
                new GoogleRecaptcha()
            )
        ));
    
        $builder->add('accept', CheckboxType::class, array(
            'required' => false,
            'mapped' => false,
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Zaznacz pole'
                )),
            )
        ));
        
        $builder->add('submit', SubmitType::class, array(
                'label' => 'Wyślij'
            )
        );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Ls\ContactBundle\Entity\Contact',
            ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix() {
        return 'form_contact';
    }
}
