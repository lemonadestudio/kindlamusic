<?php

namespace Ls\ContactBundle\Controller;

use Ls\ContactBundle\Entity\Contact;
use Ls\ContactBundle\Form\ContactType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class FrontController extends Controller {
    public function indexAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $entity = new Contact();

        $form = $this->createForm(ContactType::class, $entity, array(
            'action' => $this->container->get('router')->generate('ls_contact_form'),
            'method' => 'POST',
        ));

        $form->handleRequest($request);
        if ($form->isValid()) {
            $em->persist($entity);
            $em->flush();

            $subject = 'Wiadomość z formularza kontaktowego';

            $message_txt = '<h3>Wiadomość z formularza kontaktowego:</h3>';
            $message_txt .= nl2br($entity->getContent()) . ' <hr />';
            $message_txt .= '<h3>Pozostałe dane:</h3>';
            $message_txt .= 'Imię: ' . $entity->getName() . '<br />';
            $message_txt .= 'Adres e-mail: ' . $entity->getEmail() . '<br />';
            $message_txt .= 'Numer telefonu: ' . $entity->getPhone() . '<br />';

            $recipient_string = $em->getRepository('LsSettingBundle:Setting')->findOneByLabel('email_to_contact')->getValue();
            $recipient_array = explode(',', $recipient_string);
            $recipients = array_map('trim', $recipient_array);

            $message = \Swift_Message::newInstance();
            $message->setSubject($subject);
            $message->setFrom(array($this->container->getParameter('mailer_user') => $this->container->getParameter('mailer_name')));
            $message->setTo($recipients);
            $message->setBody($message_txt, 'text/html');
            $message->addPart(strip_tags($message_txt), 'text/plain');

            $mailer = $this->get('mailer');

            $mailer->send($message);

            return new Response('OK');
        }
        
        if ($form->isSubmitted()) {
            return new Response('ERROR');
        }
        
        return $this->render('LsContactBundle:Front:index.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}
