<?php

namespace Ls\ComposerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Composer controller.
 *
 */
class FrontController extends Controller {

    /**
     * Finds and displays a Composer entity.
     *
     */
    public function showAction($slug) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->createQueryBuilder()
            ->select('p')
            ->from('LsComposerBundle:Composer', 'p')
            ->where('p.slug = :slug')
            ->setParameter('slug', $slug)
            ->getQuery()
            ->getOneOrNullResult();

        if (null === $entity) {
            throw $this->createNotFoundException('Unable to find Composer entity.');
        }

        return $this->render('LsComposerBundle:Front:show.html.twig', array(
            'entity' => $entity,
        ));
    }
    
    public function blockAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $today = new \DateTime();

        $currentSlug = $request->get('slug');
        
        $entities = $em->createQueryBuilder()
            ->select('p')
            ->from('LsComposerBundle:Composer', 'p')
            ->where('p.published_at is NOT NULL')
            ->andWhere('p.published_at <= :today')
            ->orderBy('p.published_at', 'DESC')
            ->setParameter('today', $today)
            ->getQuery()
            ->getResult();
        
        return $this->render('LsComposerBundle:Front:block.html.twig', array(
            'entities' => $entities,
            'currentSlug' => $currentSlug,
        ));
    }
}
