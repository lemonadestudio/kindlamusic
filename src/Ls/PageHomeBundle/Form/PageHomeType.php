<?php

namespace Ls\PageHomeBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class PageHomeType extends AbstractType {
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        
        $builder->add('title', null, array(
            'label' => 'Tytuł',
            'attr' => array(
                'class' => 'form-control'
            ),
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Wypełnij pole'
                ))
            )
        ));
        
        $builder->add('slug', null, array(
            'label' => 'Końcówka adresu URL',
            'attr' => array(
                'class' => 'form-control'
            )
        ));
        
        $builder->add('content', null, array(
            'label' => 'Treść',
            'attr' => array(
                'class' => 'form-control wysiwyg'
            )
        ));
        $builder->add('seo_title', null, array(
            'label' => 'Tag "title"',
            'attr' => array(
                'class' => 'form-control'
            )
        ));
        $builder->add('seo_keywords', TextareaType::class, array(
            'label' => 'Meta "keywords"',
            'attr' => array(
                'class' => 'form-control',
                'rows' => 3
            )
        ));
        $builder->add('seo_description', TextareaType::class, array(
            'label' => 'Meta "description"',
            'attr' => array(
                'class' => 'form-control',
                'rows' => 3
            )
        ));
        
        $builder->add('seo_generate', null, array(
            'label' => 'Generuj automatycznie'
        ));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Ls\PageHomeBundle\Entity\PageHome',
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix() {
        return 'form_admin_page_home';
    }
}
