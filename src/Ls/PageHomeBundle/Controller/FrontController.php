<?php

namespace Ls\PageHomeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * PageHome controller.
 *
 */
class FrontController extends Controller {

    /**
     * Finds and displays a PageHome entity.
     *
     */
    public function showAction(Request $request, $slug) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->createQueryBuilder()
            ->select('p')
            ->from('LsPageHomeBundle:PageHome', 'p')
            ->where('p.slug = :slug')
            ->setParameter('slug', $slug)
            ->getQuery()
            ->getOneOrNullResult();

        if (null === $entity) {
            throw $this->createNotFoundException('Unable to find PageHome entity.');
        }

        return $this->render('LsPageHomeBundle:Front:show.html.twig', array(
            'entity' => $entity,
        ));
    }

}
