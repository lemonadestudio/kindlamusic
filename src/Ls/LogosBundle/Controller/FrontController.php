<?php

namespace Ls\LogosBundle\Controller;

use Doctrine\DBAL\Types\Type;
use Ls\CoreBundle\Utils\Tools;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Logos controller.
 *
 */
class FrontController extends Controller {

    public function blockAction() {
        $em = $this->getDoctrine()->getManager();

        $qb = $em->createQueryBuilder();
        $entities = $qb->select('a')
            ->from('LsLogosBundle:Logos', 'a')
            ->orderBy('a.id', 'desc')
            ->getQuery()
            ->getResult();

        return $this->render('LsLogosBundle:Front:block.html.twig', array(
            'entities' => $entities,
        ));
    }
}
