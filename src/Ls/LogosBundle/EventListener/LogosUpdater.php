<?php

namespace Ls\LogosBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Ls\CoreBundle\Utils\Tools;
use Ls\LogosBundle\Entity\Logos;

class LogosUpdater implements EventSubscriber {

    public function getSubscribedEvents() {
        return array(
            'postRemove',
        );
    }


    public function postRemove(LifecycleEventArgs $args) {
        $entity = $args->getEntity();

        if ($entity instanceof Logos) {
            $entity->deletePhoto();
        }
    }

}