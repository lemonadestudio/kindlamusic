<?php

namespace Ls\ActorsBundle\Service;

use Knp\Menu\ItemInterface;
use Ls\CoreBundle\Helper\AdminBlock;
use Ls\CoreBundle\Helper\AdminLink;
use Ls\CoreBundle\Helper\AdminRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

class AdminService {
    private $container;

    public function __construct(ContainerInterface $container) {
        $this->container = $container;
    }

    public function addToMenu(ItemInterface $parent, $route, $set) {
        $item = $parent->addChild('Artyści', array(
            'route' => 'ls_admin_actors',
        ));

        $current_set = true;

        if (!$set) {
            switch ($route) {
                case 'ls_admin_actors':
                case 'ls_admin_actors_new':
                case 'ls_admin_actors_edit':
                case 'ls_admin_actors_batch':
                    $item->setCurrent(true);
                    break;
                default:
                    $current_set = false;
                    break;
            }
        }

        return $current_set;
    }

    public function addToDashboard(AdminBlock $parent) {
        $router = $this->container->get('router');

        $row = new AdminRow('Artyści');
        $parent->addRow($row);

        $row->addLink(new AdminLink('Dodaj', 'glyphicon-plus', $router->generate('ls_admin_actors_new')));
        $row->addLink(new AdminLink('Zarządzaj', 'glyphicon-list', $router->generate('ls_admin_actors')));
    }
}

