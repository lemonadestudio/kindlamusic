<?php

namespace Ls\ActorsBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Ls\CoreBundle\Utils\Tools;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Ls\GalleryBundle\Entity\Gallery;

/**
 * Actors
 * @ORM\Table(name="actors")
 * @ORM\Entity
 */
class Actors
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @var boolean
     */
    private $show_main;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $title;

    /**
     * @Gedmo\Slug(fields={"title"})
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $old_slug;
    
    /**
     * @ORM\Column(type="text", nullable=true)
     * @var string
     */
    private $contact_information;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @var boolean
     */
    private $content_short_generate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $content_short;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @var string
     */
    private $content;
    
    /**
     * @ORM\Column(type="integer")
     * @var integer
     */
    private $arrangement;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $photo;
    
    /**
     * @ORM\ManyToOne(
     *     targetEntity="Ls\GalleryBundle\Entity\Gallery",
     *     inversedBy="actors"
     * )
     * @ORM\JoinColumn(
     *     name="gallery_id",
     *     referencedColumnName="id",
     *     onDelete="SET NULL"
     * )
     * @var \Ls\GalleryBundle\Entity\Gallery
     *
     */
    private $gallery;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @var boolean
     */
    private $seo_generate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $seo_title;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $seo_keywords;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $seo_description;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    private $updated_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    private $published_at;

    protected $file;

    protected $listWidth = 245;
    protected $listHeight = 360;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->content_short_generate = true;
        $this->seo_generate = true;
        $this->created_at = new \DateTime();
        $this->published_at = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set show_main
     *
     * @param boolean $show_main
     * @return Actors
     */
    public function setShowMain($show_main)
    {
        $this->show_main = $show_main;

        return $this;
    }

    /**
     * Get show_main
     *
     * @return boolean
     */
    public function getShowMain()
    {
        return $this->show_main;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Actors
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }
    
    /**
     * Set gallery
     *
     * @param \Ls\GalleryBundle\Entity\Gallery $gallery
     * @return Page
     */
    public function setGallery(Gallery $gallery = null) {
        $this->gallery = $gallery;

        return $this;
    }

    /**
     * Get gallery
     *
     * @return \Ls\GalleryBundle\Entity\Gallery
     */
    public function getGallery() {
        return $this->gallery;
    }
    
    /**
     * Set contact_information
     *
     * @param string $contact_information
     * @return Actors
     */
    public function setContactInformation($contact_information)
    {
        $this->contact_information = $contact_information;

        return $this;
    }

    /**
     * Get contact_information
     *
     * @return string
     */
    public function getContactInformation()
    {
        return $this->contact_information;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Actors
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set old_slug
     *
     * @param string $old_slug
     * @return Actors
     */
    public function setOldSlug($old_slug)
    {
        $this->old_slug = $old_slug;

        return $this;
    }

    /**
     * Get old_slug
     *
     * @return string
     */
    public function getOldSlug()
    {
        return $this->old_slug;
    }

    /**
     * Set content_short_generate
     *
     * @param boolean $content_short_generate
     * @return Actors
     */
    public function setContentShortGenerate($content_short_generate)
    {
        $this->content_short_generate = $content_short_generate;

        return $this;
    }

    /**
     * Get content_short_generate
     *
     * @return boolean
     */
    public function getContentShortGenerate()
    {
        return $this->content_short_generate;
    }

    /**
     * Set content_short
     *
     * @param string $content_short
     * @return Actors
     */
    public function setContentShort($content_short)
    {
        $this->content_short = $content_short;

        return $this;
    }

    /**
     * Get content_short
     *
     * @return string
     */
    public function getContentShort()
    {
        return $this->content_short;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Actors
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }
    
    /**
     * Set arrangement
     *
     * @param integer $arrangement
     * @return Actors
     */
    public function setArrangement($arrangement) {
        $this->arrangement = $arrangement;

        return $this;
    }

    /**
     * Get arrangement
     *
     * @return integer
     */
    public function getArrangement() {
        return $this->arrangement;
    }

    /**
     * Set photo
     *
     * @param string $photo
     * @return Actors
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get photo
     *
     * @return string
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * Set seo_generate
     *
     * @param boolean $seo_generate
     * @return Actors
     */
    public function setSeoGenerate($seo_generate)
    {
        $this->seo_generate = $seo_generate;

        return $this;
    }

    /**
     * Get seo_generate
     *
     * @return boolean
     */
    public function getSeoGenerate()
    {
        return $this->seo_generate;
    }

    /**
     * Set seo_title
     *
     * @param string $seo_title
     * @return Actors
     */
    public function setSeoTitle($seo_title)
    {
        $this->seo_title = $seo_title;

        return $this;
    }

    /**
     * Get seo_title
     *
     * @return string
     */
    public function getSeoTitle()
    {
        return $this->seo_title;
    }

    /**
     * Set seo_keywords
     *
     * @param string $seo_keywords
     * @return Actors
     */
    public function setSeoKeywords($seo_keywords)
    {
        $this->seo_keywords = $seo_keywords;

        return $this;
    }

    /**
     * Get seo_keywords
     *
     * @return string
     */
    public function getSeoKeywords()
    {
        return $this->seo_keywords;
    }

    /**
     * Set seo_description
     *
     * @param string $seo_description
     * @return Actors
     */
    public function setSeoDescription($seo_description)
    {
        $this->seo_description = $seo_description;

        return $this;
    }

    /**
     * Get seo_description
     *
     * @return string
     */
    public function getSeoDescription()
    {
        return $this->seo_description;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $created_at
     * @return Actors
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updated_at
     *
     * @param \DateTime $updated_at
     * @return Actors
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set published_at
     *
     * @param \DateTime $published_at
     * @return Actors
     */
    public function setPublishedAt($published_at)
    {
        $this->published_at = $published_at;

        return $this;
    }

    /**
     * Get published_at
     *
     * @return \DateTime
     */
    public function getPublishedAt()
    {
        return $this->published_at;
    }

    public function __toString()
    {
        if (is_null($this->getTitle())) {
            return 'NULL';
        }

        return $this->getTitle();
    }

    public function getThumbSize($type)
    {
        $size = array();
        switch ($type) {
            case 'list':
                $size['width'] = $this->listWidth;
                $size['height'] = $this->listHeight;
                break;
        }

        return $size;
    }

    public function getThumbWebPath($type)
    {
        if (empty($this->photo)) {
            return null;
        } else {
            $sThumbName = '';
            switch ($type) {
                case 'list':
                    $sThumbName = Tools::thumbName($this->photo, '_l');
                    break;
            }

            return '/' . $this->getUploadDir() . '/' . $sThumbName;
        }
    }

    public function getThumbAbsolutePath($type)
    {
        if (empty($this->photo)) {
            return null;
        } else {
            $sThumbName = '';
            switch ($type) {
                case 'list':
                    $sThumbName = Tools::thumbName($this->photo, '_l');
                    break;
            }

            return $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $sThumbName;
        }
    }

    public function getPhotoSize()
    {
        $temp = getimagesize($this->getPhotoAbsolutePath());
        $size = array(
            'width'  => $temp[0],
            'height' => $temp[1],
        );

        return $size;
    }

    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
    }

    public function getFile()
    {
        return $this->file;
    }

    public function deletePhoto()
    {
        if (!empty($this->photo)) {
            $filename = $this->getPhotoAbsolutePath();
            $filename_l = Tools::thumbName($filename, '_l');
            $filename_d = Tools::thumbName($filename, '_d');
            if (file_exists($filename)) {
                @unlink($filename);
            }
            if (file_exists($filename_l)) {
                @unlink($filename_l);
            }
            if (file_exists($filename_d)) {
                @unlink($filename_d);
            }
        }
    }

    public function getPhotoAbsolutePath()
    {
        return empty($this->photo) ? null : $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->photo;
    }

    public function getPhotoWebPath()
    {
        return empty($this->photo) ? null : '/' . $this->getUploadDir() . '/' . $this->photo;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . $this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
        return 'upload/actors';
    }

    public function upload()
    {
        if (null === $this->file) {
            return;
        }

        $sFileName = $this->getPhoto();

        $this->file->move($this->getUploadRootDir(), $sFileName);

        $this->createThumbs();

        unset($this->file);
    }

    public function createThumbs()
    {
        if (null !== $this->getPhotoAbsolutePath()) {
            $sFileName = $this->getPhoto();
            $sSourceName = $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $sFileName;

            //tworzy wszystkie miniatury wycinajac ze srodka zdjecia
            $thumb = \PhpThumbFactory::create($sSourceName);
            $sThumbNameL = Tools::thumbName($sSourceName, '_l');
            $aThumbSizeL = $this->getThumbSize('list');
            $thumb->adaptiveResize($aThumbSizeL['width'] + 2, $aThumbSizeL['height'] + 2);
            $thumb->crop(0, 0, $aThumbSizeL['width'], $aThumbSizeL['height']);
            $thumb->save($sThumbNameL);
        }
    }

    public function Thumb($x, $y, $x2, $y2, $type)
    {
        $sSourceName = $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->getPhoto();
        $sThumbName = $this->getThumbAbsolutePath($type);
        $aThumbSize = $this->getThumbSize($type);
        $thumb = \PhpThumbFactory::create($sSourceName);

        $cropWidth = $x2 - $x;
        $cropHeight = $y2 - $y;

        $thumb->crop($x, $y, $cropWidth, $cropHeight);
        $thumb->resize($aThumbSize['width'] + 2, $aThumbSize['height'] + 2);
        $thumb->crop(0, 0, $aThumbSize['width'], $aThumbSize['height']);
        $thumb->save($sThumbName);
    }

}