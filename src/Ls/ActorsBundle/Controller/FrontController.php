<?php

namespace Ls\ActorsBundle\Controller;

use Doctrine\DBAL\Types\Type;
use Ls\CoreBundle\Utils\Tools;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Actors controller.
 *
 */
class FrontController extends Controller {

    /**
     * Lists all Actors entities.
     *
     */
    public function indexAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $today = new \DateTime();

        $qb = $em->createQueryBuilder();
        $entities = $qb->select('a')
            ->from('LsActorsBundle:Actors', 'a')
            ->where($qb->expr()->isNotNull('a.published_at'))
            ->andWhere('a.published_at <= :today')
            ->setParameter('today', $today, Type::DATETIME)
            ->orderBy('a.arrangement', 'asc')
            ->getQuery()
            ->getResult();

        return $this->render('LsActorsBundle:Front:index.html.twig', array(
            'entities' => $entities
        ));
    }

    /**
     * Finds and displays a Actors entity.
     *
     */
    public function showAction($slug) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->createQueryBuilder()
            ->select('p')
            ->from('LsActorsBundle:Actors', 'p')
            ->where('p.slug = :slug')
            ->setParameter('slug', $slug)
            ->getQuery()
            ->getOneOrNullResult();

        if (null === $entity) {
            throw $this->createNotFoundException('Unable to find Actors entity.');
        }

        return $this->render('LsActorsBundle:Front:show.html.twig', array(
            'entity' => $entity,
        ));
    }
    
    public function sectionAction() {
        $em = $this->getDoctrine()->getManager();
        $today = new \DateTime();
        $qb = $em->createQueryBuilder();
        $entities = $qb->select('a')
            ->from('LsActorsBundle:Actors', 'a')
            ->where($qb->expr()->isNotNull('a.published_at'))
            ->andWhere('a.published_at <= :today')
            ->setParameter('today', $today, Type::DATETIME)
            ->orderBy('a.arrangement', 'asc')
            ->getQuery()
            ->getResult();

        return $this->render('LsActorsBundle:Front:section.html.twig', array(
            'entities' => $entities
        ));
    }
}
