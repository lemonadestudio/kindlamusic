<?php

namespace Ls\DictionaryBundle\Utils;

use Doctrine\ORM\EntityManager;
use Ls\DictionaryBundle\Entity\Dictionary;
use Symfony\Component\DependencyInjection\ContainerInterface;

class DictionaryUtils {

    protected $em;
    protected $repo;
    protected $container;
    protected $texts  = array();
    protected $is_loaded = false;

    public function __construct(EntityManager $em, ContainerInterface $container) {
        $this->em = $em;
        $this->container = $container;
    }

    public function get($name, $default) {
        if (array_key_exists($name, $this->texts)) {
            return $this->texts[$name];
        }
        return $default;
    }

    public function all() {
        $texts = array();

        if ($this->is_loaded) {
            return $this->texts;
        }

        foreach ($this->getRepo()->findAll() as $text) {
            if ($text->getType() == Dictionary::IMAGE) {
                $texts[$text->getLabel()] = $text->getThumbWebPath('detail');
            } else {
                $texts[$text->getLabel()] = $text->getValue();
            }
        }

        $this->texts = $texts;
        $this->is_loaded;

        return $texts;
    }

    protected function getRepo() {
        if ($this->repo === null) {
            $this->repo = $this->em->getRepository(get_class(new Dictionary()));
        }

        return $this->repo;
    }

}
