<?php

namespace Ls\DictionaryBundle\Extensions;

use Ls\DictionaryBundle\Utils\DictionaryUtils;

class DictionaryExtension extends \Twig_Extension implements \Twig_Extension_GlobalsInterface {

    protected $cms_text;

    function __construct(DictionaryUtils $text) {
        $this->cms_text = $text;
    }

    public function getGlobals() {
        return array(
            'cms_text' => $this->cms_text,
            'dictionary' => $this->cms_text->all(),
        );
    }

    public function getName() {
        return 'cms_text';
    }

}