<?php

namespace Ls\DictionaryBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Ls\DictionaryBundle\Entity\Dictionary;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class DictionaryType extends AbstractType {
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $object = $event->getData();
            $form = $event->getForm();
            
            if ($object->getId() == null ) {
                $form->add('label', null, array(
                    'label' => 'Etykieta',
                    'constraints' => array(
                        new NotBlank(array(
                            'message' => 'Wypełnij pole'
                        ))
                    )
                ));
                
                $form->add('type', ChoiceType::class, array(
                    'label' => 'Rodzaj',
                    'choices'  => Dictionary::getTypesList()
                ));
                
                $form->add('imageWidth', null, array(
                    'label' => 'Szerokość zdjęcia'
                ));
                
                $form->add('imageHeight', null, array(
                    'label' => 'Wysokość zdjęcia'
                ));
                
                $form->add('value', null, array(
                    'label' => 'Wartość',
                    'attr' => array(
                        'rows' => 5
                    )
                ));
            } else {
                $form->add('label', null, array(
                    'label' => 'Etykieta',
                    'disabled' => true
                ));
                
                if ($object->getType() != Dictionary::IMAGE) {
                    $form->add('value', null, array(
                        'label' => 'Wartość',
                        'attr' => array(
                            'rows' => 5
                        )
                    ));
                }
                
                if ($object->getType() == Dictionary::IMAGE) {
                    $size = $object->getThumbSize('detail');
                    $form->add('file', FileType::class, array(
                        'label' => 'Nowe zdjęcie',
                        'constraints' => array(
                            new Image(array(
                                'minWidth' => $size['width'],
                                'minHeight' => $size['height'],
                                'minWidthMessage' => 'Szerokość zdjęcie musi być większa niż ' . $size['width'] . 'px',
                                'minHeightMessage' => 'Wysokość zdjęcie musi być większa niż ' . $size['height'] . 'px',
                            ))
                        )
                    ));
                }
            }
        });
        
        $builder->add('description', null, array(
            'label' => 'Opis',
        ));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Ls\DictionaryBundle\Entity\Dictionary',
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix() {
        return 'form_admin_dictionary';
    }
}
