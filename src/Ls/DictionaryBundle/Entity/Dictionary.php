<?php

namespace Ls\DictionaryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ls\CoreBundle\Utils\Tools;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Dictionary
 *
 * @ORM\Entity
 * @ORM\Table(name="dictionary")
 */
class Dictionary
{
    const TEXT = 1;
    const EDITOR = 2;
    const IMAGE = 3;
    
    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $label;
    
    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $type;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    private $value;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $photo;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $imageWidth;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $imageHeight;
    
    protected $file;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set label
     *
     * @param string $label
     * @return Dictionary
     */
    public function setLabel($label) {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label
     *
     * @return string 
     */
    public function getLabel() {
        return $this->label;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Dictionary
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return Dictionary
     */
    public function setValue($value) {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string 
     */
    public function getValue() {
        return $this->value;
    }

    public function __toString() {
        if (is_null($this->getDescription())) {
            return 'NULL';
        }
        return $this->getDescription();
    }
    
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    public function getType()
    {
        return $this->type;
    }
    
    static public function getTypesList() {
        return [
            self::TEXT => 'Pole tekstowe',
            self::EDITOR => 'Pole z edytorem',
            self::IMAGE => 'Zdjęcie'
        ];
    }
    
    public function getTypeName() {
        switch ($this->type) {
            case self::TEXT:
                return "Pole tekstowe";
            case self::EDITOR:
                return "Pole z edytorem";
            case self::IMAGE:
                return "Zdjęcie";
            default:
                return "brak";
        }
    }
    
    /**
     * Set photo
     *
     * @param string $photo
     * @return Composer
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get photo
     *
     * @return string
     */
    public function getPhoto()
    {
        return $this->photo;
    }
    
    public function setImageWidth($imageWidth = null) {
        $this->imageWidth = $imageWidth;

        return $this;
    }

    public function getImageWidth() {
        return $this->imageWidth;
    }

    public function setImageHeight($imageHeight) {
        $this->imageHeight = $imageHeight;

        return $this;
    }

    public function getImageHeight() {
        return $this->imageHeight;
    }
    
    public function getThumbSize($type)
    {
        $size = array();
        switch ($type) {
            case 'detail':
                $size['width'] = $this->imageWidth;
                $size['height'] = $this->imageHeight;
                break;
        }

        return $size;
    }

    public function getThumbWebPath($type)
    {
        if (empty($this->photo)) {
            return null;
        } else {
            $sThumbName = '';
            switch ($type) {
                case 'detail':
                    $sThumbName = Tools::thumbName($this->photo, '_d');
                    break;
            }

            return '/' . $this->getUploadDir() . '/' . $sThumbName;
        }
    }

    public function getThumbAbsolutePath($type)
    {
        if (empty($this->photo)) {
            return null;
        } else {
            $sThumbName = '';
            switch ($type) {
                case 'detail':
                    $sThumbName = Tools::thumbName($this->photo, '_d');
                    break;
            }

            return $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $sThumbName;
        }
    }

    public function getPhotoSize()
    {
        $temp = getimagesize($this->getPhotoAbsolutePath());
        $size = array(
            'width'  => $temp[0],
            'height' => $temp[1],
        );

        return $size;
    }

    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
    }

    public function getFile()
    {
        return $this->file;
    }

    public function deletePhoto()
    {
        if (!empty($this->photo)) {
            $filename = $this->getPhotoAbsolutePath();
            $filename_d = Tools::thumbName($filename, '_d');
            if (file_exists($filename)) {
                @unlink($filename);
            }
            if (file_exists($filename_d)) {
                @unlink($filename_d);
            }
        }
    }

    public function getPhotoAbsolutePath()
    {
        return empty($this->photo) ? null : $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->photo;
    }

    public function getPhotoWebPath()
    {
        return empty($this->photo) ? null : '/' . $this->getUploadDir() . '/' . $this->photo;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . $this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
        return 'upload/dictionary';
    }

    public function upload()
    {
        if (null === $this->file) {
            return;
        }

        $sFileName = $this->getPhoto();

        $this->file->move($this->getUploadRootDir(), $sFileName);

        $this->createThumbs();

        unset($this->file);
    }

    public function createThumbs()
    {
        if (null !== $this->getPhotoAbsolutePath()) {
            $sFileName = $this->getPhoto();
            $sSourceName = $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $sFileName;

            //tworzy wszystkie miniatury wycinajac ze srodka zdjecia
            $thumb = \PhpThumbFactory::create($sSourceName);
            $sThumbNameD = Tools::thumbName($sSourceName, '_d');
            $aThumbSizeD = $this->getThumbSize('detail');
            $thumb->adaptiveResize($aThumbSizeD['width'] + 2, $aThumbSizeD['height'] + 2);
            $thumb->crop(0, 0, $aThumbSizeD['width'], $aThumbSizeD['height']);
            $thumb->save($sThumbNameD);
        }
    }

    public function Thumb($x, $y, $x2, $y2, $type)
    {
        $sSourceName = $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->getPhoto();
        $sThumbName = $this->getThumbAbsolutePath($type);
        $aThumbSize = $this->getThumbSize($type);
        $thumb = \PhpThumbFactory::create($sSourceName);

        $cropWidth = $x2 - $x;
        $cropHeight = $y2 - $y;

        $thumb->crop($x, $y, $cropWidth, $cropHeight);
        $thumb->resize($aThumbSize['width'] + 2, $aThumbSize['height'] + 2);
        $thumb->crop(0, 0, $aThumbSize['width'], $aThumbSize['height']);
        $thumb->save($sThumbName);
    }
    
}