<?php

namespace Ls\SettingBundle\Extensions;

use Ls\SettingBundle\Utils\Config;

class SettingsExtension extends \Twig_Extension implements \Twig_Extension_GlobalsInterface {

    protected $cms_config;

    function __construct(Config $config) {
        $this->cms_config = $config;
    }

    public function getGlobals() {
        return array(
            'cms_config' => $this->cms_config,
            'settings' => $this->cms_config->all(),
        );
    }

    public function getName() {
        return 'cms_config';
    }

}