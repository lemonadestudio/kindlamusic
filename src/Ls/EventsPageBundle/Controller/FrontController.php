<?php

namespace Ls\EventsPageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * EventsPage controller.
 *
 */
class FrontController extends Controller {
    /**
     * Finds and displays a EventsPage entity.
     *
     */
    public function showAction($slug) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->createQueryBuilder()
            ->select('p')
            ->from('LsEventsPageBundle:EventsPage', 'p')
            ->where('p.slug = :slug')
            ->setParameter('slug', $slug)
            ->getQuery()
            ->getOneOrNullResult();

        if (null === $entity) {
            throw $this->createNotFoundException('Unable to find EventsPage entity.');
        }

        return $this->render('LsEventsPageBundle:Front:show.html.twig', array(
            'entity' => $entity,
        ));
    }
}
