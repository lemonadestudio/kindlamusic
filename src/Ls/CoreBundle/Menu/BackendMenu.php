<?php

namespace Ls\CoreBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class BackendMenu implements ContainerAwareInterface {

    use ContainerAwareTrait;

    protected $containerBuilder;

    public function managedMenu(FactoryInterface $factory) {
        $request = $this->container->get('request');
        $blocks = $this->container->getParameter('ls_core.admin.menu');

        $menu = $factory->createItem('root');
        $menu->setChildrenAttribute('class', 'nav navbar-nav');

        $route = $request->get('_route');
        $set_current = false;
        foreach ($blocks as $block) {
            $parent = $menu->addChild($block['title'], array(
                'uri' => '#',
                'linkAttributes' => array(
                    'class' => 'dropdown-toggle',
                    'data-toggle' => 'dropdown'
                )
            ));
            $parent->setAttribute('class', 'dropdown');
            foreach ($block['links'] as $link) {
                $service = $this->container->get($link);
                $set_current = $service->addToMenu($parent, $route, $set_current);
            }
        }

        return $menu;
    }
}

