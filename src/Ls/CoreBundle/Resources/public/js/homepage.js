lazyLoadImages();
$(window).on('resize orientationchange', function() {
    lazyLoadImages();
    
    if ($('.container').width() < 700) {
        enabledMouseMove = false;
    } else {
        enabledMouseMove = true;
    }
});

function lazyLoadImages() {
    $("[data-src]").each(function() {
        var src = $(this).data('src');
        var currentSrc = $(this).attr('src');
        if (src !== currentSrc && src.length > 0) {
            $(this).attr('src', src);
            $(this).css('visibility', 'visible');
        }
    });
}

fixedHeader();
$(window).scroll(function() {
    fixedHeader();
    animateOnScroll();
});

animateOnScroll();
function animateOnScroll() {
    var scrollTop = $(window).scrollTop();
    var windowHeight = $(window).height();
    
    $('[data-aos]:not(.custom-fade-up)').each(function(index, item) {
        var itemPosition = $(item).offset().top;
        
        if (scrollTop + (windowHeight - windowHeight/10) >= itemPosition) {
            $(item).addClass('custom-fade-up');
        }
    });
}

function fixedHeader() {
    var scroll = $(window).scrollTop();
    if (scroll > 0) {
       $('#header').addClass('fixed');
       
    } else {
        $('#header').removeClass('fixed');
    }
}

$(document).on('click', 'a[href^="#"]', function (event) {
    if ($.attr(this, 'href').length > 1 && $($.attr(this, 'href')).length > 0) {
        event.preventDefault();
        closeMobileMenu();

        $('html, body').animate({
            scrollTop: $($.attr(this, 'href')).offset().top + 1
        }, 500);
    } else {
        location.href = "/" + $.attr(this, 'href');
    }
});

function closeMobileMenu() {
    $('.mobile-menu').removeClass('open');
}

function openMobileMenu() {
    $('.mobile-menu').addClass('open');
}

var partnersSlider;
var partnersSliderSettings;

partnersSliderSettings = {
    pause: 3000,
    speed: 500,
    mode: "horizontal",
    auto: true,
    pager: false,
    controls: false,
    touchEnabled: false,
    preventDefaultSwipeX: true,
    preventDetaultSwipeY: false,
    oneToOneTouch: false,
    autoHover: false,
    slideSelector: 'a',
    minSlides: 1,
    maxSlides: 7,
    slideWidth: 170,
    slideMargin: 16,
    infiniteLoop: true,
    moveSlides: 1,
    onSliderResize: reloadPartnersSliderBX,
    onSliderLoad: function (currentIndex) {
        setTimeout(function() {
            $(".partners-wrapper").css("visibility", "visible");
        }, 1);
    }
};

if ($('.partners').length > 0) {
    partnersSlider = $('.partners').bxSlider(partnersSliderSettings);
}

/*
Resize Callback 
*/ // Stores the current slide index.
function reloadPartnersSliderBX(idx) {
    var containerWidth = $('.partner-section .container').width();

    if (containerWidth < 750) {
        partnersSliderSettings.auto = true;
    } else {
        if (partnersSlider.getSlideCount() > 7) {
            partnersSliderSettings.auto = true;
        } else {
            partnersSliderSettings.auto = false;
        }
    }

    partnersSlider.reloadSlider(partnersSliderSettings);
}

function isGoogleRecaptchaValid(id) {
    var isValid = false;
    $('.g-recaptcha').each(function(i, item) {
        if ($(item).attr('id') == id) {
            var grecaptchaResponse = grecaptcha.getResponse(i);
            if (grecaptchaResponse.length > 0) {
                isValid = true;
            }
        }
    });
    
    return isValid;
}

function resetGoogleRecaptcha() {
    $('.g-recaptcha').each(function(i, item) {
        grecaptcha.reset(i);
    });
}

function showError(selector, message) {
    tippy(selector, {
        content: message,
        showOnCreate: true,
        trigger: 'manual',
        boundary: 'window',
        appendTo: 'parent'
    });
}

function checkEmail(emailAddress) {
    var re = /^[a-zA-Z0-9._\-]+@[a-zA-Z0-9.\-]+\.[a-zA-Z]{2,6}$/;
    if (re.test(emailAddress)) {
        return true;
    }

    return false;
}

$("#form_contact").submit(function( event ) {
    event.preventDefault();
    sendContactForm($(this).attr('action'), $(this).serialize(), event);
});

function sendContactForm(path, send_data, event) {
    var send = true;
    
    var data = {
        name: $('#form_contact_name').val(),
        phone: $('#form_contact_phone').val(),
        email: $('#form_contact_email').val(),
        message: $('#form_contact_content').val(),
        accept: $('#form_contact_accept').prop('checked')
    };
    
    if (data.name.length === 0) {
        showError('#form_contact_name', "Wypełnij pole");
        send = true;
    }
    
    if (data.phone.length === 0) {
        showError('#form_contact_phone', "Wypełnij pole");
        send = true;
    }
        
    if (data.email.length === 0) {
        showError('#form_contact_email', "Wypełnij pole");
        send = true;
    } else {
        if (!(checkEmail(data.email))) {
            showError('#form_contact_email', "Podaj poprawny adres email");
            send = true;
        }
    }
    
    if (data.message.length === 0) {
        showError('#form_contact_content', "Wypełnij pole");
        send = true;
    }
    
    if (!isGoogleRecaptchaValid('form_contact_google_recaptcha')) {
        send = false;
        showError('#form_contact .form-group .recaptcha', "Zaznacz wymagane pole");
    }
    
    if (!data.accept) {
        send = false;
        showError('#form_contact .form-group.accept .checkmark', "Zaznacz wymagane pole");
    }
    
    if (send) {
        $.ajax({
            type: 'POST',
            url: path,
            data: send_data
        }).done(function(response) {
            resetGoogleRecaptcha();
            
            if (response === 'OK') {
                $('#form_contact_name').val('');
                $('#form_contact_phone').val('');
                $('#form_contact_email').val('');
                $('#form_contact_content').val('');
                $('#form_contact_accept').prop('checked', false);
                
                $("#form_contact").hide();
                
                $('#contact-message .error').hide();
                $('#contact-message').show();
                $('#contact-message .success').show();
            } else {
                $("#form_contact").hide();
                
                $('#contact-message .success').hide();
                $('#contact-message').show();
                $('#contact-message .error').show();
            }
        });
    }

    return false;
}

function closeContactMessage() {
    $('#contact-message').hide();
    $("#form_contact").show();
}

var videoSlider;
var players = [];
function onYouTubeIframeAPIReady() {
  $.each(ytVideos, function(index, item) {
    var player = new YT.Player('video-'+item, {
        height: '363',
        width: '647',
        videoId: item,
        host: 'https://www.youtube.com',
        playerVars: {
            'playsinline': 1,
            'origin': window.location.origin
        }
    });
    
    players.push(player);
    
  });
  
  if ($('.video-slider').length > 0) {
      videoSlider = $('.video-slider').bxSlider(videoSliderSettings);
  }
}

var videoSliderSettings = {
    slideWidth: 647,
    maxSlides: 1,
    minSlides: 1,
    slideMargin: 0,
    moveSlides: 1,
    infiniteLoop: true,
    pager: false,
    hideControlOnEnd: true,
    useCSS: false,
    nextText: '<img class="icon" src="/bundles/lscore/images/homepage/slide-control.svg" alt="Poprzedni slide"><img class="icon hover" src="/bundles/lscore/images/homepage/slide-control-hover.svg" alt="Poprzedni slide">',
    prevText: '<img class="icon" src="/bundles/lscore/images/homepage/slide-control.svg" alt="Następny slide"><img class="icon hover" src="/bundles/lscore/images/homepage/slide-control-hover.svg" alt="Następny slide">',
    onSliderResize: reloadVideoSliderBX,
    onSliderLoad: function (currentIndex) {
        setTimeout(function() {
            $(".video-slider .item").removeClass('current');
            $('.video-slider .item-'+currentIndex).addClass('current');
            
            $(".video-slider").css("visibility", "visible");
        }, 1);
    },
    onSlideBefore: function ($slideElement, oldIndex, newIndex) {
        $(".video-slider .item").removeClass('current');
        $slideElement.addClass('current');
        
        $.each(players, function(index, item) {
            item.stopVideo();
        });
    }
};

function reloadVideoSliderBX() {
    videoSlider.reloadSlider(videoSliderSettings);
}


var actorsSliderSettings = {
    slideWidth: 245,
    maxSlides: 5,
    minSlides: 1,
    slideMargin: 17,
    moveSlides: 1,
    infiniteLoop: true,
    pager: false,
    hideControlOnEnd: true,
    useCSS: false,
    nextText: '<img class="icon" src="/bundles/lscore/images/homepage/slide-control.svg" alt="Poprzedni slide"><img class="icon hover" src="/bundles/lscore/images/homepage/slide-control-hover.svg" alt="Poprzedni slide">',
    prevText: '<img class="icon" src="/bundles/lscore/images/homepage/slide-control.svg" alt="Następny slide"><img class="icon hover" src="/bundles/lscore/images/homepage/slide-control-hover.svg" alt="Następny slide">',
    onSliderResize: reloadActorsSliderBX,
    onSliderLoad: function (currentIndex) {
        setTimeout(function() {
            $(".actors-slider").css("visibility", "visible");
        }, 1);
    },
    onSlideBefore: function ($slideElement, oldIndex, newIndex) {
        $(".actors-slider .item").removeClass('current');
        $slideElement.addClass('current');
    }
};

var containerWidth = $(document).width();
if (containerWidth <= 767) {
    actorsSliderSettings.slideWidth = 204;
    actorsSliderSettings.slideMargin = 15;
    actorsSliderSettings.maxSlides = 1;
} else {
    actorsSliderSettings.slideWidth = 245;
    actorsSliderSettings.slideMargin = 17;
    actorsSliderSettings.maxSlides = 5;
}

if ($('.actors-slider').length > 0) {
    var actorsSlider = $('.actors-slider').bxSlider(actorsSliderSettings);
}

function reloadActorsSliderBX() {
    var containerWidth = $(document).width();
    if (containerWidth <= 767) {
        actorsSliderSettings.slideWidth = 204;
        actorsSliderSettings.slideMargin = 15;
        actorsSliderSettings.maxSlides = 1;
    } else {
        actorsSliderSettings.slideWidth = 245;
        actorsSliderSettings.slideMargin = 17;
        actorsSliderSettings.maxSlides = 5;
    }
    
    actorsSlider.reloadSlider(actorsSliderSettings);
}

function closeCookies() {
    $('.cookies-container').remove();
    
    $.ajax({
        type: 'post',
        url: '/close-cookies'
    });
}