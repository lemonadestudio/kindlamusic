function toggleDropdown(_this) {
    if ($('.container').width() < 1000) {
        $(_this).parent().toggleClass('open');
        $(_this).parent().find('.items-wrapper').slideToggle();
    }
}

function acceptCookies(event) {
    event.preventDefault();
    localStorage.setItem('accept-cookies', true);
    $('.cookies-info').hide();
}

if (!localStorage.getItem('accept-cookies')) {
    $('.cookies-info').show();
}