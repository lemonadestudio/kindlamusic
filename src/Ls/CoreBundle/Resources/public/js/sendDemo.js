function showModal() {
    $('.send-modal-demo').fadeIn();
    $('body').addClass('modal-open');
}

$(".send-modal-demo").on("click", function (event) {
    if ($(event.target).attr('class') === $(this).attr('class')) {
        $(this).fadeOut();
        $('body').removeClass('modal-open');
    }
});

$(document).on('change', '.btn-file:file', function () {
    var input = document.getElementById('form_send_demo_uploadFiles');
    var namesContainer = $('.file-names');
    namesContainer.html("");
    for (i = 0; i < input.files.length; i++) {
        var fileName = "<div class='name'>" + input.files[i].name + "</div>";
        namesContainer.append(fileName);
    }
});

// Waliduje formularz kontaktowy
function sendDemo(path, send_data) {
    $('div[id^="qtip-"]').each(function () {
        _qtip2 = $(this).data("qtip");
        if (_qtip2 != undefined) {
            _qtip2.destroy(true);
        }
    });

    var send = true;
    var data = {
        name: $('#form_send_demo_name').val(),
        email: $('#form_send_demo_email').val(),
        message: $('#form_send_demo_message').val()
    };
    

    if (data.name.length == 0) {
        send = false;
        showError('#form_send_demo_name', 'Wypełnij pole.')
    }
    

    if (data.email.length == 0 ) {
        send = false;
        showError('#form_send_demo_email', 'Podaj adres e-mail.')
    } else if (data.email.length != 0) {
        if (!checkEmail(data.email)) {
            send = false;
            showError('#form_send_demo_email', 'Podaj poprawny email.')
        }
    }

    if (data.message.length == 0) {
        send = false;
        showError('#form_send_demo_message', 'Wypełnij pole.')
    }
    
    if (send) {
        $.ajax({
            type: 'post',
            url: path,
            data: new FormData( send_data[0] ),
            processData: false,
            contentType: false,
            success: function (response) {
                if (response === 'OK') {
                    $('#form_send_demo_name').val('');
                    $('#form_send_demo_email').val('');
                    $('#form_send_demo_message').val('');
                    
                        $('.message').show().html('<p class="green-color">Twoja wiadomość została wysłana.</p>')
                        $('.message').delay(5000).fadeOut();
                } else {
                    $('.message').show().html('<p class="red-color">Sprawdź pola formularza.</p>')
                    $('.message').delay(5000).fadeOut();
                }
            }
        });
    }

    return false;
}

// Sprawdza poprawnoĹÄ adresu email
function checkEmail(emailAddress) {
    var re = /^[a-zA-Z0-9._\-]+@[a-zA-Z0-9.\-]+\.[a-zA-Z]{2,6}$/;
    if (re.test(emailAddress)) {
        return true;
    }

    return false;
}

// WyĹwietla bĹÄdy z walidacji.
function showError(field, error) {
    $(field).qtip({
        content: {
            title: null,
            text: error
        },
        position: {
            my: 'bottom left',
            at: 'top right',
            adjust: {
                x: -30
            }
        },
        show: {
            ready: true,
            event: false
        },
        hide: {
            event: 'click focus unfocus'
        },
        style: {
            classes: 'qtip-red qtip-rounded qtip-shadow'
        }
    });
}