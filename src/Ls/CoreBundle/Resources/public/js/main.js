var audio = {
    song: new Audio(),
    canUpdate: false,
    loaded: false,
    autoplay: false
};

audio.song.addEventListener('loadedmetadata', function () {
    $('.audio-player-container').find('.progress').find('.bar').css('width', '0');
    $('.audio-player-container').find('.progress').find('.handle').css('left', '0');
    $('.audio-player-container').find('.to').html(audio.song.duration.toTimeString());
    audio.loaded = true;
    audio.canUpdate = true;
    if (audio.autoplay) {
        audio.song.play();
        $('.play').addClass('active');
        $('.audio-player-container').find('.songs').find('.selected').addClass('playing');
    }
});

audio.song.addEventListener('timeupdate', function () {
    var timePercent = this.currentTime / this.duration;
    var progress_width = $('.audio-player-container').find('.progress').width();
    var width = Math.round(progress_width * timePercent);

    if (audio.canUpdate) {
        $('.audio-player-container').find('.progress').find('.handle').css('left', width + 'px');
    }
    $('.audio-player-container').find('.progress').find('.from').html(audio.song.currentTime.toTimeString());
    $('.audio-player-container').find('.progress').find('.bar').css('width', width + 'px');
});

audio.song.addEventListener('ended', function () {
    var total = $('.audio-player-container').find('.songs').find('.song').length;
    var active = $('.audio-player-container').find('.songs').find('.selected');
    var index = $('.audio-player-container').find('.songs').find('.song').index(active) + 1;

    if (index < total) {
        var next = active.next().next();
        var src = next.find('span').attr('data-src');
        var title = next.find('span').html();
        $('.song').removeClass('selected');
        $('.song').removeClass('playing');
        next.addClass('selected');
        $('.audio-player-container').find('.current').html(title);
        audio.song.pause();
        audio.song.src = src;
    } else {
        $('.audio-player-container').find('.progress').find('.bar').css('width', '0');
        $('.audio-player-container').find('.progress').find('.handle').css('left', '0');
        $('.song').removeClass('active');
        $('.play').removeClass('active');
        $('.audio-player-container').find('.current').html('');
        $('.audio-player-container').find('.to').html('0:00');
        $('.audio-player-container').find('.from').html('0:00');
        audio.song.src = '';
        audio.loaded = false;
        audio.canUpdate = false;
    }
});

$(document).on('click', '.song', function () {
    var src = $(this).find('span').attr('data-src');
    var id = $(this).find('span').attr('data-song-id');
    var title = $(this).find('span').html();
    $('.song').removeClass('selected');
    $('.song').removeClass('playing');
    $(this).addClass('selected');
    $('.audio-player-container').find('.current').html(title);
    audio.autoplay = true;
    audio.song.pause();
    audio.song.src = src;

    var form = $('#form_front_audio_file_download');
    var data = {};
    var url = form.attr('action');
    $.ajax({
        type: "post",
        url: url,
        data: data,
        success: function (response) {
            $('.send-form-bottom').html(response);
            $('#form_front_audio_file_download_file_id').val(id);
        }
    });
});

$(document).on('click', '.play', function () {
    if (audio.loaded) {
        if ($(this).hasClass('active')) {
            audio.song.pause();
            $('.play').removeClass('active');
            $('.audio-player-container').find('.songs').find('.selected').removeClass('playing');
        } else {
            audio.autoplay = true;
            audio.song.play();
            $('.play').addClass('active');
            $('.audio-player-container').find('.songs').find('.selected').addClass('playing');
        }
    }
});

$(document).on('click', '.stop', function () {
    if (audio.loaded) {
        audio.song.pause();
        audio.song.currentTime = 0;
        $('.play').removeClass('active');
        $('.audio-player-container').find('.songs').find('.selected').removeClass('playing');
    }
});

$(document).ready(function () {
    $('#handle').draggable({
        containment: "#handle-container",
        handle: ".handle-icon",
        axis: "x",
        scroll: false,
        start: function (event, ui) {
            audio.canUpdate = false;
        },
        stop: function (event, ui) {
            audio.canUpdate = true;
            var progress_width = $('.audio-player-container').find('.progress').width();
            var percentValue = ui.position.left / progress_width;
            if (audio.loaded) {
                audio.song.currentTime = audio.song.duration * percentValue;
            }
        }
    });

    if ($('.audio-player-container').length > 0) {
        var active = $('.audio-player-container').find('.songs').find('.selected');
        if (active.length > 0) {
            var src = active.find('span').attr('data-src');
            var id = active.find('span').attr('data-song-id');
            var title = active.find('span').html();
            $('.audio-player-container').find('.current').html(title);
            audio.song.pause();
            audio.song.src = src;
            $('#form_front_audio_file_download_file_id').val(id);
        }
    }
});

Number.prototype.toTimeString = function () {
    var sec_num = parseInt(this, 10); // don't forget the second param
    var hours = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);

    var time = '';

    if (hours > 0) {
        if (hours < 10) {
            hours = "0" + hours;
        }

        time += hours + ':';

    }
    if (minutes < 10 && time.length > 0) {
        minutes = "0" + minutes;
    }
    if (seconds < 10) {
        seconds = "0" + seconds;
    }

    time += minutes + ':' + seconds;

    return time;
}

$(document).on('click', '#show-send-form', function () {
    if ($(this).is(':checked')) {
        $('.send-form-bottom').slideDown('fast')
    } else {
        $('.send-form-bottom').find('.form-error').html('');
        $('.send-form-bottom').slideUp('fast')
    }
});


function sendFile() {
    var form = $('#form_front_audio_file_download');
    var data = form.serialize();
    var url = form.attr('action');

    $('.song-ajax-loader').attr('src', '/bundles/lscore/images/ajax_loader_blue.gif');
    $('.song-ajax-loader').show();
    $.ajax({
        type: "post",
        url: url,
        data: data,
        success: function (response) {
            $('.send-form-bottom').html(response);
        }
    });

    return false;
}