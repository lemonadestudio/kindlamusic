var slider;

slider=$('.partners').bxSlider({
    auto: true,
    pager: false,
    speed: 2000,
    minSlides: 1,
    maxSlides: 5,
    slideWidth: 110,
    slideMargin: 105,
    controls: false,
    slideSelector: 'a',
    onSliderResize: reloadBX,
    onSliderLoad: function () {
        $(".partners").css("visibility", "visible");
    }
});


/*
Resize Callback 
*/ // Stores the current slide index.
function reloadBX(idx) {
    setTimeout(slider.reloadSlider, 0);
    var current = parseInt(idx, 10);
    slider.goToSlide(current);
    slider.startAuto();
    localStorage.removeItem("cache");
}

var logo_slider;

logo_slider=$('.logos').bxSlider({
    auto: true,
    pager: false,
    speed: 2000,
    minSlides: 1,
    maxSlides: 5,
    slideWidth: 110,
    slideMargin: 25,
    controls: false,
    slideSelector: 'a',
    onSliderResize: reloadBX,
    onSliderLoad: function () {
        $(".logos").css("visibility", "visible");
    }
});


/*
Resize Callback 
*/ // Stores the current slide index.
function reloadBX(idx) {
    setTimeout(logo_slider.reloadSlider, 0);
    var current = parseInt(idx, 10);
    logo_slider.goToSlide(current);
    logo_slider.startAuto();
    localStorage.removeItem("cache");
}

var kmeventsSlider;

kmeventsSlider=$('.kmeventsSlider').bxSlider({
    auto: true,
    pager: false,
    speed: 2000,
    minSlides: 1,
    maxSlides: 5,
    slideWidth: 110,
    slideMargin: 105,
});