function closePopup(path) {
    $('#popup').remove();
    
    $.ajax({
        type: "GET",
        url: path,
        dataType: "json", 
        success: function (response) {
        },
        error: function (response) {
        }
    });
}