$(function () {
    fckconfig_common = {
        skin: 'BootstrapCK-Skin',
        contentsCss: ['/bundles/lscore/css/editor.css'],
        bodyClass: 'editor',
        stylesSet: [
            {name: 'Normalny', element: 'p', attributes: {'class': ''}},
            {name: 'Czerwony', element: 'p', attributes: {'class': 'red'}},
            {name: 'Media - tytuł', element: 'p', attributes: {'class': 'media-page-title'}}
        ],
        filebrowserBrowseUrl: kcfinderBrowseUrl + '?type=files',
        filebrowserImageBrowseUrl: kcfinderBrowseUrl + '?type=images',
        filebrowserFlashBrowseUrl: kcfinderBrowseUrl + '?type=flash',
        filebrowserUploadUrl: '/bundles/lsadmin/kcfinder/upload.php?type=files',
        filebrowserImageUploadUrl: '/bundles/lsadmin/kcfinder/upload.php?type=images',
        filebrowserFlashUploadUrl: '/bundles/lsadmin/kcfinder/upload.php?type=flash'
    };
    
    fckconfig_common2 = {
        skin: 'BootstrapCK-Skin',
        contentsCss: ['/bundles/lscore/css/editor.css'],
        bodyClass: 'editor',
        stylesSet: [
            {name: 'Normalny 1', element: 'p', attributes: {'class': 'roboto'}},
            {name: 'Normalny 2', element: 'p', attributes: {'class': 'lato'}},
            {name: 'Czerwony', element: 'p', attributes: {'class': 'red'}},
            {name: 'Nagłówek', element: 'h2', attributes: {'class': 'head2'}},
            {name: 'Media - tytuł', element: 'p', attributes: {'class': 'media-page-title'}}
        ],
        filebrowserBrowseUrl: kcfinderBrowseUrl + '?type=files',
        filebrowserImageBrowseUrl: kcfinderBrowseUrl + '?type=images',
        filebrowserFlashBrowseUrl: kcfinderBrowseUrl + '?type=flash',
        filebrowserUploadUrl: '/bundles/lsadmin/kcfinder/upload.php?type=files',
        filebrowserImageUploadUrl: '/bundles/lsadmin/kcfinder/upload.php?type=images',
        filebrowserFlashUploadUrl: '/bundles/lsadmin/kcfinder/upload.php?type=flash'
    };

    fckconfig = jQuery.extend(true, {
        height: '400px',
        width: 'auto'
    }, fckconfig_common);
    
    fckconfig2 = jQuery.extend(true, {
        height: '400px',
        width: 'auto'
    }, fckconfig_common2);

    $('.wysiwyg').ckeditor(fckconfig);
    $('.wysiwyg2').ckeditor(fckconfig2);
});