<?php

namespace Ls\CoreBundle\Controller;

use Ls\AudioPlayerBundle\Entity\AudioFileDownload;
use Ls\AudioPlayerBundle\Form\AudioFileDownloadType;
use Ls\CoreBundle\Helper\AdminBlock;
use Ls\CoreBundle\Helper\AdminDashboard;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Response;
use Ls\UserBundle\Entity\User;

class DefaultController extends Controller {
    protected $containerBuilder;
    
    public function indexAction(Request $request) {
        
        if ($request->query->has('_locale')) {
            return $this->redirect($this->generateUrl('ls_core_homepage'));
        }
        
        $em = $this->getDoctrine()->getManager();
        
        $videoGallery = $em->createQueryBuilder()
            ->select('g', 'm')
            ->from('LsGalleryBundle:Gallery', 'g')
            ->leftJoin('g.movies', 'm')
            ->where('g.name = :galleryName')
            ->setParameter('galleryName', "Filmy na stronie głównej kmevents")
            ->getQuery()
            ->getOneOrNullResult();
        
        return $this->render('LsCoreBundle:Default:index_new.html.twig', array(
            'videoGallery' => $videoGallery
        ));
    }

    public function closeCookiesAction() 
    {
        $response = new Response("OK");
        
        $response->headers->setCookie(new Cookie('close-cookies', '1', time() + 31536000));
        $response->sendHeaders();
        
        return new $response;
    }
    
    public function playerAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        if ($request->query->has('_locale')) {
            return $this->redirect($this->generateUrl('ls_core_player'));
        }
        
        $curentlyUser = $this->get('security.context')->getToken()->getUser();
        if ($curentlyUser instanceof User) {
            $userData = $curentlyUser;
        } else {
            return $this->redirect($this->generateUrl('ls_user_login'));
        }
        
        $user = $em->getRepository('LsUserBundle:User')->find($userData->getId());

        $songs = $em->createQueryBuilder()
            ->select('e', 'c', 'u')
            ->from('LsAudioPlayerBundle:AudioFile', 'e')
            ->leftJoin('e.category', 'c')
            ->leftJoin('c.users', 'u')
            ->where('u.id = :user_id')
            ->setParameter('user_id', $user->getId())
            ->orderBy('e.arrangement', 'ASC')
            ->getQuery()
            ->getResult();

        $form = $this->createForm(AudioFileDownloadType::class, null, array(
            'action' => $this->generateUrl('ls_core_song_send'),
            'method' => 'POST'
        ));
        $form->add('submit', SubmitType::class, array('label' => $this->get('translator')->trans('form.label.send')));

        return $this->render('LsCoreBundle:Default:player.html.twig', array(
            'form' => $form->createView(),
            'songs' => $songs
        ));
    }

    public function songSendAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(AudioFileDownloadType::class, null, array(
            'action' => $this->generateUrl('ls_core_song_send'),
            'method' => 'POST'
        ));
        $form->add('submit', SubmitType::class, array('label' => $this->get('translator')->trans('form.label.send')));

        $form->handleRequest($request);
        if ($form->isValid()) {
            $email = $form->get('email')->getData();
            $file_id = $form->get('file_id')->getData();

            $entity = $em->getRepository('LsAudioPlayerBundle:AudioFile')->find($file_id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find AudioFile entity.');
            }

            $subject = 'Utwór "' . $entity->getTitle() . '" z portalu kindlamusic.pl';

            $message_txt = 'W załaczniku znajduje się utwór "' . $entity->getTitle() . '"';

            $message = \Swift_Message::newInstance();
            $message->setSubject($subject);
            $message->setFrom(array($this->container->getParameter('mailer_user') => $this->container->getParameter('mailer_name')));
            $message->setTo($email);
            $message->setBody($message_txt, 'text/html');
            $message->addPart(strip_tags($message_txt), 'text/plain');
            $message->attach(\Swift_Attachment::fromPath($entity->getFileAbsolutePath()));

            $mailer = $this->get('mailer');
            $mailer->send($message);
            
            $download = new AudioFileDownload();
            $download->setAudioFile($entity);
            $download->setEmail($email);
            $em->persist($download);
            $em->flush();

            return $this->render('LsCoreBundle:Default:song_success.html.twig', array());
        } else {
            return $this->render('LsAudioPlayerBundle:Front:song_form.html.twig', array(
                'form' => $form->createView(),
            ));
        }
    }

    public function contactAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $config_modules = $this->container->getParameter('ls_menu.modules');
        $currentRoute = $request->get('_route');

        if ($request->query->has('_locale')) {
            $route_name = '';
            foreach ($config_modules as $module) {
                if ($module['id'] == 'contact') {
                    $route_name = $module['route'][$request->getLocale()];
                }
            }

            return $this->redirect($this->generateUrl($route_name));
        }

        foreach ($config_modules as $module) {
            if ($module['id'] == 'contact') {
                foreach ($module['route'] as $key => $route) {
                    if ($route == $currentRoute && $key != $request->getLocale()) {
                        return $this->redirect($this->generateUrl($route) . '?_locale=' . $key);
                    }
                }
            }
        }

        return $this->render('LsCoreBundle:Default:contact.html.twig', array());
    }

    public function adminAction() {
        $blocks = $this->container->getParameter('ls_core.admin.dashboard');
        $dashboard = new AdminDashboard();

        foreach ($blocks as $block) {
            $parent = new AdminBlock($block['label']);
            $dashboard->addBlock($parent);
            foreach ($block['items'] as $item) {
                $service = $this->container->get($item);
                $service->addToDashboard($parent);
            }
        }

        return $this->render('LsCoreBundle:Default:admin.html.twig', array(
            'dashboard' => $dashboard
        ));
    }
    
    public function bambiniIndexAction() {
        $em = $this->getDoctrine()->getManager();
        
        $categories = $em->createQueryBuilder()
            ->select('e')
            ->from('LsBambiniColoringBundle:BambiniColoring', 'e')
            ->orderBy('e.title', 'asc')
            ->getQuery()
            ->getResult();
        
        return $this->render('LsCoreBundle:Default:bambini.html.twig', [
            'categories' => $categories
        ]);
    }
}
