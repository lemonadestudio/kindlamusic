<?php

namespace Ls\CoreBundle\Controller;


use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class KCFinderController extends Controller
{
    public function browseAction()
    {
        $_SESSION['KCFINDER'] = array();
        $_SESSION['KCFINDER']['disabled'] = false;


        $getParameters = $this->get('request')->query->all();


        return new RedirectResponse(
            $this->get('request')->getBasePath() . 
            '/bundles/lscore/kcfinder/browse.php?' . 
            http_build_query($getParameters));
    }
}
