<?php

namespace Ls\CoreBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class GoogleRecaptcha extends Constraint {
    public $message = 'Należy wypełnić ochronę antyspamową.';

    public function validatedBy() {
        return GoogleRecaptchaValidator::class;
    }
}