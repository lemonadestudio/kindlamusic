<?php

namespace Ls\CoreBundle\Form;

use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GoogleRecaptchaType extends AbstractType {
    private $siteKey;

    public function __construct() {
        $this->siteKey = "6LepZ1ImAAAAAPdDsTYWeAhmHCve1t9b4huPQnNU";
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'site_key' => $this->siteKey,
        ));
    }

    public function buildView(FormView $view, FormInterface $form, array $options) {
        $view->vars['site_key'] = $options['site_key'];
    }

    public function getParent() {
        return FormType::class;
    }

    public function getBlockPrefix() {
        return 'google_recaptcha';
    }
}
