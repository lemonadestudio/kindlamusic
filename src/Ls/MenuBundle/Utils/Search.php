<?php

namespace Ls\MenuBundle\Utils;

use Symfony\Component\DependencyInjection\ContainerInterface;

class Search {

    private $container;
    private $method;

    public function __construct(ContainerInterface $container, $method = '') {
        $this->container = $container;
        $this->method = $method;
    }

    public function search() {
        switch ($this->method) {
            case 'podstrona' :
                return $this->podstrona();
			case 'events_podstrona' :
                return $this->events_podstrona();
            default:
                break;
        }

        return array();
    }

    public function podstrona() {

        $em = $this->container->get('doctrine')->getManager();

        $items = $em->createQueryBuilder()
                ->select('s')
                ->from('LsPageBundle:Page', 's')
                ->getQuery()
                ->getResult();

        $ret_arr = array();
        foreach ($items as $item) {
            $ret_arr[] = array(
                'id' => $item->getId(),
                'title' => $item->getTranslation('pl')->getTitle()
            );
        }

        return $ret_arr;
    }
	
	public function events_podstrona() {

        $em = $this->container->get('doctrine')->getManager();

        $items = $em->createQueryBuilder()
                ->select('s')
                ->from('LsEventsPageBundle:EventsPage', 's')
                ->getQuery()
                ->getResult();

        $ret_arr = array();
        foreach ($items as $item) {
            $ret_arr[] = array(
                'id' => $item->getId(),
                'title' => $item->getTitle()
            );
        }

        return $ret_arr;
    }
}
