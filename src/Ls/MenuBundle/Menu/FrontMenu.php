<?php

namespace Ls\MenuBundle\Menu;

use Knp\Menu\FactoryInterface;
use Ls\MenuBundle\Entity\MenuItem;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class FrontMenu implements ContainerAwareInterface {

    use ContainerAwareTrait;

    public function managedMenu(FactoryInterface $factory, array $options) {
        $router = $this->container->get('router');
        $request = $this->container->get('request');

        $menu = $factory->createItem('root');
        $currentRoute = $request->get('_route');

        $em = $this->container->get('doctrine')->getManager();
        $menu_items = $em->createQueryBuilder()
            ->select('m', 't')
            ->from('LsMenuBundle:MenuItem', 'm')
            ->leftJoin('m.translations', 't')
            ->where('m.location = :location')
            ->andWhere('m.parent is NULL')
            ->orderBy('m.arrangement', 'ASC')
            ->setParameter('location', $options['location'])
            ->getQuery()
            ->getResult();

        foreach ($menu_items as $item) {
            $child = $this->buildMenu($item, $menu);
        }

        return $menu;
    }

    /**
     *
     * @param MenuItem $item
     *
     * @return bool
     */
    public function checkItem($item) {
        $item_arr = $this->getMenuItemArray($item);

        if (!$item_arr) {
            return false;
        }

        return true;
    }

    public function buildMenu($item, $menu) {
        $item_arr = $this->getMenuItemArray($item);

        if (false === $item_arr) {
            return null;
        }

        $option = array();
        if (isset($item_arr['uri'])) {
            $option['uri'] = $item_arr['uri'];
        }
        if (isset($item_arr['route'])) {
            $option['route'] = $item_arr['route'];
        }
        if (isset($item_arr['routeParameters'])) {
            $option['routeParameters'] = $item_arr['routeParameters'];
        }
        $option['linkAttributes'] = array_merge(array('title' => $item_arr['title']), $item_arr['linkAttributes']);
        if (count($item_arr['itemAttributes']) > 0) {
            $option['attributes'] = $item_arr['itemAttributes'];
        }

        return $menu->addChild($item_arr['title'], $option);
    }

    public function getMenuItemArray($item) {
        $router = $this->container->get('router');
        $request = $this->container->get('request');
        $em = $this->container->get('doctrine')->getManager();

        $result = array(
            'title' => $item->getTranslation($request->getLocale())->getTitle(),
            'linkAttributes' => array(),
            'itemAttributes' => array(),
        );

        if (!$item->getUrl() && !$item->getRoute() && !$item->getOnclick()) {
            return false;
        }

        switch ($item->getType()) {
            case 'url':
                $result['uri'] = $item->getUrl();
                if ($item instanceof MenuItem) {
                    if (count($item->getChildren()) > 0) {
                        $result['linkAttributes']['class'] = 'has-submenu';
                        $result['itemAttributes']['class'] = 'has-submenu';
                    }
                    if ($item->getBlank()) {
                        $result['linkAttributes']['target'] = '_blank';
                    }
                }
                break;

            case 'route':
                if ($item->getRoute()) {
                    $config_modules = $this->container->getParameter('ls_menu.modules');
                    $route_name = '';
                    foreach ($config_modules as $module) {
                        if ($module['id'] == $item->getRoute()) {
                            $route_name = $module['route'][$request->getLocale()];
                        }
                    }
                    $result['route'] = $route_name;
                    $route = $router->getRouteCollection()->get($route_name);
                    if ($route) {
                        $parameters = array();
                        switch ($route_name) {
                            case 'ls_page_show':
                                $object = $em->createQueryBuilder()
                                    ->select('e', 't')
                                    ->from('LsPageBundle:Page', 'e')
                                    ->leftJoin('e.translations', 't')
                                    ->where('e.id = :object')
                                    ->setParameter(':object', $item->getObject())
                                    ->getQuery()
                                    ->getOneOrNullResult();

                                if (null !== $object) {
                                    $parameters['slug'] = $object->getTranslation($request->getLocale())->getSlug();
                                }
                                if (count($parameters) > 0) {
                                    $result['routeParameters'] = $parameters;
                                }
                                break;
								
							case 'ls_events_page_show':
                                $object = $em->createQueryBuilder()
                                    ->select('e')
                                    ->from('LsEventsPageBundle:EventsPage', 'e')
                                    ->where('e.id = :object')
                                    ->setParameter(':object', $item->getObject())
                                    ->getQuery()
                                    ->getOneOrNullResult();

                                if (null !== $object) {
                                    $parameters['slug'] = $object->getSlug();
                                }
                                if (count($parameters) > 0) {
                                    $result['routeParameters'] = $parameters;
                                }
                                break;

                            default:
                                break;
                        }
                    }
                }
                $class = array();
                if ($item instanceof MenuItem) {
                    if (count($item->getChildren()) > 0) {
                        $class[] = 'has-submenu';
                    }
                    if ($item->getRoute() == 'ls_core_homepage') {
                        $class[] = 'home';
                    }
                }
                if (count($class) > 0) {
                    $result['itemAttributes']['class'] = implode(' ', $class);
                }
                break;

            case 'button':
                $result['uri'] = '#';
                if ($item instanceof MenuItem) {
                    if (count($item->getChildren()) > 0) {
                        $result['linkAttributes']['class'] = 'has-submenu';
                        $result['itemAttributes']['class'] = 'has-submenu';
                    }
                }
                $result['linkAttributes']['onclick'] = 'return ' . $item->getOnClick() . ';';
                break;

            default:
                break;
        }

        return $result;
    }

}
