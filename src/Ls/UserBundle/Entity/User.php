<?php

namespace Ls\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Ls\AudioPlayerBundle\Entity\AudioFileCategory;

/**
 * @ORM\Entity(repositoryClass="Ls\UserBundle\Entity\UserRepository")
 * @ORM\Table(name="user")
 */
class User implements AdvancedUserInterface, \Serializable {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @var string
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @var string
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $salt;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $plain_password;

    /**
     * @ORM\Column(type="boolean")
     * @var boolean
     */
    private $active;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    private $last_login;

    /**
     * @ORM\Column(type="json_array")
     * @var string
     */
    private $roles;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    private $updated_at;
    
    /**
     * @ORM\ManyToMany(targetEntity="\Ls\AudioPlayerBundle\Entity\AudioFileCategory", mappedBy="users")
     * @ORM\JoinTable(name="categories_users",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="category_id", referencedColumnName="id")}
     *      )
     */
    private $categories;

    /**
     * Constructor
     */
    public function __construct() {
        $this->active = false;
        $this->salt = md5(uniqid(null, true));
        $this->created_at = new \DateTime();
        $this->categories = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer $id
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return User
     */
    public function setUsername($username) {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername() {
        return $this->username;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email) {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * Set salt
     *
     * @param string $salt
     * @return User
     */
    public function setSalt($salt) {
        $this->salt = $salt;

        return $this;
    }

    /**
     * Get salt
     *
     * @return string
     */
    public function getSalt() {
        return $this->salt;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password) {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword() {
        return $this->password;
    }

    /**
     * Set plain_password
     *
     * @param string $plain_password
     * @return User
     */
    public function setPlainPassword($plain_password) {
        $this->plain_password = $plain_password;

        return $this;
    }

    /**
     * Get plain_password
     *
     * @return string
     */
    public function getPlainPassword() {
        return $this->plain_password;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return User
     */
    public function setActive($active) {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive() {
        return $this->active;
    }

    /**
     * Set last_login
     *
     * @param \DateTime $last_login
     * @return User
     */
    public function setLastLogin($last_login) {
        $this->last_login = $last_login;

        return $this;
    }

    /**
     * Get last_login
     *
     * @return \DateTime
     */
    public function getLastLogin() {
        return $this->last_login;
    }

    /**
     * Set roles
     *
     * @param array $roles
     * @return User
     */
    public function setRoles($roles) {
        $this->roles = $roles;

        return $this;
    }

    /**
     * Get roles
     *
     * @return array
     */
    public function getRoles() {
        return $this->roles;
    }

    /**
     * Get roles
     *
     * @return array
     */
    static public function getRolesOptions() {
        $options = array(
            'Użytkownik' => 'ROLE_USER',
            'Administrator' => 'ROLE_ADMIN',
            'Redaktor Bambini' => 'ROLE_ADMIN_BAMBINI',
            'Umożliwia podszywanie się pod innego użytkownika' => 'ROLE_ALLOWED_TO_SWITCH',
        );
        return $options;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $created_at
     * @return User
     */
    public function setCreatedAt($created_at) {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime
     */
    public function getCreatedAt() {
        return $this->created_at;
    }

    /**
     * Set updated_at
     *
     * @param \DateTime $updated_at
     * @return User
     */
    public function setUpdatedAt($updated_at) {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime
     */
    public function getUpdatedAt() {
        return $this->updated_at;
    }
    
    /**
     * Add category
     *
     * @param \Ls\AudioPlayerBundle\Entity\AudioFileCategory $category
     */
    public function addCategory(AudioFileCategory $category) {
        if (!$this->categories->contains($category)) {
            $this->categories->add($category);
        }
            
        return $this;
    }
    
    /**
     * Remove category
     *
     * @param \Ls\AudioPlayerBundle\Entity\AudioFileCategory $category
     */
    public function removeCategory(AudioFileCategory $category) {
        $this->categories->removeElement($category);
    }

    /**
     * Get categories
     *
     * @return \Ls\AudioPlayerBundle\Entity\AudioFileCategory
     */
    public function getCategories() {
        return $this->categories;
    }

    public function eraseCredentials() {
    }

    public function isAccountNonExpired() {
        return true;
    }

    public function isAccountNonLocked() {
        return true;
    }

    public function isCredentialsNonExpired() {
        return true;
    }

    public function isEnabled() {
        return $this->active;
    }

    /** @see \Serializable::serialize() */
    public function serialize() {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
            $this->salt,
            $this->active
        ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized) {
        list (
            $this->id,
            $this->username,
            $this->password,
            $this->salt,
            $this->active
            ) = unserialize($serialized);
    }

    public function __toString() {
        if (is_null($this->getUsername())) {
            return 'NULL';
        }
        return $this->getUsername();
    }
}
