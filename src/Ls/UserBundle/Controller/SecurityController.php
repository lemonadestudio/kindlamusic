<?php

namespace Ls\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SecurityController extends Controller {
    public function loginAction(Request $request) {
        if ($request->query->has('_locale')) {
            return $this->redirect($this->generateUrl('ls_user_login'));
        }

        $authenticationUtils = $this->get('security.authentication_utils');

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('LsUserBundle:Security:login.html.twig', array(
            'last_username' => $lastUsername,
            'error' => $error,
        ));
    }

    public function loginCheckAction() {
        return new Response('OK');
    }

    public function logoutAction() {
        return new Response('OK');
    }

    public function loginAdminAction(Request $request) {
        $authenticationUtils = $this->get('security.authentication_utils');

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('LsUserBundle:Security:loginAdmin.html.twig', array(
            'last_username' => $lastUsername,
            'error' => $error,
        ));
    }

    public function loginAdminCheckAction() {
        return new Response('OK');
    }

    public function logoutAdminAction() {
        return new Response('OK');
    }
    
    public function loginRedirectAction(Request $request) {
        return $this->redirectToRoute('ls_user_login', array(), 301);
    }
}
