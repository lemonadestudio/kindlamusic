-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: lemonade.nazwa.pl:3306
-- Czas generowania: 15 Mar 2018, 11:12
-- Wersja serwera: 10.1.30-MariaDB
-- Wersja PHP: 5.5.9-1ubuntu4.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `lemonade_65`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `audio_file`
--

CREATE TABLE `audio_file` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `filename` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `arrangement` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `audio_file`
--

INSERT INTO `audio_file` (`id`, `title`, `filename`, `arrangement`, `category_id`) VALUES
(33, 'BEAR - SPOT - NEWS', 'bear-spot- news.mp3', 1, 2),
(34, 'BEAR - SPOT 2 - NEWS', 'bear-spot2_news.mp3', 2, 2),
(35, 'BEAR - SPOT 4 - SWIETA', 'bear-spot4_swieta.mp3', 3, 2),
(36, 'BEAR - SPOT 3 - NEWS', 'bear-spot3_news.mp3', 4, 2),
(37, 'BEAR - SPOT 1 - NEWS', 'bear-spot1_news.mp3', 5, 2),
(38, 'LOVE ISLAND , motivation, good mood, enegetic ballad pop song Come together - 100bpm demo INSTR', 'love island , motivation, good mood, enegetic ballad pop song come together - 100bpm demo instr (1).mp3', 6, 2),
(39, 'ON THE GUITAR BEAT Background dynamic guitar, energetic, beat, intro music, motivation Widze_To Instrumental', 'on the guitar beat background dynamic guitar, energetic, beat, intro music, motivation widze_to instrumental .mp3', 7, 2),
(40, 'HEART OF HISTORY History, document , background, guitar, rythm, simple LoveYou Project', 'heart of history history, document , background, guitar, rythm, simple loveyou project.mp3', 8, 2),
(41, 'SWING RETRO Retro song, positive, couple, split, guitar, swing Between The Lines', 'swing retro retro song, positive, couple, split, guitar, swing between the lines.mp3', 9, 2),
(42, 'TEARS ROMANCE Romantic, love , ballad, guitar, melody, song, positive, emotional - JD - One Day', 'tears romance romantic, love , ballad, guitar, melody, song, positive, emotional - jd - one day.mp3', 10, 2),
(43, 'TRAVEL INTERLUDE Intro music, travel, motivation, positive, nice, energetic, guitar, background documentary - LiN v3 Project', 'travel interlude intro music, travel, motivation, positive, nice, energetic, guitar, background documentary - lin v3 project.mp3', 11, 2),
(44, 'WALK SIMPLE Documentary, travel, bells background, electronic, organ, beat, electronic rythm, bass mood, calym, sercem', 'walk simple documentary, travel, bells background, electronic, organ, beat, electronic rythm, bass mood, calym, sercem.mp3', 12, 2),
(45, 'VINTAGE 1 , vintage , sport, cars,80s 90s race fast electronic energetic bass Drive 101 BPM', 'vintage 1 , vintage , sport, cars,80s 90s race fast electronic energetic bass drive 101 bpm .mp3', 13, 2),
(46, 'COSMIC PUNCH Documentary, energetic, hard, motion, glitch, screamy, electronic Dub lajt 150BPM', 'cosmic punch documentary, energetic, hard, motion, glitch, screamy, electronic dub lajt 150bpm .mp3', 14, 2),
(47, 'RAGGA DIGITAL Simple regge, funny, electronic, electroregge, positive BrosSBresSa1.', 'ragga digital simple regge, funny, electronic, electroregge, positive brossbressa1..mp3', 15, 2),
(48, 'CITY LIGHTS background report, documentary, grime, hard, background , sharp electronic  - grime 07 11 106bpm', 'city lights background report, documentary, grime, hard, background , sharp electronic  - grime 07 11 106bpm.mp3', 16, 2),
(49, 'SIMPLE LIFE Background, document, history, nature, airy, smooth, electronic, mood, pads, slow, Wavy 142 Bpm.', 'simple life background, document, history, nature, airy, smooth, electronic, mood, pads, slow, wavy 142 bpm..mp3', 17, 2),
(50, 'CALL COPS Crime documentary, nervous, police, beat, dark piano Tafla 92', 'call cops crime documentary, nervous, police, beat, dark piano tafla 92.mp3', 18, 2),
(51, 'UP IN THE SKY  Background document motivation nostalgia pop piano sad Project', 'up in the sky  background document motivation nostalgia pop piano sad project.mp3', 19, 2),
(52, 'FLASH ONE Dynamic background sport social news electronic pump spiegel YS2015 1 ProjectLoop-srodek 128bpm', 'flash one dynamic background sport social news electronic pump spiegel ys2015 1 projectloop-srodek 128bpm.mp3', 20, 2),
(53, 'Easy Evening - Fashion,Trendy,Club,Modern,Sensation,uptempo', 'easy evening.mp3', 21, 2),
(54, 'Nevada Drive - Travel,Drive,Happy,Electronic,Beat', 'nevada drive.mp3', 22, 2),
(55, 'Long Way - Travel,News,Discover,Landscape', 'long way through ands.mp3', 23, 2),
(56, 'Danger Zone - criminal, action,beat,dangerous', 'danger zone.mp3', 24, 2),
(57, 'Criminal Mood - Action,Criminal,Drama', 'criminal mood.mp3', 25, 2),
(58, 'HorrorStrange92 - Background ,scratch, matrix, industrial, hardcore, synth, electronic, sampled ANTY Project', 'horrorstrange92 - background ,scratch, matrix, industrial, hardcore, synth, electronic, sampled anty project.mp3', 26, 2),
(59, 'PopRythm110 - simple, plastic, pop, synth, slow soul bez efektow pop110 bpm', 'poprythm110 - simple, plastic, pop, synth, slow soul bez efektow pop110 bpm.mp3', 27, 2),
(60, 'Salutmarch98 - Background march, loop, army, simple, piano, report, alert, watch out marszowy 6512 98bpm', 'salutmarch98 - background march, loop, army, simple, piano, report, alert, watch out marszowy 6512 98bpm.mp3', 28, 2),
(61, 'Electro80s100 - Electro intro, alarm, positive, jean michelle jarre, uptempo, noisy, drums, 80s, 90s elektro 2126 demo v', 'electro80s100 - electro intro, alarm, positive, jean michelle jarre, uptempo, noisy, drums, 80s, 90s elektro 2126 demo v.mp3', 29, 2),
(62, 'Popyeah122 - Pop electro energetic intro music opening synth bass lekki db 16  Project 122bpm', 'popyeah122 - pop electro energetic intro music opening synth bass lekki db 16  project 122bpm.mp3', 30, 2),
(63, 'FizzyDub140 - Intro theme agressive dynamic future muscle poer energetic dubstep db rap 140', 'fizzydub140 - intro theme agressive dynamic future muscle poer energetic dubstep db rap 140.mp3', 31, 2),
(64, 'SimpleDisco128 - Disco dance dynamic moto club uptempo DP2 128 bpm Project', 'simpledisco128 - disco dance dynamic moto club uptempo dp2 128 bpm project.mp3', 32, 2),
(65, 'TropicDon105 - Tropical house, sand, sky, summer, couple, light, sea Donmato105 bpm Project', 'tropicdon105 - tropical house, sand, sky, summer, couple, light, sea donmato105 bpm project.mp3', 33, 2),
(66, 'Furious160 - Fast, cars, dynamic, dnb, synth DrumnBass Disco Rmx-Zoska 160 BPM Instr', 'furious160 - fast, cars, dynamic, dnb, synth drumnbass disco rmx-zoska 160 bpm instr.mp3', 34, 2),
(67, 'PodarujSobieSwieta112 - Christmas, kids, kidssong, Swieta, nastroj , bells, Podaruj sobie svieta Master 112bpm', 'podarujsobieswieta112 - christmas, kids, kidssong, swieta, nastroj , bells, podaruj sobie svieta master 112bpm.mp3', 35, 2),
(68, 'TravelDance126 - Dance, fast, electronic, dynamic, cars, city, positive 126 bpm Grill Vhit', 'traveldance126 - dance, fast, electronic, dynamic, cars, city, positive 126 bpm grill vhit.mp3', 36, 2),
(69, 'RetroLin130 - Retro band, dynamic, report, document , 60s 70s -  LiN v4 130 Project', 'retrolin130 - retro band, dynamic, report, document , 60s 70s -  lin v4 130 project.mp3', 37, 2),
(70, 'PodarujSobieSwietaInstr112 - Christmas, kids, kidssong, Swieta, nastroj , bells, Podaruj sobie svieta instrumental 112bpm', 'podarujsobieswietainstr112 - christmas, kids, kidssong, swieta, nastroj , bells, podaruj sobie svieta instrumental 112bpm.mp3', 38, 2),
(71, 'LoungePop110 - Background, melancholic, positive, lounge, uptempo, report, mood, electronic, piano LiN v2', 'loungepop110 - background, melancholic, positive, lounge, uptempo, report, mood, electronic, piano lin v2.mp3', 39, 2),
(72, 'PartyShake128 - Dance, club, hard, simple, banger, intro, opening Prima sort remix 128 bpm', 'partyshake128 - dance, club, hard, simple, banger, intro, opening prima sort remix 128 bpm.mp3', 40, 2),
(73, 'LoveGreg101 - Ballad music, nostalgia, guitar poem, report, mood, slo, pop GREG 1 101', 'lovegreg101 - ballad music, nostalgia, guitar poem, report, mood, slo, pop greg 1 101.mp3', 41, 2),
(74, 'Beat Up - Drama,Action,Hip Hop,Criminal', 'beat_up_tempo.mp3', 42, 2),
(75, 'Heaven - Action,Drama,Guitar,Motivation,UpTempo', 'beat_heaven.mp3', 43, 2);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `audio_file_category`
--

CREATE TABLE `audio_file_category` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `arrangement` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `audio_file_category`
--

INSERT INTO `audio_file_category` (`id`, `title`, `arrangement`) VALUES
(1, 'Piosenki:', 1),
(2, 'Muzyka ilustracyjna, oprawa, czołówki:', 2);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `audio_file_download`
--

CREATE TABLE `audio_file_download` (
  `id` int(11) NOT NULL,
  `audio_file_id` int(11) DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `audio_file_download`
--

INSERT INTO `audio_file_download` (`id`, `audio_file_id`, `email`, `created_at`) VALUES
(1, NULL, 'marcin@lemonadestudio.pl', '2016-05-24 16:20:13'),
(2, NULL, 'makindla@interia.pl', '2016-05-24 16:23:32'),
(3, NULL, 'wojtekbyrski@wp.pl', '2016-05-29 18:03:10'),
(4, NULL, 'jakub@hqt.pl', '2016-05-31 16:31:31'),
(5, NULL, 'kamilakrzempek@onet.pl', '2016-06-07 18:46:31'),
(6, NULL, 'makindla@interia.pl', '2016-06-10 13:09:56'),
(7, NULL, 'antoni-smykiewicz@wp.pl', '2016-07-13 12:46:53'),
(8, NULL, 'antoni-smykiewicz@wp.pl', '2016-07-13 12:49:22'),
(9, NULL, 'jakub@hqt.pl', '2016-09-08 16:32:13'),
(10, NULL, 'jakub@hqt.pl', '2016-09-08 16:32:42'),
(11, NULL, 'antoni-smykiewicz@wp.pl', '2016-09-10 11:55:47'),
(12, NULL, 'antoni-smykiewicz@wp.pl', '2016-09-10 11:56:07'),
(13, NULL, 'makindla@interia.pl', '2016-09-10 11:57:08'),
(14, NULL, 'makindla@interia.pl', '2016-09-11 16:41:09'),
(15, NULL, 'makindla@interia.pl', '2016-10-29 14:42:41'),
(16, NULL, 'makindla@interia.pl', '2016-12-01 12:22:03'),
(17, NULL, 'makindla@interia.pl', '2016-12-01 12:26:34'),
(18, NULL, 'makindla@interia.pl', '2017-02-18 11:13:39'),
(19, NULL, 'makindla@interia.pl', '2017-02-18 11:15:25'),
(20, NULL, 'makindla@interia.pl', '2017-02-26 12:43:21'),
(21, NULL, 'oleh@lemonadestudio.pl', '2017-06-27 11:37:18'),
(22, NULL, 'oleh@lemonadestudio.pl', '2017-06-27 11:37:50'),
(23, NULL, 'oleh@lemonadestudio.pl', '2017-06-27 11:45:10'),
(24, NULL, 'oleh@lemonadestudio.pl', '2017-06-27 11:45:45'),
(25, NULL, 'oleh@lemonadestudio.pl', '2017-06-27 11:58:30'),
(26, NULL, 'oleh@lemonadestudio.pl', '2017-06-27 11:59:06'),
(27, NULL, 'makindla@interia.pl', '2017-09-03 17:50:51'),
(28, NULL, 'makindla@interia.pl', '2017-09-03 17:53:13'),
(29, NULL, 'bzdanowicz@wp.pl', '2017-09-03 18:03:11'),
(30, NULL, 'makindla@interia.pl', '2017-09-03 18:03:42'),
(31, 40, 'piotr.lisiak@takomedia.pl', '2018-01-17 22:26:30'),
(32, 34, 'igor.myroshnychenko@takomedia.pl', '2018-01-17 22:46:33'),
(33, 35, 'oneigor@gmail.com', '2018-01-17 22:48:21'),
(34, 33, 'oneigor@gmail.com', '2018-01-17 22:55:00'),
(35, 34, 'oneigor@gmail.com', '2018-01-17 22:55:16'),
(36, 36, 'oneigor@gmail.com', '2018-01-17 22:55:26'),
(37, 37, 'oneigor@gmail.com', '2018-01-17 22:55:36'),
(38, 38, 'oneigor@gmail.com', '2018-01-17 22:55:44'),
(39, 39, 'oneigor@gmail.com', '2018-01-17 22:55:50'),
(40, 40, 'oneigor@gmail.com', '2018-01-17 22:55:59'),
(41, 41, 'oneigor@gmail.com', '2018-01-17 22:56:09'),
(42, 42, 'oneigor@gmail.com', '2018-01-17 22:56:15'),
(43, 43, 'oneigor@gmail.com', '2018-01-17 22:56:22'),
(44, 44, 'oneigor@gmail.com', '2018-01-17 22:56:31'),
(45, 45, 'oneigor@gmail.com', '2018-01-17 22:56:40'),
(46, 46, 'oneigor@gmail.com', '2018-01-17 22:56:47'),
(47, 47, 'oneigor@gmail.com', '2018-01-17 22:56:58'),
(48, 48, 'oneigor@gmail.com', '2018-01-17 22:57:07'),
(49, 49, 'oneigor@gmail.com', '2018-01-17 22:57:13'),
(50, 50, 'oneigor@gmail.com', '2018-01-17 22:57:23'),
(51, 51, 'oneigor@gmail.com', '2018-01-17 22:57:33'),
(52, 52, 'oneigor@gmail.com', '2018-01-17 22:57:46'),
(53, 53, 'oneigor@gmail.com', '2018-01-17 22:57:56'),
(54, 54, 'oneigor@gmail.com', '2018-01-17 22:58:09'),
(55, 55, 'oneigor@gmail.com', '2018-01-17 22:58:21'),
(56, 56, 'oneigor@gmail.com', '2018-01-17 22:58:31'),
(57, 57, 'oneigor@gmail.com', '2018-01-17 22:58:38'),
(58, 58, 'oneigor@gmail.com', '2018-01-17 22:58:50'),
(59, 58, 'oneigor@gmail.com', '2018-01-17 22:59:01'),
(60, 59, 'oneigor@gmail.com', '2018-01-17 22:59:10'),
(61, 60, 'oneigor@gmail.com', '2018-01-17 22:59:20'),
(62, 61, 'oneigor@gmail.com', '2018-01-17 22:59:33'),
(63, 62, 'oneigor@gmail.com', '2018-01-17 22:59:44'),
(64, 63, 'oneigor@gmail.com', '2018-01-17 22:59:52'),
(65, 64, 'oneigor@gmail.com', '2018-01-17 23:00:04'),
(66, 65, 'oneigor@gmail.com', '2018-01-17 23:00:20'),
(67, 66, 'oneigor@gmail.com', '2018-01-17 23:00:30'),
(68, 67, 'oneigor@gmail.com', '2018-01-17 23:00:40'),
(69, 68, 'oneigor@gmail.com', '2018-01-17 23:00:48'),
(70, 72, 'oneigor@gmail.com', '2018-01-17 23:00:58'),
(71, 73, 'oneigor@gmail.com', '2018-01-17 23:01:06'),
(72, 74, 'oneigor@gmail.com', '2018-01-17 23:01:15'),
(73, 75, 'oneigor@gmail.com', '2018-01-17 23:01:29'),
(74, 38, 'piotr.lisiak@takomedia.pl', '2018-01-18 11:27:24'),
(75, 39, 'piotr.lisiak@takomedia.pl', '2018-01-18 11:27:55'),
(76, 40, 'piotr.lisiak@takomedia.pl', '2018-01-18 11:28:37'),
(77, 40, 'piotr.lisiak@takomedia.pl', '2018-01-18 11:28:38'),
(78, 33, 'pazdder@wp.pl', '2018-01-18 13:38:52'),
(79, 34, 'pazdder@wp.pl', '2018-01-18 13:39:32'),
(80, 35, 'pazdder@wp.pl', '2018-01-18 13:39:56'),
(81, 36, 'pazdder@wp.pl', '2018-01-18 13:42:42'),
(82, 37, 'pazdder@wp.pl', '2018-01-18 13:43:02'),
(83, 38, 'pazdder@wp.pl', '2018-01-18 13:43:27'),
(84, 39, 'pazdder@wp.pl', '2018-01-18 13:45:13'),
(85, 40, 'pazdder@wp.pl', '2018-01-18 13:45:30'),
(86, 33, 'pazdder@wp.pl', '2018-01-18 13:45:46'),
(87, 41, 'pazdder@wp.pl', '2018-01-18 13:47:08'),
(88, 42, 'pazdder@wp.pl', '2018-01-18 13:47:24'),
(89, 43, 'pazdder@wp.pl', '2018-01-18 13:47:47'),
(90, 44, 'pazdder@wp.pl', '2018-01-18 13:48:13'),
(91, 45, 'pazdder@wp.pl', '2018-01-18 13:48:34'),
(92, 46, 'pazdder@wp.pl', '2018-01-18 13:49:04'),
(93, 47, 'pazdder@wp.pl', '2018-01-18 13:50:04'),
(94, 48, 'pazdder@wp.pl', '2018-01-18 14:01:20'),
(95, 49, 'pazdder@wp.pl', '2018-01-18 14:08:36'),
(96, 50, 'pazdder@wp.pl', '2018-01-18 14:08:49'),
(97, 51, 'pazdder@wp.pl', '2018-01-18 14:09:08'),
(98, 51, 'pazdder@wp.pl', '2018-01-18 14:09:34'),
(99, 52, 'pazdder@wp.pl', '2018-01-18 14:13:29'),
(100, 52, 'pazdder@wp.pl', '2018-01-18 14:13:43'),
(101, 53, 'pazdder@wp.pl', '2018-01-18 14:14:17'),
(102, 54, 'pazdder@wp.pl', '2018-01-18 14:14:30'),
(103, 55, 'pazdder@wp.pl', '2018-01-18 14:17:18'),
(104, 56, 'pazdder@wp.pl', '2018-01-18 14:17:36'),
(105, 57, 'pazdder@wp.pl', '2018-01-18 14:17:51'),
(106, 58, 'pazdder@wp.pl', '2018-01-18 14:18:05'),
(107, 59, 'pazdder@wp.pl', '2018-01-18 14:18:21'),
(108, 60, 'pazdder@wp.pl', '2018-01-18 14:18:38'),
(109, 61, 'pazdder@wp.pl', '2018-01-18 14:18:55'),
(110, 62, 'pazdder@wp.pl', '2018-01-18 14:19:11'),
(111, 63, 'pazdder@wp.pl', '2018-01-18 14:19:22'),
(112, 64, 'pazdder@wp.pl', '2018-01-18 14:19:43'),
(113, 65, 'pazdder@wp.pl', '2018-01-18 14:26:12'),
(114, 66, 'pazdder@wp.pl', '2018-01-18 14:26:27'),
(115, 67, 'pazdder@wp.pl', '2018-01-18 14:26:43'),
(116, 68, 'pazdder@wp.pl', '2018-01-18 14:26:59'),
(117, 72, 'pazdder@wp.pl', '2018-01-18 14:27:17'),
(118, 73, 'pazdder@wp.pl', '2018-01-18 14:27:28'),
(119, 73, 'pazdder@wp.pl', '2018-01-18 14:27:55'),
(120, 74, 'pazdder@wp.pl', '2018-01-18 14:28:08'),
(121, 75, 'pazdder@wp.pl', '2018-01-18 14:28:59'),
(122, 38, 'maciejwojcikmango@gmail.com', '2018-01-18 20:06:26'),
(123, 66, 'maciejwojcikmango@gmail.com', '2018-01-18 20:11:44'),
(124, 58, 'maciejwojcikmango@gmail.com', '2018-01-18 20:13:03'),
(125, 57, 'maciejwojcikmango@gmail.com', '2018-01-18 20:13:50'),
(126, 48, 'maciejwojcikmango@gmail.com', '2018-01-18 20:14:40'),
(127, 61, 'maciejwojcikmango@gmail.com', '2018-01-18 20:15:46'),
(128, 45, 'maciejwojcikmango@gmail.com', '2018-01-18 20:16:56'),
(129, 56, 'maciejwojcikmango@gmail.com', '2018-01-18 20:17:56'),
(130, 51, 'maciejwojcikmango@gmail.com', '2018-01-18 20:19:07'),
(131, 53, 'maciejwojcikmango@gmail.com', '2018-01-18 20:19:35'),
(132, 54, 'maciejwojcikmango@gmail.com', '2018-01-18 20:25:55'),
(133, 69, 'pazdder@wp.pl', '2018-01-20 16:27:32'),
(134, 73, 'pazdder@wp.pl', '2018-01-20 16:27:46'),
(135, 75, 'pazdder@wp.pl', '2018-01-20 16:28:01'),
(136, 69, 'maciejwojcikmango@gmail.com', '2018-01-29 20:27:01'),
(137, 60, 'maciejwojcikmango@gmail.com', '2018-01-29 20:27:54'),
(138, 72, 'maciejwojcikmango@gmail.com', '2018-01-29 20:29:14'),
(139, 68, 'maciejwojcikmango@gmail.com', '2018-01-29 20:30:10'),
(140, 63, 'maciejwojcikmango@gmail.com', '2018-01-29 20:31:08'),
(141, 65, 'maciejwojcikmango@gmail.com', '2018-01-29 20:32:07'),
(142, 34, 'damian.szkudlarczyk@gmail.com', '2018-01-31 12:18:59'),
(143, 40, 'damian.szkudlarczyk@gmail.com', '2018-01-31 12:21:01'),
(144, 42, 'damian.szkudlarczyk@gmail.com', '2018-01-31 12:22:05'),
(145, 43, 'damian.szkudlarczyk@gmail.com', '2018-01-31 12:23:01'),
(146, 45, 'damian.szkudlarczyk@gmail.com', '2018-01-31 12:24:13'),
(147, 48, 'damian.szkudlarczyk@gmail.com', '2018-01-31 12:25:32'),
(148, 50, 'damian.szkudlarczyk@gmail.com', '2018-01-31 12:26:27'),
(149, 51, 'damian.szkudlarczyk@gmail.com', '2018-01-31 12:28:29'),
(150, 53, 'damian.szkudlarczyk@gmail.com', '2018-01-31 12:28:48'),
(151, 57, 'damian.szkudlarczyk@gmail.com', '2018-01-31 12:30:18'),
(152, 69, 'damian.szkudlarczyk@gmail.com', '2018-01-31 12:32:34'),
(153, 73, 'damian.szkudlarczyk@gmail.com', '2018-01-31 12:33:45'),
(154, 74, 'damian.szkudlarczyk@gmail.com', '2018-01-31 12:34:24'),
(155, 75, 'damian.szkudlarczyk@gmail.com', '2018-01-31 12:35:16'),
(156, 47, 'piotr.lisiak@takomedia.pl', '2018-02-02 09:49:43'),
(157, 42, 'piotr.lisiak@takomedia.pl', '2018-02-02 12:10:41'),
(158, 71, 'pazdder@wp.pl', '2018-02-03 17:45:58'),
(159, 74, 'pazdder@wp.pl', '2018-02-03 17:46:12'),
(160, 75, 'pazdder@wp.pl', '2018-02-03 17:46:26'),
(161, 71, 'piotr.lisiak@takomedia.pl', '2018-02-07 14:31:21'),
(162, 71, 'piotr.lisiak@takomedia.pl', '2018-02-07 14:31:22'),
(163, 42, 'shkanov1984@gmail.com', '2018-02-22 21:52:15');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `composer`
--

CREATE TABLE `composer` (
  `id` int(11) NOT NULL,
  `gallery_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `old_slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8_unicode_ci,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_generate` tinyint(1) DEFAULT NULL,
  `seo_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_keywords` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `published_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `gallery`
--

CREATE TABLE `gallery` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `gallery`
--

INSERT INTO `gallery` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Filmy w biografii', '2015-09-11 11:10:32', NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `gallery_movie`
--

CREATE TABLE `gallery_movie` (
  `id` int(11) NOT NULL,
  `gallery_id` int(11) DEFAULT NULL,
  `movie_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `arrangement` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `gallery_movie`
--

INSERT INTO `gallery_movie` (`id`, `gallery_id`, `movie_id`, `arrangement`) VALUES
(1, 1, 'M5LeBDAaJvo', 29),
(2, 1, '5c8geMaTbkQ', 30),
(7, 1, 'O8D2Hqq03xw', 31),
(8, 1, '8IR39Gs1f4g', 32),
(9, 1, 'GT9XvKZicow', 33),
(10, 1, 'yfRgHMKKDag', 34),
(11, 1, 'if-bol2RxgY', 35),
(12, 1, 'B-rKxxm8EsQ', 15),
(13, 1, 'tAI_L2tBEwk', 19),
(14, 1, '5sxOH2Ra8mY', 17),
(15, 1, 'VfP44Xn5Cn4', 16),
(16, 1, 'OYJ-uPTZxbA', 18),
(17, 1, 'eOrHDJiJN6Y', 22),
(18, 1, 'gvCMj71BT20', 13),
(19, 1, 'oD4bwF0vZgg', 20),
(20, 1, 'LFyHxM8YfEc', 21),
(21, 1, 'BEMgyYp2UZE', 23),
(22, 1, 'IZyWJkIyR18', 27),
(23, 1, 'o8xjBk8_98M', 28),
(24, 1, 'o0u5FmUt-iw', 25),
(25, 1, 'eUpTkav9d9E', 36),
(26, 1, '6KPNtPqsT4g', 39),
(27, 1, '7X_TI04x1Bc', 40),
(28, 1, 'rTs9nxHrdN4', 41),
(29, 1, 'K9Ac18gFDFA', 42),
(30, 1, '-aKpworaD7U', 38),
(31, 1, 'wGYA-dsgtjg', 43),
(32, 1, '4HASE_JqYe4', 44),
(33, 1, 'xAK2kCtBs6Y', 45),
(34, 1, 'VTnCHildAKE', 3),
(35, 1, 'ZNkKqGIUI0o', 12),
(38, 1, 'S2lsiTAvhkQ', 26),
(39, 1, 'mTXxmcsdbyA', 37),
(40, 1, 'f13TCEzL1LM', 7),
(41, 1, 'Cdxl_4zqxXw', 24),
(42, 1, 'Bu4y1ogAo9g', 14),
(43, 1, 'GImpgsf5XDk', 9),
(44, 1, 'V2V9kJ-DRr4', 1),
(46, 1, 'iLFPENzmw8A', 6),
(47, 1, 'UFkd6c_UUzY', 4),
(48, 1, '-zrYo0mPZ40', 2),
(49, 1, 'GpG0M27_ZpE', 8),
(50, 1, '264bh1K755I', 5),
(51, 1, 'WeAk0GALZJE', 10),
(52, 1, 'guXeucNetBg', 11);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `logos`
--

CREATE TABLE `logos` (
  `id` int(11) NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `logos`
--

INSERT INTO `logos` (`id`, `link`, `photo`) VALUES
(2, NULL, 'logos-image-595227b3c4887.png'),
(3, NULL, 'logos-image-595227dddb3a7.png');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `menu_item`
--

CREATE TABLE `menu_item` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `route` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `object` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `onclick` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `blank` tinyint(1) DEFAULT NULL,
  `arrangement` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `menu_item`
--

INSERT INTO `menu_item` (`id`, `parent_id`, `location`, `type`, `route`, `object`, `url`, `onclick`, `blank`, `arrangement`) VALUES
(1, NULL, 'menu_top', 'route', 'player', NULL, NULL, NULL, 0, 1),
(2, NULL, 'menu_top', 'route', 'page', '1', NULL, NULL, 0, 2),
(3, NULL, 'menu_top', 'route', 'contact', NULL, NULL, NULL, 0, 3);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `menu_item_translations`
--

CREATE TABLE `menu_item_translations` (
  `id` int(11) NOT NULL,
  `object_id` int(11) DEFAULT NULL,
  `locale` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `menu_item_translations`
--

INSERT INTO `menu_item_translations` (`id`, `object_id`, `locale`, `title`) VALUES
(1, 1, 'pl', 'Piosenki Demo'),
(2, 1, 'en', 'Demo songs'),
(3, 2, 'pl', 'Biografia'),
(4, 2, 'en', 'Bio'),
(5, 3, 'pl', 'Kontakt'),
(6, 3, 'en', 'Contact');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `migration_versions`
--

CREATE TABLE `migration_versions` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `migration_versions`
--

INSERT INTO `migration_versions` (`version`) VALUES
('20150903213841'),
('20150904101633'),
('20150907095907'),
('20150909132039'),
('20150909135459'),
('20150911105219'),
('20150913114051'),
('20150913114204'),
('20150913210813'),
('20160524113703');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `page`
--

CREATE TABLE `page` (
  `id` int(11) NOT NULL,
  `gallery_id` int(11) DEFAULT NULL,
  `content_short_generate` tinyint(1) DEFAULT NULL,
  `seo_generate` tinyint(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `page`
--

INSERT INTO `page` (`id`, `gallery_id`, `content_short_generate`, `seo_generate`, `created_at`, `updated_at`) VALUES
(1, 1, 0, 0, '2015-09-07 12:57:42', '2015-09-11 11:59:32');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `page_translations`
--

CREATE TABLE `page_translations` (
  `id` int(11) NOT NULL,
  `object_id` int(11) DEFAULT NULL,
  `locale` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `old_slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content_short` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8_unicode_ci,
  `seo_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_keywords` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `page_translations`
--

INSERT INTO `page_translations` (`id`, `object_id`, `locale`, `title`, `slug`, `old_slug`, `content_short`, `content`, `seo_title`, `seo_keywords`, `seo_description`) VALUES
(1, 1, 'pl', 'Biografia', 'biografia', NULL, 'Z muzyką związany od wielu lat a zawodowo od połowy lat 90tych. Początkowo głównie jako muzyk sesyjny - gitarzysta basowy brał udział w wielu projektach a od 98 roku na stałe związał się z zespołem STACHURSKY z którym wspólnie zagrali kilkaset koncertów.', '<p class=\"red\">\r\n	<strong>Marcin Kindla</strong> - kompozytor, autor tekst&oacute;w, producent muzyczny.</p>\r\n<p>\r\n	<br />\r\n	Z muzyką związany od wielu lat a zawodowo od połowy lat 90tych. Początkowo gł&oacute;wnie jako muzyk sesyjny - gitarzysta basowy brał udział w wielu projektach a od 98 roku na stałe związał się z zespołem STACHURSKY z kt&oacute;rym wsp&oacute;lnie zagrali kilkaset koncert&oacute;w.<br />\r\n	Właśnie w tamtym czasie rozpoczęła się kariera jako kompozytora i autora tekst&oacute;w, pisząc dla Stachursky utwory kt&oacute;re stały się przebojami i co nie umknęło uwadze branży muzycznej. W 2000 roku otrzymał propozycję związania się z BMG Music Publishing Poland, kt&oacute;re kilka lat p&oacute;źniej stało się UNIVERSAL MUSIC PUBLISHING MGB POLAND i ten stan trwa do dziś.</p>\r\n<p class=\"red\">\r\n	<br />\r\n	Od tamtej pory skupiony gł&oacute;wnie na rozwoju artystycznym i nabieraniu doświadczenia w komponowaniu piosenek. Lista artyst&oacute;w dla kt&oacute;rych komponował piosenki jest bardzo długa i są wśr&oacute;d nich gwiazdy polskiej sceny muzycznej.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Marcin ma na swoim koncie wiele ilustracji muzycznych do program&oacute;w telewizyjnych emitowanych na antenach TVP oraz stacji komercyjnych. W 2011 roku stworzył piosenkę promującą serial <strong>RODZINKA.PL</strong> - kt&oacute;ry stał się jednym z najpopularniejszych seriali w Polsce przyciągając przed telewizory miliony widz&oacute;w.</p>\r\n<p>\r\n	<br />\r\n	Od kilku lat jako producent muzyczny i kompozytor pracuje z topowymi polskimi artystami związanymi gł&oacute;wnie z UNIVERSAL MUSIC POLSKA i MAGIC RECORDS. Płyty tych artyst&oacute;w często uzyskiwały statu Złotej lub Platynowej a piosenki w stacjach radiowych trafiały na TOP AIRPLAY CHARTS.</p>\r\n<p>\r\n	<br />\r\n	Nie boi się nowych muzycznych wyzwań i jak sam twierdzi - &bdquo;rzeczy niemożliwe robimy od ręki, na cuda trzeba poczekać do 7 dni&rdquo; :-)</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Tworzenie piosenek to jego pasja i praca - połączenie idealne.</strong></p>', 'Biografia', 'Biografia, Maecenas, vulputate, metus, molestie, vehicula, elit, interdum, quam, Donec', 'Maecenas vulputate metus molestie, vehicula elit nec, interdum quam. Donec fringilla arcu cursus massa lobortis auctor. Vivamus hendrerit dolor vel justo posuere, in facilisis nisi vestibulum. Praesent et fermentum lorem.'),
(2, 1, 'en', 'Bio', 'bio', NULL, 'Marcin has been involved with music since the latter half of the nineties. In the early days, he worked mainly as a session bass player and took part in numerous projects. In 1998 he started working with a famous Polish pop artist called STACHURSKY, with', '<p class=\"red\">\r\n	<strong>Marcin Kindla</strong> - songwriter and producer.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Marcin has been involved with music since the latter half of the nineties. In the early days, he worked mainly as a session bass player and took part in numerous projects. In 1998 he started working with a famous Polish pop artist called STACHURSKY, with whom he played hundreds of concerts. At that time, Marcin began to compose and write lyrics, and his songs were performed by Stachursky. The songs turned out to be very successful and were quickly spotted by people working in the music business.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p class=\"red\">\r\n	In 2000, BMG Music Publishing Poland, which was soon to become Universal Music Publishing MGB Poland, made Marcin an offer to collaborate. Since then, Marcin has concentrated on developing his skills and gaining more experience in the field of music production. He has worked with a huge group of Polish artists, including many well known and famous stars.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Marcin is also known as a writer of theme tunes for TV programmes and series. His most significant and successful work was the music and lyrics he created for a comedy show called &ldquo;Rodzinka.PL&rdquo;. The series has been broadcast on Polish national TV since 2011 and is considered as the most popular Polish comedy show, watched by millions of viewers.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	For the last few years, Marcin has been collaborating with top, Polish artists, recording mainly for Universal Music Poland and Magic Records. Many of these artists&#39; albums often go &#39;gold&#39; or &#39;platinum&#39;, and songs written by Marcin jump to the top of radio airplay charts.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p class=\"red\">\r\n	Marcin enjoys taking up new challenges and his favourite motto is: &ldquo;The things which are impossible to achieve, I deal with right here and now. For a miracle &ndash; you&#39;ll have to wait &hellip;&hellip;&hellip;. I&#39;ll manage to deal with it in 7 days&rdquo; :-) Marcin is able to combine work with his biggest passion &ndash; composing songs &ndash; and he regards this as the perfect match.&nbsp;</p>', 'Bio', 'Maecenas, vulputate, metus, molestie, vehicula, elit, interdum, quam, Donec, fringilla', 'Maecenas vulputate metus molestie, vehicula elit nec, interdum quam. Donec fringilla arcu cursus massa lobortis auctor. Vivamus hendrerit dolor vel justo posuere, in facilisis nisi vestibulum. Praesent et fermentum lorem.');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `partners`
--

CREATE TABLE `partners` (
  `id` int(11) NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `partners`
--

INSERT INTO `partners` (`id`, `link`, `photo`) VALUES
(6, NULL, 'partners-image-594d6d75c5f5e.jpeg'),
(7, NULL, 'partners-image-594d6d7eab462.jpeg'),
(8, NULL, 'partners-image-594d6d838e69a.jpeg'),
(9, NULL, 'partners-image-594d6d89e29c9.jpeg'),
(10, NULL, 'partners-image-594d6d8e986d1.jpeg'),
(11, NULL, 'partners-image-594d6d99e9188.jpeg'),
(12, NULL, 'partners-image-594d6d9f06840.jpeg'),
(13, NULL, 'partners-image-594d6da3de12f.jpeg'),
(14, NULL, 'partners-image-594d6db019b79.jpeg'),
(15, NULL, 'partners-image-594d6db4aa3ea.jpeg'),
(16, NULL, 'partners-image-594d6dba1752a.jpeg'),
(17, NULL, 'partners-image-594d6dc10bfd0.jpeg');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `section`
--

CREATE TABLE `section` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `seo_generate` tinyint(1) DEFAULT NULL,
  `seo_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_keywords` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `section`
--

INSERT INTO `section` (`id`, `title`, `content`, `created_at`, `updated_at`, `seo_generate`, `seo_title`, `seo_keywords`, `seo_description`) VALUES
(1, 'Kindla Music', '<p class=\"roboto\">\r\n	<strong>Kindla Music - wszystko co nas otacza jest muzyką! Dla nas jest jak powietrze. Muzyka jest naszą miłością, pracą ale przede wszystkim pasją.&nbsp;</strong></p>\r\n<p class=\"roboto\">\r\n	&nbsp;</p>\r\n<p class=\"roboto\">\r\n	Produkcja muzyczna to dla nas ekscytujący proces tw&oacute;rczy zakończony dziełem za kt&oacute;rym kryję się zawsze zesp&oacute;ł ludzi zaangażowanych na 100% w to co robią! Dzięki temu możemy pochwalić się pracą dla wielu artyst&oacute;w i firm. Produkcja muzyczna, aranżacja, komponowanie piosenek, muzyki ilustracyjnej, pisanie tekst&oacute;w to nasza praca i pasja jednocześnie.</p>\r\n<p class=\"roboto\">\r\n	Nie ma dla nas projekt&oacute;w zbyt dużych i nie ma projekt&oacute;w zbyt małych do zrealizowania - wszystkie są tak samo ważne!</p>\r\n<p class=\"roboto\">\r\n	&nbsp;</p>\r\n<p class=\"roboto\">\r\n	<strong>Kindla Music</strong> to r&oacute;wnież <strong>KM Publishing</strong> oraz <strong>MAMAMUSICstudio</strong> - zapraszamy do wsp&oacute;łpracy!</p>\r\n<p class=\"red\">\r\n	&nbsp;</p>\r\n<p class=\"red\">\r\n	<span style=\"font-size:12px;\"><strong><u><span style=\"font-family:verdana,geneva,sans-serif;\">Kindla Music - &nbsp;Biuro (Office):</span></u></strong></span></p>\r\n<p class=\"red\">\r\n	<span style=\"font-size:12px;\"><span style=\"font-family:verdana,geneva,sans-serif;\">info@kindlamusic.pl&nbsp;</span></span></p>\r\n<p class=\"red\">\r\n	<span style=\"font-size:12px;\"><span style=\"font-family:verdana,geneva,sans-serif;\">Phone: +48 501 32 32 87&nbsp;</span></span></p>\r\n<p class=\"red\">\r\n	<span style=\"font-size:12px;\"><span style=\"font-family:verdana,geneva,sans-serif;\">www.kindlamusic.pl&nbsp;</span></span></p>\r\n<p class=\"red\">\r\n	<span style=\"font-size:12px;\"><span style=\"font-family:verdana,geneva,sans-serif;\">www.mamamusicstudio.pl&nbsp;</span></span></p>\r\n<p class=\"red\">\r\n	&nbsp;</p>\r\n<p class=\"roboto\">\r\n	&nbsp;</p>\r\n<p class=\"roboto\">\r\n	<span style=\"font-size:12px;\"><u><em><span style=\"font-family:verdana,geneva,sans-serif;\">Artyści z kt&oacute;rymi wsp&oacute;łpracowaliśmy:</span></em></u></span></p>\r\n<p class=\"roboto\">\r\n	<span style=\"font-size:12px;\"><em>Antek Smykiewicz, Agata Nizińska, Andrzej Rybiński, Feel, Maria Niklińska, Rafał Brzozowski, Doda, Sarsa, Katarzyna Cerekwicka, Marcin Spenner, Krzysztof Kiljański, Artur Gadowski, Arkadiusz Jakubik, Tomasz Karolak, Olivier Janiak, Marcin Perchuć, Andrzej Grabowski, Olek Klepacz, Włodek Pawlik, Margo, Margaret, Magdalena R&oacute;żczka, Katarzyna Pakosińska, Anja Orthodox, Paweł Małaszyński, Kasia Popowska, Ewa Farna, Anna Guzik, Paulina Sykut - Jeżyna, Adi Kowalski, Iza Kowalewska, Alexandra, Halina Mlynkova, Paulla, Honorata Skarbek, Patryk Kum&oacute;r, Przemysław Smolarski, Kriss Sheridan, RH+, Sachiel, Aneta Figiel, Grzegorz Poloczek, Damian Ukeje, Janusz Radek, Finaliści THE VOICE OF POLAND (I i II edycja programu).</em></span></p>\r\n<p class=\"roboto\">\r\n	&nbsp;</p>', '2017-04-06 10:32:00', '2017-09-03 18:10:55', 0, 'Kindla music', 'Kindla, music, Lorem, ipsum, dolor, amet, consectetur, adipiscing, elit, Etiam', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam viverra euismod malesuada. Mauris vel fringilla dui. Praesent sit amet maximus erat. Sed ac...'),
(2, 'KM Publishing', '<h2 class=\"head2\">\r\n	<span style=\"font-family:verdana,geneva,sans-serif;\"><span style=\"color: rgb(37, 35, 34); font-size: 14px; font-weight: normal; letter-spacing: -0.016em;\"><strong>KM Publishing</strong> - tu znajdziesz kompozycję jakiej szukasz,znajdziesz teksty,znajdziesz muzykę do oprawy serialu,film&oacute;w czy program&oacute;w telewizyjnych,kt&oacute;rą stworzymy specjalnie dla Ciebie.Wsp&oacute;łpracujemy z najlepszymi tw&oacute;rcami z Polski jak i z zagranicy oraz wieloma publisherami z całego świata.Jesteśmy &bdquo;butikowym&rdquo; publisherem, kt&oacute;ry starannie dobiera tw&oacute;rc&oacute;w do wsp&oacute;łpracy chcąc zachować wysoką jakość!</span></span></h2>\r\n<h2 class=\"head2\">\r\n	<span style=\"font-family:verdana,geneva,sans-serif;\"><span style=\"color: rgb(37, 35, 34); font-size: 14px; font-weight: normal; letter-spacing: -0.016em;\">KM Publishing należy do wielkiej rodziny UNIVERSAL MUSIC PUBLISHING GRUP.</span></span></h2>\r\n<p class=\"roboto\">\r\n	&nbsp;</p>\r\n<p class=\"roboto\">\r\n	<span style=\"font-family:verdana,geneva,sans-serif;\">Zapraszamy do wsp&oacute;łpracy!</span></p>\r\n<div>\r\n	&nbsp;</div>', '2017-04-06 10:36:59', '2017-09-03 18:16:21', 0, 'KM Publishing by UMPG,Kindla Music', 'KM Publishing, publishing, Publishing, znajdziesz, kompozycję, jakiej, szukaszznajdziesz, teksty znajdziesz, muzykę, oprawy', 'KM Publishing - tu znajdziesz kompozycję jakiej szukasz,znajdziesz teksty,znajdziesz muzykę do oprawy serialu,filmy czy programów...'),
(3, 'MAMAMUSICstudio', '<p class=\"roboto\">\r\n	<span style=\"font-family:verdana,geneva,sans-serif;\"><b>MAMAMUSICstudio - znajdujące się w samym centrum Katowic studio nagrań, w kt&oacute;rym już od blisko 10 lat powstaje wiele płyt i singli najpopularniejszych Polskich artyst&oacute;w oraz wielu gości z zagranicy.</b></span></p>\r\n<p class=\"roboto\">\r\n	&nbsp;</p>\r\n<p class=\"roboto\">\r\n	<span style=\"font-family: verdana, geneva, sans-serif;\">Na początku 2017 roku studio zostało przebudowane.W tej chwili składa się z dw&oacute;ch gł&oacute;wnych pomieszczeń - REŻYSERKI oraz WRITING ROOM&#39;u.W studio znajduje się r&oacute;wnież kabina do nagrań.</span></p>\r\n<p class=\"roboto\">\r\n	<span style=\"font-family:verdana,geneva,sans-serif;\">Sercem studia jest konsoleta SSL Matrix.</span></p>\r\n<p class=\"roboto\">\r\n	<span style=\"font-family:verdana,geneva,sans-serif;\">MAMAMUSICstudio jako pierwsze postawiło w swojej ofercie na w pełni wyposażone i komfortowe miejsce do komponowania piosenek (Writing Room) zaopatrzone r&oacute;wnież w instrumenty i wszystko co potrzebne do zrealizowania swojego pomysłu na piosenkę.</span></p>\r\n<p class=\"roboto\">\r\n	<span style=\"font-family:verdana,geneva,sans-serif;\">Filarami studia są Marcin Kindla,Michał Kuczera oraz Lukas Drozd - wszyscy tworzący od lat pod szyldem MAMAMUSICteam mający na swoim koncie ogromne doświadczenie oraz wiele wyr&oacute;żnień w postaci Złotych lub Platynowych płyt.</span></p>\r\n<p class=\"roboto\">\r\n	&nbsp;</p>\r\n<p class=\"roboto\">\r\n	<u><strong><span style=\"font-size:12px;\"><span style=\"font-family:verdana,geneva,sans-serif;\"><span style=\"color: rgb(29, 33, 41); letter-spacing: -0.11999999731779099px;\">Sprzęt (REŻYSERKA):</span></span></span></strong></u></p>\r\n<p style=\"margin-bottom: 6px; font-family: \'SF Optimized\', system-ui, -apple-system, BlinkMacSystemFont, \'.SFNSText-Regular\', sans-serif; color: rgb(29, 33, 41); letter-spacing: -0.11999999731779099px;\">\r\n	<span style=\"font-size:12px;\"><span style=\"font-family:verdana,geneva,sans-serif;\"><span class=\"text_exposed_show\" style=\"display: inline;\">Konsoleta: Solid State Logic Matrix 2</span></span></span></p>\r\n<div class=\"text_exposed_show\" style=\"display: inline; font-family: \'SF Optimized\', system-ui, -apple-system, BlinkMacSystemFont, \'.SFNSText-Regular\', sans-serif; color: rgb(29, 33, 41); font-size: 14px; letter-spacing: -0.11999999731779099px;\">\r\n	<p style=\"margin-bottom: 6px; font-family: inherit;\">\r\n		<span style=\"font-size:12px;\"><span style=\"font-family:verdana,geneva,sans-serif;\">Audio Card:Universal Audio Apollo&nbsp;</span></span></p>\r\n	<p style=\"margin-top: 6px; margin-bottom: 6px; font-family: inherit;\">\r\n		<span style=\"font-size:12px;\"><span style=\"font-family:verdana,geneva,sans-serif;\">Preamp:SSL Alpha Channel<br />\r\n		Universal Audio LA-610 mkII</span></span></p>\r\n	<p style=\"margin-top: 6px; margin-bottom: 6px; font-family: inherit;\">\r\n		<span style=\"font-size:12px;\"><span style=\"font-family:verdana,geneva,sans-serif;\">Dynamika / Psychoakustyka:<br />\r\n		Teletronix LA-2A Leveling Amplifier<br />\r\n		IGS 1176 FET Compressor<br />\r\n		IGS S-Type VCA Compressor<br />\r\n		DBX Digital Dynamics Procesor<br />\r\n		UAD-2 Plugins<br />\r\n		SSL Duende Native<br />\r\n		Symetrix 201 VCA Compressor<br />\r\n		BBE 482 Sonic Maximizer</span></span></p>\r\n	<p style=\"margin-top: 6px; margin-bottom: 6px; font-family: inherit;\">\r\n		<span style=\"font-size:12px;\"><span style=\"font-family:verdana,geneva,sans-serif;\">Reverb/Delay:Lexicon MPX-1<br />\r\n		Lexicon MX300<br />\r\n		TC Electronic M2000<br />\r\n		Roland SDE 330 Dimension Space Delay</span></span></p>\r\n	<p style=\"margin-top: 6px; margin-bottom: 6px; font-family: inherit;\">\r\n		<span style=\"font-size:12px;\"><span style=\"font-family:verdana,geneva,sans-serif;\">Monitors: KRK V8&nbsp;<br />\r\n		FOCAL Alpha 80<br />\r\n		APS Aeon</span></span></p>\r\n	<p style=\"margin-top: 6px; margin-bottom: 6px; font-family: inherit;\">\r\n		<span style=\"font-size:12px;\"><span style=\"font-family:verdana,geneva,sans-serif;\">Komputery: Iron Mac IntelCore i7 3,41 GHz<br />\r\n		PC Intel i7 DualQuad 2,81 GHz<br />\r\n		Logic X Pro<br />\r\n		Cubase 5</span></span></p>\r\n	<p style=\"margin-top: 6px; margin-bottom: 6px; font-family: inherit;\">\r\n		<span style=\"font-size:12px;\"><span style=\"font-family:verdana,geneva,sans-serif;\">Software &amp; Plug-ins:Native Instruments Ultimate Complete 11<br />\r\n		UAD-2 Powered Plug-ins<br />\r\n		d-16 All Plug-ins &amp; VSTi<br />\r\n		SSL Duende Plug-ins<br />\r\n		Soundtoys All<br />\r\n		Slate Digital&nbsp;<br />\r\n		and more...</span></span></p>\r\n	<p style=\"margin-top: 6px; margin-bottom: 6px; font-family: inherit;\">\r\n		&nbsp;</p>\r\n	<p style=\"margin-top: 6px; margin-bottom: 6px; font-family: inherit;\">\r\n		<u><strong><span style=\"font-size:12px;\"><span style=\"font-family:verdana,geneva,sans-serif;\"><span style=\"color: rgb(29, 33, 41);\">SPRZĘT (Writing Room):</span></span></span></strong></u></p>\r\n</div>\r\n<p style=\"margin-top: 6px; margin-bottom: 6px; font-family: \'SF Optimized\', system-ui, -apple-system, BlinkMacSystemFont, \'.SFNSText-Regular\', sans-serif; color: rgb(29, 33, 41); letter-spacing: -0.11999999731779099px;\">\r\n	<span style=\"font-size:12px;\"><span style=\"font-family:verdana,geneva,sans-serif;\">Audio Card: Apogee Quartet</span></span></p>\r\n<p style=\"margin-top: 6px; margin-bottom: 6px; font-family: \'SF Optimized\', system-ui, -apple-system, BlinkMacSystemFont, \'.SFNSText-Regular\', sans-serif; color: rgb(29, 33, 41); letter-spacing: -0.11999999731779099px;\">\r\n	<span style=\"font-size:12px;\"><span style=\"font-family:verdana,geneva,sans-serif;\">Preamp: Universal Audio LA-610 mkII</span></span></p>\r\n<p style=\"margin-top: 6px; margin-bottom: 6px; font-family: \'SF Optimized\', system-ui, -apple-system, BlinkMacSystemFont, \'.SFNSText-Regular\', sans-serif; color: rgb(29, 33, 41); letter-spacing: -0.11999999731779099px;\">\r\n	<span style=\"font-size:12px;\"><span style=\"font-family:verdana,geneva,sans-serif;\">Monitors: FOCAL ALpha 65<br />\r\n	KRK Rokit 5</span></span></p>\r\n<p style=\"margin-top: 6px; margin-bottom: 6px; font-family: \'SF Optimized\', system-ui, -apple-system, BlinkMacSystemFont, \'.SFNSText-Regular\', sans-serif; color: rgb(29, 33, 41); letter-spacing: -0.11999999731779099px;\">\r\n	<br />\r\n	<span style=\"font-size:12px;\"><span style=\"font-family:verdana,geneva,sans-serif;\">Komputery: Iron Mac IntelCore i7 3,41 GHz<br />\r\n	Logic Pro X<br />\r\n	Możliwość podpięcia własnego komputera.</span></span></p>\r\n<p style=\"margin-top: 6px; margin-bottom: 6px; font-family: \'SF Optimized\', system-ui, -apple-system, BlinkMacSystemFont, \'.SFNSText-Regular\', sans-serif; color: rgb(29, 33, 41); letter-spacing: -0.11999999731779099px;\">\r\n	<span style=\"font-size:12px;\"><span style=\"font-family:verdana,geneva,sans-serif;\">Software &amp; Plug-ins: Native Instruments Ultimate Komplete 11<br />\r\n	UAD-2 Powered Plug-ins<br />\r\n	d-16 All Plug-ins &amp; VSTi<br />\r\n	Soundtoys All</span></span></p>\r\n<p style=\"margin-top: 6px; margin-bottom: 6px; font-family: \'SF Optimized\', system-ui, -apple-system, BlinkMacSystemFont, \'.SFNSText-Regular\', sans-serif; color: rgb(29, 33, 41); letter-spacing: -0.11999999731779099px;\">\r\n	<span style=\"font-size:12px;\"><span style=\"font-family:verdana,geneva,sans-serif;\">Instrumenty: Gitary akustyczne - Taylor,Gibson,Baton Rouge<br />\r\n	Gitary elektryczne - Ibanez,Fender,Baton Rouge<br />\r\n	Gitara basowa - Yamaha<br />\r\n	Ukulele - Baton Rouge<br />\r\n	Klawiatury: M-Audio Axiom Pro 49,Novation MiniNova</span></span></p>\r\n<p class=\"roboto\">\r\n	&nbsp;</p>\r\n<ul>\r\n</ul>', '2017-04-06 10:37:28', '2017-09-03 18:13:03', 0, 'MAMAMUSICstudio', 'MAMAMUSIC, grupa, producencka, zajmująca, się, realizacją, aranżacją, produkcją, muzyczną,studio,nagran', NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `send_demo`
--

CREATE TABLE `send_demo` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `message` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `send_demo`
--

INSERT INTO `send_demo` (`id`, `name`, `email`, `message`, `created_at`) VALUES
(7, 'test', 'test@test.test', 'test', '2017-06-08 15:41:30');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `send_demo_file`
--

CREATE TABLE `send_demo_file` (
  `id` int(11) NOT NULL,
  `send_demo_id` int(11) DEFAULT NULL,
  `filename` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `send_demo_file`
--

INSERT INTO `send_demo_file` (`id`, `send_demo_id`, `filename`) VALUES
(11, 7, 'demo-5939540b8aee3.png');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `setting`
--

CREATE TABLE `setting` (
  `id` int(11) NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `setting`
--

INSERT INTO `setting` (`id`, `label`, `description`, `value`) VALUES
(1, 'seo_description', 'Domyślna wartość meta tagu \"description\"', 'Marcin Kindla - Player'),
(2, 'seo_keywords', 'Domyślna wartość meta tagu \"keywords\"', 'Marcin, Kindla, Music, Producent, Autor tekstów'),
(3, 'seo_title', 'Domyślna wartość meta tagu \"title\"', 'Marcin Kindla - kompozytor, autor tekstów, producent muzyczny.'),
(4, 'url_facebook', 'Adres strony na Facebook-u', 'https://www.facebook.com/MarcinKindla-156451071069488/timeline/'),
(5, 'url_youtube', 'Adres kanału na YouTube', 'https://www.youtube.com/user/MarcinKindlaVEVO'),
(6, 'contact_kindla_firm', 'Kontakt - Kindla Music - Nazwa firmy', 'Kindla Music'),
(7, 'contact_kindla_name', 'Kontakt - Kindla Music - Imię i nazwisko', 'Marcin Kindla'),
(8, 'contact_kindla_email', 'Kontakt - Kindla Music - Adres e-mail', 'info@kindlamusic.pl'),
(9, 'contact_universal_firm', 'Kontakt - Universal Music - Nazwa firmy', 'Universal Music Publishing MGB Poland'),
(10, 'contact_universal_address', 'Kontakt - Universal Music - Adres', 'ul.Włodarzewska 69\r\n02-384 Warszawa\r\nPoland'),
(11, 'contact_universal_phone', 'Kontakt - Universal Music - Telefon', '(48 22) 59 28 246'),
(12, 'contact_universal_fax', 'Kontakt - Universal Music - Fax', '(48 22) 59 28 250'),
(13, 'contact_universal_email_title', 'Kontakt - Universal Music - Adres e-mail - Adresat', 'Beata Olechowska, Publishing Director'),
(14, 'contact_universal_email', 'Kontakt - Universal Music - Adres e-mail', 'beata.olechowska@umusic.com'),
(15, 'contact_universal_page', 'Kontakt - Universal Music - Strona internetowa', 'www.universalmusicpublishing.com'),
(16, 'kmpublishing_contact_email', 'KM Publishing - E-Mail kontaktowy', 'info@kindlamusic.pl'),
(17, 'kmpublishing_contact_phone', 'KM Publishing - Telefon kontaktowy', '+48 501 323 287');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `temp_file`
--

CREATE TABLE `temp_file` (
  `id` int(11) NOT NULL,
  `filename` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `temp_file`
--

INSERT INTO `temp_file` (`id`, `filename`, `created_at`, `updated_at`) VALUES
(3, 'Sialala_demo.mp3', '2015-10-02 16:27:03', NULL),
(4, NULL, '2017-06-07 11:55:27', NULL),
(5, 'Sialala_demo (1).mp3', '2017-06-07 15:12:46', NULL),
(7, 'Sialala_demo (2).mp3', '2017-06-07 15:48:57', NULL),
(8, 'Sialala_demo (3).mp3', '2017-06-07 15:49:05', NULL),
(9, 'LOVE ISLAND , motivation, good mood, enegetic ballad pop song Come together - 100bpm demo INSTR.mp3', '2018-01-17 18:49:27', NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:json_array)',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `user`
--

INSERT INTO `user` (`id`, `username`, `email`, `salt`, `password`, `active`, `last_login`, `roles`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'biuro@lemonadestudio.pl', 'e40e0bc6f3ebacbed6e0e19b772845a8', 'Rd/3Txoa82/D9hi5AUbzSaMo0mR7YPs5tVG7TwD12SREfgob9BnzCItP1AhQxf7gi/IHE67DdDYPbMWPZIAROg==', 1, NULL, '[\"ROLE_ADMIN\",\"ROLE_ALLOWED_TO_SWITCH\",\"ROLE_USER\"]', '2015-07-31 13:46:34', NULL),
(2, 'user', 'user@test.pl', '01e41e1026d384d704a0a99b180bfd27', '/jkSRqRPHSvYjcMy+ip1yY1UFH282hjU2rV1nOc40SzFK90W5Kla9WWjopQQq4/6LFkDkkUM7pacLPrDLF9n9Q==', 1, NULL, '[\"ROLE_USER\"]', '2015-09-15 14:05:54', NULL),
(3, 'marcin', 'makindla@interia.pl', 'baf17077fd07b77d41617ed50a049386', 'iNZsww0Zs30vOcy+UGNR2+n9oC3v3SZIptnZ4VMiPjWRC6URIkc2GLLrU5c1PizwyzX+hnOABuiZAChn2YzRqA==', 1, NULL, '[\"ROLE_ADMIN\",\"ROLE_USER\",\"ROLE_ALLOWED_TO_SWITCH\"]', '2015-09-16 22:06:10', NULL),
(4, 'Marcin Russek', 'marcin.russek@sonymusic.com', 'e7a0b1c73eb690ecd11599c9697723c1', '+3H+SKYxpFbft9+VPPF1jRc7Zg8Qo2qJPhJWSpJJU8cIPtkpOZvdCiorVY8y1a/2piogp6BXNv0FhGnQLtFexg==', 1, NULL, '[\"ROLE_USER\"]', '2016-02-01 20:42:45', NULL),
(5, 'TakoMedia', 'test@test.pl', 'f8e1cd6b449087ade54ab18d773e0f92', '6Om9qjz8CgYCFH3/CZlN622HWXXEPHF4sVPxb/O4Eqds60xRYcOp29PywvBMzpHKp2HgujAc4XCReaRmfl4nAg==', 1, NULL, '[\"ROLE_USER\"]', '2018-01-17 17:30:36', NULL);

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `audio_file`
--
ALTER TABLE `audio_file`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_C32E2A4C12469DE2` (`category_id`);

--
-- Indeksy dla tabeli `audio_file_category`
--
ALTER TABLE `audio_file_category`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `audio_file_download`
--
ALTER TABLE `audio_file_download`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_E346746AC7C70B0` (`audio_file_id`);

--
-- Indeksy dla tabeli `composer`
--
ALTER TABLE `composer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_987306D84E7AF8F` (`gallery_id`);

--
-- Indeksy dla tabeli `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `gallery_movie`
--
ALTER TABLE `gallery_movie`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_F9C3224C4E7AF8F` (`gallery_id`);

--
-- Indeksy dla tabeli `logos`
--
ALTER TABLE `logos`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `menu_item`
--
ALTER TABLE `menu_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_D754D550727ACA70` (`parent_id`);

--
-- Indeksy dla tabeli `menu_item_translations`
--
ALTER TABLE `menu_item_translations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_24D3F735232D562B` (`object_id`);

--
-- Indeksy dla tabeli `migration_versions`
--
ALTER TABLE `migration_versions`
  ADD PRIMARY KEY (`version`);

--
-- Indeksy dla tabeli `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_140AB6204E7AF8F` (`gallery_id`);

--
-- Indeksy dla tabeli `page_translations`
--
ALTER TABLE `page_translations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_78AB76C9232D562B` (`object_id`);

--
-- Indeksy dla tabeli `partners`
--
ALTER TABLE `partners`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `section`
--
ALTER TABLE `section`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `send_demo`
--
ALTER TABLE `send_demo`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `send_demo_file`
--
ALTER TABLE `send_demo_file`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_A0C75EB6F7021484` (`send_demo_id`);

--
-- Indeksy dla tabeli `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_9F74B898EA750E8` (`label`);

--
-- Indeksy dla tabeli `temp_file`
--
ALTER TABLE `temp_file`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_8D93D649F85E0677` (`username`),
  ADD UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `audio_file`
--
ALTER TABLE `audio_file`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;

--
-- AUTO_INCREMENT dla tabeli `audio_file_category`
--
ALTER TABLE `audio_file_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT dla tabeli `audio_file_download`
--
ALTER TABLE `audio_file_download`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=164;

--
-- AUTO_INCREMENT dla tabeli `composer`
--
ALTER TABLE `composer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `gallery`
--
ALTER TABLE `gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT dla tabeli `gallery_movie`
--
ALTER TABLE `gallery_movie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT dla tabeli `logos`
--
ALTER TABLE `logos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT dla tabeli `menu_item`
--
ALTER TABLE `menu_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT dla tabeli `menu_item_translations`
--
ALTER TABLE `menu_item_translations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT dla tabeli `page`
--
ALTER TABLE `page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT dla tabeli `page_translations`
--
ALTER TABLE `page_translations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT dla tabeli `partners`
--
ALTER TABLE `partners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT dla tabeli `section`
--
ALTER TABLE `section`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT dla tabeli `send_demo`
--
ALTER TABLE `send_demo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT dla tabeli `send_demo_file`
--
ALTER TABLE `send_demo_file`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT dla tabeli `setting`
--
ALTER TABLE `setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT dla tabeli `temp_file`
--
ALTER TABLE `temp_file`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT dla tabeli `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `audio_file`
--
ALTER TABLE `audio_file`
  ADD CONSTRAINT `FK_C32E2A4C12469DE2` FOREIGN KEY (`category_id`) REFERENCES `audio_file_category` (`id`) ON DELETE SET NULL;

--
-- Ograniczenia dla tabeli `audio_file_download`
--
ALTER TABLE `audio_file_download`
  ADD CONSTRAINT `FK_E346746AC7C70B0` FOREIGN KEY (`audio_file_id`) REFERENCES `audio_file` (`id`) ON DELETE SET NULL;

--
-- Ograniczenia dla tabeli `composer`
--
ALTER TABLE `composer`
  ADD CONSTRAINT `FK_987306D84E7AF8F` FOREIGN KEY (`gallery_id`) REFERENCES `gallery` (`id`) ON DELETE SET NULL;

--
-- Ograniczenia dla tabeli `gallery_movie`
--
ALTER TABLE `gallery_movie`
  ADD CONSTRAINT `FK_F9C3224C4E7AF8F` FOREIGN KEY (`gallery_id`) REFERENCES `gallery` (`id`) ON DELETE CASCADE;

--
-- Ograniczenia dla tabeli `menu_item`
--
ALTER TABLE `menu_item`
  ADD CONSTRAINT `FK_D754D550727ACA70` FOREIGN KEY (`parent_id`) REFERENCES `menu_item` (`id`);

--
-- Ograniczenia dla tabeli `menu_item_translations`
--
ALTER TABLE `menu_item_translations`
  ADD CONSTRAINT `FK_24D3F735232D562B` FOREIGN KEY (`object_id`) REFERENCES `menu_item` (`id`) ON DELETE CASCADE;

--
-- Ograniczenia dla tabeli `page`
--
ALTER TABLE `page`
  ADD CONSTRAINT `FK_140AB6204E7AF8F` FOREIGN KEY (`gallery_id`) REFERENCES `gallery` (`id`) ON DELETE SET NULL;

--
-- Ograniczenia dla tabeli `page_translations`
--
ALTER TABLE `page_translations`
  ADD CONSTRAINT `FK_78AB76C9232D562B` FOREIGN KEY (`object_id`) REFERENCES `page` (`id`) ON DELETE CASCADE;

--
-- Ograniczenia dla tabeli `send_demo_file`
--
ALTER TABLE `send_demo_file`
  ADD CONSTRAINT `FK_A0C75EB6F7021484` FOREIGN KEY (`send_demo_id`) REFERENCES `send_demo` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
