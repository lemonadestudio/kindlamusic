<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            new Doctrine\Bundle\MigrationsBundle\DoctrineMigrationsBundle(),
            new Doctrine\Bundle\FixturesBundle\DoctrineFixturesBundle(),
            new Knp\Bundle\MenuBundle\KnpMenuBundle(),
            new Knp\Bundle\PaginatorBundle\KnpPaginatorBundle(),
            new Lunetics\LocaleBundle\LuneticsLocaleBundle(),
            new WhiteOctober\BreadcrumbsBundle\WhiteOctoberBreadcrumbsBundle(),
            new Ls\CoreBundle\LsCoreBundle(),
            new Ls\AudioPlayerBundle\LsAudioPlayerBundle(),
            new Ls\GalleryBundle\LsGalleryBundle(),
            new Ls\MenuBundle\LsMenuBundle(),
            new Ls\PageBundle\LsPageBundle(),
            new Ls\MediaBundle\LsMediaBundle(),
            new Ls\SettingBundle\LsSettingBundle(),
            new Ls\UserBundle\LsUserBundle(),
            new Ls\TempFileBundle\LsTempFileBundle(),
            new Ls\SectionBundle\LsSectionBundle(),
            new Ls\ComposerBundle\LsComposerBundle(),
            new Ls\PartnersBundle\LsPartnersBundle(),
            new Ls\SendDemoBundle\LsSendDemoBundle(),
            new Ls\LogosBundle\LsLogosBundle(),
            new Ls\ActorsBundle\LsActorsBundle(),            
            new Ls\PopupBundle\LsPopupBundle(),
            new Ls\EventsPageBundle\LsEventsPageBundle(),
            new Ls\NewsletterBundle\LsNewsletterBundle(),
            new Ls\DictionaryBundle\LsDictionaryBundle(),
            new Ls\ContactBundle\LsContactBundle(),
            new Ls\PageHomeBundle\LsPageHomeBundle(),
            new Ls\BambiniSettingBundle\LsBambiniSettingBundle(),
            new Ls\BambiniColoringBundle\LsBambiniColoringBundle(),
        );

        if (in_array($this->getEnvironment(), array('dev', 'test'))) {
            $bundles[] = new Symfony\Bundle\DebugBundle\DebugBundle();
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load($this->getRootDir().'/config/config_'.$this->getEnvironment().'.yml');
    }
}
