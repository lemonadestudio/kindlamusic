imports:
    - { resource: parameters.yml }
    - { resource: security.yml }
    - { resource: services.yml }

# Put parameters here that don't need to change on each machine where the app is deployed
# http://symfony.com/doc/current/best_practices/configuration.html#application-related-configuration
parameters:
    locale: pl

framework:
    #esi:             ~
    translator:      { fallbacks: ["%locale%"] }
    secret:          "%secret%"
    router:
        resource: "%kernel.root_dir%/config/routing.yml"
        strict_requirements: ~
    form:            ~
    csrf_protection: ~
    validation:      { enable_annotations: true }
    #serializer:      { enable_annotations: true }
    templating:
        engines: ['twig']
        #assets_version: SomeVersionScheme
    default_locale:  "%locale%"
    trusted_hosts:   ~
    trusted_proxies: ~
    session:
        # handler_id set to null will use default session handler from php.ini
        handler_id:  ~
    fragments:       ~
    http_method_override: true

# Twig Configuration
twig:
    debug:            "%kernel.debug%"
    strict_variables: "%kernel.debug%"

# Doctrine Configuration
doctrine:
    dbal:
        driver:   pdo_mysql
        host:     "%database_host%"
        port:     "%database_port%"
        dbname:   "%database_name%"
        user:     "%database_user%"
        password: "%database_password%"
        charset:  UTF8
        # if using pdo_sqlite as your database driver:
        #   1. add the path in parameters.yml
        #     e.g. database_path: "%kernel.root_dir%/data/data.db3"
        #   2. Uncomment database_path in parameters.yml.dist
        #   3. Uncomment next line:
        #     path:     "%database_path%"

    orm:
        auto_generate_proxy_classes: "%kernel.debug%"
        naming_strategy: doctrine.orm.naming_strategy.underscore
        auto_mapping: true

# Swiftmailer Configuration
swiftmailer:
    transport: "%mailer_transport%"
    host:      "%mailer_host%"
    port:      "%mailer_port%"
    username:  "%mailer_user%"
    password:  "%mailer_password%"
    encryption: "%mailer_encryption%"
    spool:     { type: memory }

lunetics_locale:
    strict_mode: true
    allowed_locales:
        - pl
        - en
    guessing_order:
        - query
        - router
        - session
        - cookie
        - browser
    switcher:
        template: LsCoreBundle:Frontend:locale_switcher.html.twig
        show_current_locale: true

services:
    twig.extension.intl:
        class: Twig_Extensions_Extension_Intl
        tags:
            - { name: twig.extension }
    twig.extension.text:
        class: Twig_Extensions_Extension_Text
        tags:
            - { name: twig.extension }

white_october_breadcrumbs:
    separator: ''

knp_paginator:
    page_range: 5                      # default page range used in pagination control
    default_options:
        page_name: page                # page query parameter name
        sort_field_name: sort          # sort field query parameter name
        sort_direction_name: direction # sort direction query parameter name
        distinct: true                 # ensure distinct results, useful when ORM queries are using GROUP BY statements
    template:
        pagination: LsCoreBundle:Frontend:paginator.html.twig     # sliding pagination controls template
        sortable: KnpPaginatorBundle:Pagination:sortable_link.html.twig # sort link template

ls_core:
    admin:
        dashboard:
            cmusic:
                label: "Kindla Music"
                items:
                    - ls_section.admin_kmusic
                    - ls_partners.admin
            kpublishing:
                label: "KM Publishing"
                items:
                    - ls_section.admin_kpublishing
                    - ls_composer.admin
                    - ls_logos.admin
                    - ls_send_demo.admin
            mamamusic:
                label: "MAMAMUSIC"
                items:
                    - ls_section.admin_mamamusic
            kmevents:
                label: "KM Events"
                items:
                    - ls_section.admin_kmevents
                    - ls_actors.admin
                    - ls_events_page.admin
                    - ls_popup.admin
            kplayer:
                label: "Kindla Player"
                items:
                    - ls_audio_player.admin
                    - ls_page.admin
            multimedia:
                label: "Multimedia"
                items:
                    - ls_gallery.admin
            other:
                label: "Inne"
                items:
                    - ls_menu.admin
                    - ls_setting.admin
                    - ls_user.admin
        menu:
            cmusic:
                title: "Kindla Music"
                links:
                    - ls_section.admin_kmusic
                    - ls_partners.admin
            kpublishing:
                title: "KM Publishing"
                links:
                    - ls_section.admin_kpublishing
                    - ls_composer.admin
                    - ls_logos.admin
                    - ls_send_demo.admin
            mamamusic:
                title: "MAMAMusic"
                links:
                    - ls_section.admin_mamamusic
            kmevents:
                title: "KM Events"
                links:
                    - ls_section.admin_kmevents
                    - ls_actors.admin
                    - ls_events_page.admin
                    - ls_popup.admin
            kplayer:
                title: "Kindla Player"
                links:
                    - ls_audio_player.admin
                    - ls_page.admin
            multimedia:
                title: "Multimedia"
                links:
                    - ls_gallery.admin
            other:
                title: "Inne"
                links:
                    - ls_menu.admin
                    - ls_setting.admin
                    - ls_user.admin

ls_menu:
    locations:
        - { label: menu_top, name: Menu Kindla Player }
        - { label: menu_events, name: Menu KMEvents }
    modules:
        - { id: home,    label: Strona główna, route: { pl: ls_core_homepage,   en: ls_core_homepage } }
        - { id: player,  label: Player, route: { pl: ls_core_player,   en: ls_core_player } }
        - { id: contact, label: Kontakt,       route: { pl: ls_core_contact_pl, en: ls_core_contact_en } }
        - { id: page,    label: Podstrona,     route: { pl: ls_page_show,       en: ls_page_show },            get_elements_service: ls_menu.search.podstrona }
        - { id: events_home,    label: KMEvents - Strona główna,     route: { pl: ls_section_kmevents,       en: ls_section_kmevents } }
        - { id: events_actors,    label: KMEvents - Artyści,     route: { pl: ls_section_kmevents_actors,       en: ls_section_kmevents_actors } }
        - { id: events_contect,    label: KMEvents - Kontakt,     route: { pl: ls_section_kmevents_contact,       en: ls_section_kmevents_contact } }
        - { id: events_page,    label: KMEvents - Podstrona,     route: { pl: ls_events_page_show,       en: ls_events_page_show },            get_elements_service: ls_menu.search.events_podstrona }
    onclick: ~
